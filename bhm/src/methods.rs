use crate::dbus::{IN_FACE, IN_NAME, IN_PATH};
use crate::devices::IW_DEVICE;

fn call_bhm_dbus_method<B>(method_name: &str, body: &B) -> (String, String, u8)
where
    B: serde::ser::Serialize + zbus::export::zvariant::Type,
{
    let connection = zbus::Connection::new_system().unwrap();
    let destination = Some(IN_NAME);
    let path = IN_PATH;
    let iface = Some(IN_FACE);
    let ds = String::new();
    let ts = String::from("str");
    let te = String::from("err");
    match connection.call_method(destination, path, iface, method_name, body) {
        //Ok(message) => println!("{} {:?}", method_name, message.fields()),
        Ok(message) => {
            if method_name == "GetVersion" {
                match message.body::<String>() {
                    Ok(body) => (ts, body, 0),
                    Err(error) => {
                        let err = format!("Error {} body: {}.", method_name, error);
                        eprintln!("{}", error);
                        (te, err, 0)
                    }
                }
            } else {
                (ts, ds, 0)
            }
            //Some(message)
        }
        Err(error) => {
            let error = format!("{} Error: {}.", method_name, error);
            if method_name != "GetVersion" {
                eprintln!("{} Error: {}.", method_name, error);
            }
            (te, error, 0)
        }
    }
}

pub(crate) fn get_version() -> String {
    let r = call_bhm_dbus_method("GetVersion", &());
    if &r.0 == "str" {
        r.1
    } else {
        String::new()
    }
    // match call_bhm_dbus_method("GetVersion", &()) {
    //     Some(str) => s,
    //     None => String::new()
    //     //Evt::String(message) => message,
    //     //Evt::U8(_) =>  String::new(),
    //     //Evt::Err() => String::new(),
    // }
}

// pub fn set_profile(profile: u8) {
//     call_bhm_dbus_method("SetProfile", &(profile));
// }

pub(crate) fn set_cpu_boost(enable: bool) {
    let value: u8 = if enable { 1 } else { 0 };
    call_bhm_dbus_method("SetCpuFreqBoost", &(value));
}

pub(crate) fn set_fan_pwm(enable: bool) {
    let value: u8 = if enable { 2 } else { 0 };
    call_bhm_dbus_method("SetFanPwmEnable", &(value));
}

pub(crate) fn set_fan_ttp(mode: u8) {
    call_bhm_dbus_method("SetFanThrottleThermalPolicy", &(mode));
}

pub(crate) fn set_fan_curve(curve: String) {
    call_bhm_dbus_method("SetFanCurve", &(curve));
}

pub(crate) fn set_bat_limit(limit: u8) {
    call_bhm_dbus_method("SetBatChargeControlEndThreshold", &(limit));
}

pub(crate) fn set_wireless_enabled(enable: bool) {
    call_bhm_dbus_method("SetWirelessEnabled", &(enable));
}

pub(crate) fn set_wireless_power_save(mode: u8) {
    let device = IW_DEVICE;
    call_bhm_dbus_method("SetWirelessPowerSave", &(device, mode));
}

// pub fn get_wireless_enabled() -> bool {
//     let connection = zbus::Connection::new_system().unwrap();
//     let proxy = dbus::ClientNMProxy::new(&connection).unwrap();
//     proxy.wireless_enabled().unwrap()
// }

pub(crate) fn set_signal(message: &str) {
    call_bhm_dbus_method("SetSignal", &(message));
}

pub(crate) fn set_delta(delta: &[Vec<String>]) {
    call_bhm_dbus_method("SetDelta", &(delta));
}

pub(crate) fn next_profile() {
    call_bhm_dbus_method("NextProfile", &());
}
