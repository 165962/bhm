use crate::utility::{read_file_to_string, Evt};
use inotify::{EventMask, Inotify as SuperInotify, WatchDescriptor, WatchMask};
use std::{collections::HashMap, sync::mpsc::Sender, thread};
//use std::sync::{Arc,Mutex};

pub struct Inotify {
    pub data: Vec<Vec<String>>,
}

impl Inotify {
    pub fn new() -> Self {
        Self { data: vec![] }
    }

    pub fn data_new(&mut self, data: Vec<Vec<String>>) {
        self.data = data;
    }

    pub fn start(&self, tx: Sender<Evt>) {
        let mut map: HashMap<WatchDescriptor, (String, String, String)> = HashMap::new();
        let mut inotify = SuperInotify::init().expect("Failed to initialize inotify");
        for item in self.data.iter() {
            let wd = inotify
                .add_watch(&item[0], WatchMask::MODIFY)
                .expect("Failed to add inotify watch");
            map.insert(
                wd,
                (
                    item[0].to_string(),
                    item[1].to_string(),
                    read_file_to_string(&item[0]),
                ),
            );
        }
        thread::spawn(move || {
            //inotify.add_watch(paths[1], WatchMask::ALL_EVENTS)
            //    .expect("Failed to add inotify watch");
            println!("Inotify watching...");
            let mut buffer = [0u8; 4096];
            loop {
                match inotify.read_events_blocking(&mut buffer) {
                    Ok(events) => {
                        for event in events {
                            // if event.mask.contains(EventMask::MODIFY)
                            //     && !event.mask.contains(EventMask::ISDIR)
                            if event.mask.contains(EventMask::MODIFY) {
                                if let Some(item) = map.get_mut(&event.wd) {
                                    let value = read_file_to_string(&item.0);
                                    if item.2 != value {
                                        item.2 = value;
                                        tx.send(Evt::SetAction(
                                            item.1.to_string(),
                                            item.2.to_string(),
                                        ))
                                        .ok();
                                    }
                                }
                            }
                        }
                    }
                    Err(err) => eprintln!("Failed to read inotify events: {}", err),
                }
            }
        });
    }
}
