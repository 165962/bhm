use crate::{
    config::{Data, Profile},
    devices::{Dev, IW_DEVICE},
    fan_curve, iw, methods, service, VERSION,
};
use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::sync::{Arc, Mutex};
use std::{collections::HashMap, convert::TryInto};
use users::get_current_uid;
use zbus::{dbus_interface, dbus_proxy, export::zvariant, fdo};

pub(crate) const IN_NAME: &str = "bas.apps.bhm";
pub(crate) const IN_PATH: &str = "/bas/apps/bhm";
pub(crate) const IN_FACE: &str = "bas.apps.bhm";

// TODO
const PROFILE_FIELDS: [&str; 8] = [
    "id",
    "name",
    "cpu0-boost",
    "fan0-pwm_cpu",
    "fan0-ttp",
    "fan_curve",
    "wifi-enabled",
    "wifi-power_save",
];

#[dbus_proxy(
    default_service = "org.freedesktop.NetworkManager",
    default_path = "/org/freedesktop/NetworkManager",
    interface = "org.freedesktop.NetworkManager"
)]
pub(crate) trait ClientNM {
    #[dbus_proxy(property)]
    fn wireless_enabled(&self) -> zbus::Result<bool>;
    #[dbus_proxy(signal)]
    fn state_changed(&self, data: u32) -> zbus::Result<()>;
    #[dbus_proxy(signal)]
    fn properties_changed(
        &self,
        properties: HashMap<String, zvariant::Value<'_>>,
    ) -> zbus::Result<()>;
}

pub(crate) struct IFace {
    pub(crate) devs: Arc<Mutex<BTreeMap<String, Dev>>>,
}

#[dbus_interface(name = "bas.apps.bhm")]
impl IFace {
    fn get_version(&self) -> zbus::Result<&str> {
        Ok(VERSION)
    }

    fn get_supported(&self) -> zbus::Result<Vec<Vec<String>>> {
        let devs = &self.devs;

        // TODO
        let iw0 = iw::Iw::new(IW_DEVICE);
        let value_by_action = |act: &str| -> String {
            let pads = act.split('-').collect::<Vec<&str>>();
            if pads.len() == 2 {
                match devs.lock().unwrap().get(pads[0]) {
                    Some(a) => a.get(pads[1]),
                    None => {
                        if act == "nm-state" {
                            service::get_nm_state().to_string()
                        } else if act == "wifi-enabled" {
                            (service::get_wireless_enabled() as u8).to_string()
                        } else if act == "wifi-power_save" {
                            iw0.get_power_save().to_string()
                        } else if act == "profile-set"
                            || act == "profile-prev"
                            || act == "profile-next"
                            || act == "profile-editor"
                        {
                            "1".to_string()
                        } else if act == "profile-refresh" {
                            "0".to_string()
                        } else {
                            String::new()
                        }
                    }
                }
            } else {
                String::new()
            }
        };

        let mut data = vec![];

        let acts = [
            ("profile", "profile", "set", "profile-set"),
            ("profile", "profile", "prev", "profile-prev"),
            ("profile", "profile", "next", "profile-next"),
            ("profile", "profile", "create", "profile-create"),
            ("profile", "profile", "update", "profile-update"),
            ("profile", "profile", "remove", "profile-remove"),
            ("profile", "profile", "editor", "profile-editor"),
            ("profile", "profile", "refresh", "profile-refresh"),
        ];
        for (id, typ, key, act) in acts {
            data.push(vec![
                act.to_string(),
                value_by_action(act),
                id.to_string(),
                typ.to_string(),
                key.to_string(),
                String::new(),
            ]);
        }

        for (id, dev) in self.devs.lock().unwrap().iter() {
            for (key, path) in dev.paths().iter() {
                let act = &format!("{}-{}", id, key);
                data.push(vec![
                    act.to_string(),
                    dev.get(key),
                    dev.id(),
                    dev.typ(),
                    key.to_string(),
                    path.to_string(),
                ]);
            }
        }

        let acts = [
            ("bat0", "bat", "charge", "bat0-charge"),
            ("bat0", "bat", "health", "bat0-health"),
            ("nm", "nm", "state", "nm-state"),
            ("wifi", "wifi", "enabled", "wifi-enabled"),
            ("wifi", "wifi", "power_save", "wifi-power_save"),
        ];
        for (id, typ, key, act) in acts {
            data.push(vec![
                act.to_string(),
                value_by_action(act),
                id.to_string(),
                typ.to_string(),
                key.to_string(),
                String::new(),
            ]);
        }
        Ok(data)
    }

    fn get_cpu_freq_boost(&self) -> zbus::Result<u8> {
        Ok(service::get_cpu_boost(
            self.devs.lock().unwrap().get("cpu0").unwrap(),
        ))
    }
    fn get_fan_pwm_enable(&self) -> zbus::Result<u8> {
        Ok(service::get_fan_pwm(self.devs.lock().unwrap().get("fan0").unwrap()) as u8)
    }
    fn get_fan_throttle_thermal_policy(&self) -> zbus::Result<u8> {
        Ok(service::get_fan_ttp(
            self.devs.lock().unwrap().get("fan0").unwrap(),
        ))
    }
    fn get_fan_curve(&self) -> zbus::Result<String> {
        Ok(service::get_fan_curve())
    }
    fn get_bat_charge_control_end_threshold(&self) -> zbus::Result<u8> {
        Ok(service::get_bat_limit(
            self.devs.lock().unwrap().get("bat0").unwrap(),
        ))
    }
    fn get_kbd_brightness(&self) -> zbus::Result<u8> {
        let is = "";
        if is == "upower" {
            Ok(service::get_kbd_brightness())
        } else {
            Ok(match self.devs.lock().unwrap().get("kbd0") {
                Some(dev) => dev.get("brightness").parse::<u8>().unwrap_or_default(),
                None => 0,
            })
        }
    }
    fn get_kbd_max_brightness(&self) -> zbus::Result<u8> {
        let is = "";
        if is == "upower" {
            Ok(service::get_kbd_max_brightness())
        } else {
            Ok(match self.devs.lock().unwrap().get("kbd0") {
                Some(dev) => dev.get("max_brightness").parse::<u8>().unwrap_or_default(),
                None => 0,
            })
        }
    }
    fn get_scr_brightness(&self) -> zbus::Result<u8> {
        Ok(self
            .devs
            .lock()
            .unwrap()
            .get("scr0")
            .unwrap()
            .get("brightness")
            .parse::<u8>()
            .unwrap_or_default())
    }
    fn get_scr_max_brightness(&self) -> zbus::Result<u8> {
        Ok(self
            .devs
            .lock()
            .unwrap()
            .get("scr0")
            .unwrap()
            .get("max_brightness")
            .parse::<u8>()
            .unwrap_or_default())
    }
    fn get_wireless_enabled(&self) -> zbus::Result<bool> {
        Ok(service::get_wireless_enabled())
    }
    fn get_wireless_power_save(&self, device: &str) -> zbus::Result<u8> {
        let iw0 = iw::Iw::new(device);
        Ok(service::get_wireless_power_save(&iw0))
    }
    fn get_action(&self, action: &str) -> zbus::Result<String> {
        let devs = &self.devs;

        // TODO
        let iw0 = iw::Iw::new(IW_DEVICE);
        let value_by_action = |act: &str| -> String {
            let pads = act.split('-').collect::<Vec<&str>>();
            if pads.len() == 2 {
                match devs.lock().unwrap().get(pads[0]) {
                    Some(a) => a.get(pads[1]),
                    None => {
                        if act == "nm-state" {
                            service::get_nm_state().to_string()
                        } else if act == "wifi-enabled" {
                            (service::get_wireless_enabled() as u8).to_string()
                        } else if act == "wifi-power_save" {
                            iw0.get_power_save().to_string()
                        } else if act == "profile-set" {
                            service::get_profile()
                        } else if act == "profile-prev"
                            || act == "profile-next"
                            || act == "profile-editor"
                        {
                            "1".to_string()
                        } else if act == "profile-refresh" {
                            "0".to_string()
                        } else {
                            String::new()
                        }
                    }
                }
            } else {
                String::new()
            }
        };
        Ok(value_by_action(action))
    }

    fn set_action(&self, action: &str, value: &str) {
        let pats = action.split('-').collect::<Vec<&str>>();
        if pats.len() == 2 {
            let is = {
                match self.devs.lock().unwrap().get(pats[0]) {
                    Some(dev) => {
                        dev.set(pats[1], value);
                        // TODO
                        self.action(
                            &format!("other_profile:{}:", action),
                            &format!("{}:{}", action, value),
                        )
                        .expect("Failed to emit signal");
                        true
                    }
                    None => false,
                }
            };
            if !is {
                if action == "profile-set" {
                    self.set_profile(value).ok();
                } else if action == "profile-refresh" {
                    self.set_profile(&service::get_profile()).ok();
                } else if action == "wifi-enabled" {
                    service::set_wireless_enabled(value == "1");
                } else if action == "wifi-power_save" {
                    if let Ok(mode) = value.parse::<u8>() {
                        let iw0 = &iw::Iw::new(IW_DEVICE);
                        service::set_wireless_power_save(mode, iw0);
                    }
                } else {
                    eprintln!("set_action None: {} {}", action, value);
                }
            }
        }
    }

    fn set_bat_charge_control_end_threshold(&self, limit: u8) -> zbus::Result<u8> {
        let old = service::get_bat_limit(self.devs.lock().unwrap().get("bat0").unwrap());
        let set = limit;
        Ok(if old != set {
            service::set_bat_limit(set, self.devs.lock().unwrap().get("bat0").unwrap());
            let get = service::get_bat_limit(self.devs.lock().unwrap().get("bat0").unwrap());
            let mut data = Data::new();
            data.bat0_limit = set.to_string();
            data.save();
            get
        } else {
            old
        })
    }

    fn set_cpu_freq_boost(&self, boost: u8) -> zbus::Result<u8> {
        let old = service::get_cpu_boost(self.devs.lock().unwrap().get("cpu0").unwrap());
        let set = boost;
        let get = if old == set {
            set
        } else {
            service::set_cpu_boost(set, self.devs.lock().unwrap().get("cpu0").unwrap());
            service::get_cpu_boost(self.devs.lock().unwrap().get("cpu0").unwrap())
        };

        // TODO
        let value = set;
        // Profile::current()
        //     .set("cpu0-boost", &value)
        //     .save_changes();
        self.action(
            "other_profile:cpu0-boost:",
            &format!("cpu0-boost:{}", value),
        )
        .expect("Failed to emit signal");

        Ok(get)
    }
    fn set_fan_pwm_enable(&self, mode: u8) -> zbus::Result<u8> {
        let old = self
            .devs
            .lock()
            .unwrap()
            .get("fan0")
            .unwrap()
            .get("pwm_cpu")
            .parse::<u8>()
            .unwrap();
        let set = mode;
        let get = if old == set {
            set
        } else {
            self.devs
                .lock()
                .unwrap()
                .get("fan0")
                .unwrap()
                .set("pwm_cpu", &set.to_string());
            self.devs
                .lock()
                .unwrap()
                .get("fan0")
                .unwrap()
                .get("pwm_cpu")
                .parse::<u8>()
                .unwrap()
        };

        // TODO
        let value = set;
        // Profile::current()
        //     .set("fan0-pwm_cpu", &value)
        //     .save_changes();
        self.action(
            "other_profile:fan0-pwm_cpu:",
            &format!("fan0-pwm_cpu:{}", value),
        )
        .expect("Failed to emit signal");

        Ok(get)
    }
    fn set_fan_throttle_thermal_policy(&self, mode: u8) -> zbus::Result<u8> {
        let old = self
            .devs
            .lock()
            .unwrap()
            .get("fan0")
            .unwrap()
            .get("ttp")
            .parse::<u8>()
            .unwrap();
        let set = mode;
        let get = if old == set {
            set
        } else {
            self.devs
                .lock()
                .unwrap()
                .get("fan0")
                .unwrap()
                .set("ttp", &set.to_string());
            self.devs
                .lock()
                .unwrap()
                .get("fan0")
                .unwrap()
                .get("ttp")
                .parse::<u8>()
                .unwrap()
        };

        // TODO
        let value = set;
        // Profile::current()
        //     .set("fan0-ttp", &set.to_string())
        //     .save_changes();
        self.action("other_profile:fan0-ttp:", &format!("fan0-ttp:{}", value))
            .expect("Failed to emit signal");

        Ok(get)
    }
    fn set_fan_curve(&self, curve: &str) -> zbus::Result<String> {
        //let old = &service::get_fan_curve();
        let set = &curve.to_string();
        let get = set;
        if set.is_empty() {
            self.devs.lock().unwrap().get("fan0").unwrap().set(
                "ttp",
                &self.devs.lock().unwrap().get("fan0").unwrap().get("ttp"),
            );
        } else {
            fan_curve::apply_cpu(set).unwrap_or_default();
            fan_curve::apply_gpu(set).unwrap_or_default();
        }
        // TODO
        // Profile::current()
        //     .set("fan_curve", &set.to_string())
        //     .save_changes();

        Ok(get.to_string())
    }
    fn set_kbd_brightness(&self, value: u8) -> zbus::Result<u8> {
        let is = "";
        if is == "upower" {
            service::set_kbd_brightness(value);
            Ok(service::get_kbd_brightness())
        } else {
            self.devs
                .lock()
                .unwrap()
                .get("kbd0")
                .unwrap()
                .set("brightness", &value.to_string());
            Ok(self
                .devs
                .lock()
                .unwrap()
                .get("kbd0")
                .unwrap()
                .get("brightness")
                .parse::<u8>()
                .unwrap())
        }
    }
    fn set_scr_brightness(&self, value: u8) -> zbus::Result<u8> {
        self.devs
            .lock()
            .unwrap()
            .get("scr0")
            .unwrap()
            .set("brightness", &value.to_string());
        Ok(self
            .devs
            .lock()
            .unwrap()
            .get("scr0")
            .unwrap()
            .get("brightness")
            .parse::<u8>()
            .unwrap())
    }
    fn set_wireless_enabled(&self, enable: bool) -> zbus::Result<bool> {
        service::set_wireless_enabled(enable);
        Ok(service::get_wireless_enabled())
    }
    fn set_wireless_power_save(&self, device: &str, mode: u8) -> zbus::Result<u8> {
        let iw0 = &iw::Iw::new(device);
        service::set_wireless_power_save(mode, iw0);
        Ok(service::get_wireless_power_save(iw0))
    }

    fn set_signal(&self, message: &str) {
        self.signal(message).expect("Failed to emit signal");
    }
    fn set_delta(&self, delta: Vec<Vec<String>>) {
        self.delta(delta).expect("Failed to emit signal");
    }

    // Profile
    fn get_profile(&self) -> zbus::Result<String> {
        Ok(service::get_profile())
    }
    fn set_profile(&self, id: &str) -> zbus::Result<String> {
        let old_id = service::get_profile();
        service::set_profile(id, &self.devs, &iw::Iw::new(IW_DEVICE));
        let get_id = service::get_profile();

        self.signal(&format!("profile-set:{}", &get_id))
            .expect("Failed to emit signal");

        self.action(
            &format!("profile_switched:{}:", &old_id),
            &format!("set_profile:{}:", &get_id),
        )
        .expect("Failed to emit signal");

        Ok(get_id)
    }
    fn profile_data(&self, id: &str) -> zbus::Result<Vec<Vec<String>>> {
        // ProfileData
        let mut data = vec![];
        let profile = Profile::from_id(id);
        for key in PROFILE_FIELDS {
            let value = profile.get(key);
            if !value.is_empty() {
                data.push(vec![key.to_string(), value.to_string()]);
            }
        }
        Ok(data)
    }
    fn prev_profile(&self) -> zbus::Result<String> {
        let old_id = service::get_profile();
        service::prev_profile(&self.devs, &iw::Iw::new(IW_DEVICE));
        let get_id = service::get_profile();

        self.signal(&format!("profile-set:{}", &get_id))
            .expect("Failed to emit signal");

        self.action(
            &format!("profile_switched:{}:", &old_id),
            &format!("prev_profile:{}:", &get_id),
        )
        .expect("Failed to emit signal");

        Ok(get_id)
    }
    fn next_profile(&self) -> zbus::Result<String> {
        let old_id = service::get_profile();
        service::next_profile(&self.devs, &iw::Iw::new(IW_DEVICE));
        let get_id = service::get_profile();

        self.signal(&format!("profile-set:{}", &get_id))
            .expect("Failed to emit signal");

        self.action(
            &format!("profile_switched:{}:", &old_id),
            &format!("next_profile:{}:", &get_id),
        )
        .expect("Failed to emit signal");

        Ok(get_id)
    }
    fn create_profile(&self, data: Vec<Vec<String>>) -> zbus::Result<String> {
        //service::create_profile();

        let mut profile_data = HashMap::new();
        for item in data.iter() {
            profile_data.insert(item[0].to_string(), item[1].to_string());
        }

        let mut profile = Profile::current();
        let id = profile.create();
        profile.set_profile_id(&id);
        for key in PROFILE_FIELDS {
            if key == "id" {
                continue;
            }
            let value = profile_data.get(key);
            if let Some(value) = value {
                profile.set(key, value);
            }
        }
        profile.save();

        let id = profile.get("id");

        self.action(&format!("create_profile:{}:", id), "create_profile:1")
            .expect("Failed to emit signal");

        //self.signal(&format!("profile-editor:{}:", id)).ok();

        // ProfileData
        let mut data = vec![];
        let profile = Profile::from_id(&id);
        for key in PROFILE_FIELDS {
            let value = profile.get(key);
            if !value.is_empty() {
                data.push(vec![key.to_string(), value.to_string()]);
            }
        }
        self.profile(data).ok();

        Ok(profile.get("id"))
    }
    fn update_profile(&self, id: &str, data: Vec<Vec<String>>) -> zbus::Result<String> {
        let mut profile_data = HashMap::new();
        for item in data.iter() {
            profile_data.insert(item[0].to_string(), item[1].to_string());
        }

        let mut profile = Profile::from_id(id);
        for key in PROFILE_FIELDS {
            if key == "id" {
                continue;
            }
            if let Some(value) = profile_data.get(key) {
                profile.set(key, value);
            }
        }
        profile.save_changes();

        self.action(
            &format!("update_profile:{}:", profile.get("id")),
            "update_profile:1",
        )
        .expect("Failed to emit signal");

        let id = profile.get("id");
        // ProfileData
        let mut data = vec![];
        let profile = Profile::from_id(&id);
        for key in PROFILE_FIELDS {
            let value = profile.get(key);
            if !value.is_empty() {
                data.push(vec![key.to_string(), value.to_string()]);
            }
        }
        self.profile(data).ok();

        Ok(profile.get("id"))
    }
    fn remove_profile(&self, id: &str) -> zbus::Result<String> {
        //service::remove_profile();

        let mut profile = Profile::from_id(id);
        if profile.is_profile() {
            let set_id = &if profile.get_index(id) > 0 {
                profile.get_prev_id()
            } else {
                profile.get_next_id()
            };
            profile.remove(id);
            profile.set_profile_id(set_id);
            profile.save_changes();

            service::set_profile(set_id, &self.devs, &iw::Iw::new(IW_DEVICE));
        }
        let get_id = service::get_profile();
        self.action(
            &format!("remove_profile:{}:", id),
            &format!("get_profile:{}:", &get_id),
        )
        .expect("Failed to emit signal");

        let id = profile.get("id");
        // ProfileData
        let mut data = vec![];
        let profile = Profile::from_id(&id);
        for key in PROFILE_FIELDS {
            let value = profile.get(key);
            if !value.is_empty() {
                data.push(vec![key.to_string(), value.to_string()]);
            }
        }
        self.profile(data).ok();

        // None => {
        //     eprintln!("Removing profile, ID {} not found.", id);
        //     Ok(String::new())
        // }
        Ok(get_id)
    }
    fn profile_names(&self) -> zbus::Result<Vec<Vec<String>>> {
        Ok(service::profile_names())
    }

    #[dbus_interface(signal)]
    fn error(&self, message: String) -> zbus::Result<()>;

    #[dbus_interface(signal)]
    fn signal(&self, message: &str) -> zbus::Result<()>;

    #[dbus_interface(signal)]
    fn delta(&self, delta: Vec<Vec<String>>) -> zbus::Result<()>;

    #[dbus_interface(signal)]
    fn action(&self, name: &str, message: &str) -> zbus::Result<()>;

    #[dbus_interface(signal)]
    fn profile(&self, data: Vec<Vec<String>>) -> zbus::Result<()>;

    #[dbus_interface(signal)]
    fn change_fan_pwm_enable(&self, mode: u8) -> zbus::Result<()>;
}

pub(crate) fn start(devs: Arc<Mutex<BTreeMap<String, Dev>>>) {
    std::thread::spawn(move || {
        // TODO
        let mut i = 0;
        loop {
            let version = methods::get_version();
            if version.is_empty() {
                if i > 200 {
                    // TODO
                    break;
                }
                i += 1;
                std::thread::sleep(std::time::Duration::from_millis(10));
            } else {
                println!("The dbus service has started, bhm version: {}.", version);
                // TODO
                println!("loop counter: {}.", i);

                //// TODO
                //use std::path::Path;
                //let mut counter = HashMap::new();
                //let mut indexer = HashMap::new();
                //for item in DEV_TYPES {
                //    counter.insert(item.to_string(), 0);
                //}
                //let mut devices = BTreeMap::new();
                //for item in DEV_PATHS {
                //    if Path::new(item[1]).is_dir() {
                //        let count = counter.get_mut(item[0]).unwrap();
                //        let dev = Dev::new(&format!("{}{}", item[0], count), item[0], item[1]);
                //        if !dev.test().is_empty() {
                //            indexer.insert(dev.id(), devices.len());
                //            devices.insert(devices.len(), dev);
                //            *count += 1;
                //        }
                //    }
                //}
                //for (key, dev) in devices {
                //    println!("{} {} {} {:?}", key, dev.id(), dev.typ(), dev.test());
                //}

                break;
            }
        }
    });

    std::thread::spawn(move || {
        let conn = zbus::Connection::new_system().unwrap();
        let client = ClientNMProxy::new_for_path(&conn, "/org/freedesktop/NetworkManager").unwrap();
        client
            .connect_state_changed(move |state| {
                methods::set_signal(&format!("nm-state:{}", state));
                Ok(())
            })
            .unwrap();
        client
            .connect_properties_changed(move |properties| {
                if let Some(value) = properties.get("WirelessEnabled") {
                    methods::set_signal(&format!(
                        "wifi-enabled:{}",
                        bool::try_from(value).unwrap_or_default() as u8
                    ));
                }
                Ok(())
            })
            .unwrap();

        let mut receiver = zbus::SignalReceiver::new(conn);
        receiver.receive_for(&client);

        loop {
            receiver.next_signal().unwrap();
        }
    });

    let uid = get_current_uid();
    let connection = if uid == 0 {
        zbus::Connection::new_system().unwrap()
    } else {
        zbus::Connection::new_session().unwrap()
    };

    println!(
        "Starting the dbus service in {}.",
        if uid == 0 { "system" } else { "session" }
    );

    fdo::DBusProxy::new(&connection)
        .unwrap()
        .request_name(IN_NAME, fdo::RequestNameFlags::ReplaceExisting.into())
        .unwrap();

    //std::thread::spawn(move || {
    let mut object_server = zbus::ObjectServer::new(&connection);

    let iface = IFace {
        devs: Arc::clone(&devs),
    };

    // object_server.with(Some(IN_PATH), |iface: &IFace| {
    //     iface.emit_signal()
    // }).ok();

    if let Ok(is) = object_server.at(&IN_PATH.try_into().unwrap(), iface) {
        if is {
            object_server
                .with(&IN_PATH.try_into().unwrap(), |iface: &IFace| {
                    iface.signal("test")
                })
                .ok();
            //ObjectServer::local_node_emit_signal(
            //    Some(IN_NAME),
            //    IN_FACE,
            //    "signal",
            //    &("test2")
            //);

            loop {
                if let Err(err) = object_server.try_handle_next() {
                    eprintln!("{}", err);
                }
            }
        }
    }
}

// pub fn start(path_fan: String) -> Result<(), Box<dyn Error>> {
