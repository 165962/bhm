use std::fs;
use std::process::Command;
use users::get_current_uid;

pub enum Evt {
    Tick(),
    RunLevel(u8),
    SetAction(String, String),
}

const PKEXEC: &str = "/usr/bin/pkexec";

pub(crate) fn exec(command: &str) -> String {
    let output = Command::new("sh")
        .arg("-c")
        .arg(command)
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
    if output.status.success() {
        String::from_utf8_lossy(&output.stdout).to_string()
    } else {
        let stderr = String::from_utf8_lossy(&output.stderr);
        eprintln!("stderr: {}", stderr);
        String::new()
    }
}

pub(crate) fn root_exec(command: &str) -> String {
    exec(&format!(
        "{}{}",
        if get_current_uid() == 0 {
            String::new()
        } else {
            format!("{} ", PKEXEC)
        },
        command
    ))
}

pub(crate) fn root_file_write(path: &str, content: &str) -> String {
    exec(&format!(
        "echo {} | {}tee {}",
        content,
        if get_current_uid() == 0 {
            String::new()
        } else {
            format!("{} ", PKEXEC)
        },
        path
    ))
}

pub fn read_file_to_string(path: &str) -> String {
    match fs::read_to_string(path) {
        Ok(x) => x.trim().to_string(),
        Err(err) => {
            //Err(std::io::Error::new(std::io::ErrorKind::Other, "Could not parse float")),
            eprintln!("Error read file {}. {}.", path, err);
            String::from("")
        }
    }
}

pub fn min_max_u8(value: u8, min: u8, max: u8) -> u8 {
    if value < min {
        min
    } else if value > max {
        max
    } else {
        value
    }
}
