use crate::inotify::Inotify;
use crate::utility::exec;
use crate::{
    config::{Data, Profile},
    dbus,
    devices::Dev,
    fan_curve, iw, methods, Evt, DEFAULT_STR, DEV_PATHS, DEV_TYPES,
};
use std::collections::BTreeMap;
use std::path::Path;
use std::{
    collections::HashMap,
    convert::TryFrom,
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc, Mutex,
    },
    thread,
};
use users::get_current_uid;
use zbus::Error;

pub(crate) struct Service {
    tx: Sender<Evt>,
    rx: Receiver<Evt>,
    ms: u128,
    now: std::time::Instant,
    iw0: iw::Iw,
    cur: HashMap<String, String>,
    acts: Vec<String>,
    devs: Arc<Mutex<BTreeMap<String, Dev>>>,
    delta: HashMap<String, String>,
    is_delta: bool,
    profile: Profile,
}

impl Service {
    pub(crate) fn new(
        devs: Arc<Mutex<BTreeMap<String, Dev>>>,
        profile: Profile,
        iw0: iw::Iw,
    ) -> Self {
        let (tx, rx) = channel();
        let acts = vec![
            "ac0-online".to_string(),
            "bat0-charge".to_string(),
            "bat0-health".to_string(),
            "bat0-voltage".to_string(),
            "fan0-rpm_cpu".to_string(),
            //"kbd0-brightness",
            //"scr0-brightness",
            //"wireless_enabled",
            "wifi-power_save".to_string(),
        ];
        let cur = HashMap::new();
        //for act in acts.iter() {
        //    cur.insert(act.to_string(), String::new());
        //}

        Self {
            rx,
            tx,
            ms: 0,
            now: std::time::Instant::now(),
            iw0,
            cur,
            acts,
            devs,
            delta: HashMap::new(),
            profile,
            is_delta: false,
        }
    }

    fn runlevel(&self) {
        let tx = self.tx.clone();
        let mut i = 0;
        let mut old = 0;
        thread::spawn(move || {
            let sleep_time = std::time::Duration::from_millis(1000);
            while i < 60 {
                let out = exec("runlevel");
                let lvl = if out.is_empty() {
                    0
                } else {
                    out[2..3].parse::<u8>().unwrap_or(0)
                };
                if old != lvl {
                    println!("Run level {}", lvl);
                    tx.send(Evt::RunLevel(lvl)).ok();
                    if lvl > 2 {
                        break;
                    }
                    old = lvl;
                }
                i += 1;
                thread::sleep(sleep_time);
            }
        });
    }
    fn devices(&self) {
        let mut counter = HashMap::new();
        let mut indexer = HashMap::new();
        //let devs = Arc::new(Mutex::new(HashMap::new()));
        for item in DEV_TYPES {
            counter.insert(item.to_string(), 0);
        }
        self.devs.lock().unwrap().clear();
        for item in DEV_PATHS {
            // TODO
            if Path::new(item[1]).is_dir() {
                let count = counter.get_mut(item[0]).unwrap();
                let dev = Dev::new(&format!("{}{}", item[0], count), item[0], item[1]);
                if !dev.paths().is_empty() {
                    indexer.insert(dev.id(), self.devs.lock().unwrap().len());
                    //devices.insert(devices.len(), dev);
                    self.devs.lock().unwrap().insert(dev.id().to_string(), dev);
                    *count += 1;
                }
            }
        }
        for (_key, dev) in self.devs.lock().unwrap().iter() {
            println!("{} {} {:?}", dev.id(), dev.typ(), dev.paths());
        }
    }
    fn inotify(&self) {
        // TODO block
        let mut data_inotify: Vec<Vec<String>> = vec![];
        let mut inotify_keys_by_type = HashMap::new();
        inotify_keys_by_type.insert("ac".to_string(), vec!["online"]);
        inotify_keys_by_type.insert("bat".to_string(), vec!["limit"]);
        inotify_keys_by_type.insert("cpu".to_string(), vec!["boost"]);
        inotify_keys_by_type.insert("fan".to_string(), vec!["ttp", "pwm_cpu"]);
        inotify_keys_by_type.insert("kbd".to_string(), vec!["brightness"]);
        inotify_keys_by_type.insert("scr".to_string(), vec!["brightness"]);
        for (_id, dev) in self.devs.lock().unwrap().iter() {
            if let Some(keys) = inotify_keys_by_type.get(&dev.typ()) {
                for key in keys {
                    if let Some(path) = dev.path_by_key(key) {
                        data_inotify.push(vec![path.to_string(), format!("{}-{}", dev.id(), key)]);
                    }
                }
            }
        }
        let mut inotify = Inotify::new();
        inotify.data_new(data_inotify);
        inotify.start(self.tx());
    }
    pub(crate) fn init(&self) {
        if get_current_uid() == 0 {
            self.runlevel();
        }
        for item in self.profile.get_actions().iter() {
            let (id, key, act, set) = if item.0 == "cpu0-boost" {
                ("cpu0", "boost", "cpu0-boost", item.1.to_owned())
            } else if item.0 == "fan0-pwm_cpu" {
                ("fan0", "pwm_cpu", "fan0-pwm_cpu", item.1.to_owned())
            } else if item.0 == "fan0-ttp" {
                ("fan0", "ttp", "fan0-ttp", item.1.to_owned())
            } else {
                ("", "", "", String::new())
            };
            if !id.is_empty() {
                //let set = profile.get(act);
                if !set.is_empty() {
                    let old = self.devs.lock().unwrap().get(id).unwrap().get(key);
                    if set != old {
                        self.devs.lock().unwrap().get(id).unwrap().set(key, &set);
                        let get = self.devs.lock().unwrap().get(id).unwrap().get(key);
                        println!("{}: {}, {} -> {}.", act, get, old, set);
                    }
                }
            }
        }
        //
        let key = "fan_curve";
        let set = &self.profile.get(key);
        let old = DEFAULT_STR; //let old = TODO &service::get_fan_curve();
        if set != old {
            set_fan_curve(set);
            let get = get_fan_curve();
            println!("{}: {}, {} -> {}.", key, get, old, set);
        }
        //
        // TODO init Network Manager
        //let key = "wireless_enabled";
        //let set = profile.get(key).unwrap() == "true";
        //let old = service::get_wireless_enabled();
        //if set != old {
        //    service::set_wireless_enabled(set);
        //    let get = service::get_wireless_enabled();
        //    println!("{}: {}, {} -> {}.", key, get, old, set);
        //}
        //
        // TODO init Network Manager
        //let key = "wlp2s0+power_save";
        //let set = if profile.get(key).unwrap() == "true" {
        //    1u8
        //} else {
        //    0u8
        //};
        //let old = service::get_wireless_power_save(&iw0);
        //if set != old {
        //    service::set_wireless_power_save(set, &iw0);
        //    let get = service::get_wireless_power_save(&iw0);
        //    println!("{}: {}, {} -> {}.", key, get, old, set);
        //}
        //
    }
    pub(crate) fn start(&mut self) {
        for act in self.acts.iter() {
            self.cur.insert(act.to_string(), self.value_by_act(act));
        }

        self.timer();

        // DBUS server
        let devs1 = Arc::clone(&self.devs);
        thread::spawn(move || {
            dbus::start(devs1);
        });

        while let Ok(msg) = self.rx.recv() {
            match msg {
                Evt::Tick() => {
                    self.tick();
                    if !self.delta.is_empty() {
                        let mut set = vec![];
                        for item in self.delta.iter() {
                            set.push(vec![item.0.to_string(), item.1.to_string()]);
                        }
                        methods::set_delta(&set);
                        self.delta.clear();
                    }
                }
                Evt::RunLevel(level) => {
                    if level > 0 {
                        self.devices();
                        self.inotify();
                    }
                }
                Evt::SetAction(action, value) => {
                    if self.is_delta {
                        match self.delta.get_mut(&action) {
                            Some(item) => *item = value,
                            None => {
                                self.delta.insert(action, value);
                            }
                        }
                    } else {
                        methods::set_signal(&format!("{}:{}", action, value));
                    }
                }
            }
        }
    }

    pub(crate) fn tx(&self) -> Sender<Evt> {
        self.tx.clone()
    }

    fn tick(&mut self) {
        let tm = self.now.elapsed().as_millis() / 1000;
        if tm != self.ms {
            for act in self.acts.iter() {
                let value = self.value_by_act(act);
                if *self.cur.get(act).unwrap() != value {
                    *self.cur.get_mut(act).unwrap() = value;
                    self.tx
                        .send(Evt::SetAction(
                            act.to_string(),
                            self.cur.get(act).unwrap().to_string(),
                        ))
                        .ok();
                }
            }
            //println!("millis {}", tm);
            //println!("seconds {}", (std::time::Instant::now() - self.now).as_secs());
            self.ms = tm;
        }
    }

    fn value_by_act(&self, act: &str) -> String {
        let (id, key) = if act == "ac0-online" {
            ("ac0", "online")
        } else if act == "bat0-charge" {
            ("bat0", "charge")
        } else if act == "bat0-health" {
            ("bat0", "health")
        } else if act == "bat0-voltage" {
            ("bat0", "voltage")
        } else if act == "fan0-rpm_cpu" {
            ("fan0", "rpm_cpu")
        } else if act == "kbd0-brightness" {
            ("kbd0", "brightness")
        } else if act == "scr0-brightness" {
            ("scr0", "brightness")
        } else {
            ("", "")
        };
        if !id.is_empty() {
            self.devs.lock().unwrap().get(id).unwrap().get(key)
        } else if act == "wifi-enabled" {
            // TODO Listening through a non-persistent dbus connection loads the CPU.
            (get_wireless_enabled() as u8).to_string()
        } else if act == "wifi-power_save" {
            self.iw0.get_power_save().to_string()
        } else {
            String::new()
        }
    }

    fn timer(&self) {
        let tx = self.tx.clone();
        thread::spawn(move || {
            let sleep_time = std::time::Duration::from_millis(500);
            loop {
                thread::sleep(sleep_time);
                tx.send(Evt::Tick()).ok();
            }
        });
    }
}

pub(crate) fn get_bat_limit(bat0: &Dev) -> u8 {
    bat0.get("limit").parse::<u8>().unwrap_or_default()
}

pub(crate) fn get_cpu_boost(cpu0: &Dev) -> u8 {
    cpu0.get("boost").parse::<u8>().unwrap_or_default()
}

pub(crate) fn get_fan_pwm(fan0: &Dev) -> u8 {
    fan0.get("pwm_cpu").parse::<u8>().unwrap()
}

pub(crate) fn get_fan_ttp(fan0: &Dev) -> u8 {
    fan0.get("ttp").parse::<u8>().unwrap()
}

pub(crate) fn get_fan_curve() -> String {
    let profile = Profile::current();
    profile.get("fan_curve")
}

pub(crate) fn get_kbd_brightness() -> u8 {
    match match zbus::Connection::new_system() {
        Ok(connection) => match connection.call_method(
            Some("org.freedesktop.UPower"),
            "/org/freedesktop/UPower/KbdBacklight",
            Some("org.freedesktop.UPower.KbdBacklight"),
            "GetBrightness",
            &(),
        ) {
            Ok(message) => match Some(message) {
                Some(message) => match message.body::<i32>() {
                    Ok(val) => Ok(val),
                    Err(err) => Err(Error::from(err)),
                },
                None => Ok(0),
            },
            Err(err) => Err(err),
        },
        Err(err) => Err(err),
    } {
        Ok(val) => val as u8,
        Err(err) => {
            eprintln!("Error {:?}", err);
            0
        }
    }
}

pub(crate) fn get_kbd_max_brightness() -> u8 {
    match match zbus::Connection::new_system() {
        Ok(connection) => match connection.call_method(
            Some("org.freedesktop.UPower"),
            "/org/freedesktop/UPower/KbdBacklight",
            Some("org.freedesktop.UPower.KbdBacklight"),
            "GetMaxBrightness",
            &(),
        ) {
            Ok(message) => match Some(message) {
                Some(message) => match message.body::<i32>() {
                    Ok(body) => Ok(body),
                    Err(err) => Err(err.to_string()),
                },
                None => Err("Err".to_string()),
            },
            Err(err) => Err(err.to_string()),
        },
        Err(err) => Err(err.to_string()),
    } {
        Ok(value) => value as u8,
        Err(err) => {
            eprintln!("Error {:?}", err);
            0
        }
    }
}

pub(crate) fn get_network_manager_version() -> String {
    match match zbus::Connection::new_system() {
        Ok(connection) => match connection.call_method(
            Some("org.freedesktop.NetworkManager"),
            "/org/freedesktop/NetworkManager",
            Some("org.freedesktop.DBus.Properties"),
            "Get",
            &("org.freedesktop.NetworkManager", "Version"),
        ) {
            Ok(message) => match message.body::<zbus::export::zvariant::Value<'_>>() {
                Ok(value) => match String::try_from(value) {
                    Ok(version) => Ok(version),
                    Err(err) => Err(err.to_string()),
                },
                Err(err) => Err(err.to_string()),
            },
            Err(err) => Err(err.to_string()),
        },
        Err(err) => Err(err.to_string()),
    } {
        Ok(value) => value,
        Err(err) => {
            eprintln!("Error {}", err);
            String::new()
        }
    }
}

pub(crate) fn get_nm_state() -> u32 {
    match match zbus::Connection::new_system() {
        Ok(connection) => match connection.call_method(
            Some("org.freedesktop.NetworkManager"),
            "/org/freedesktop/NetworkManager",
            Some("org.freedesktop.NetworkManager"),
            "state",
            &(),
        ) {
            Ok(message) => match message.body::<u32>() {
                Ok(value) => Ok(value),
                Err(err) => Err(Error::from(err)),
            },
            Err(err) => Err(err),
        },
        Err(err) => Err(err),
    } {
        Ok(value) => value,
        Err(err) => {
            eprintln!("Error {}", err);
            0
        }
    }
}

pub(crate) fn get_wireless_enabled() -> bool {
    match match zbus::Connection::new_system() {
        Ok(connection) => match connection.call_method(
            Some("org.freedesktop.NetworkManager"),
            "/org/freedesktop/NetworkManager",
            Some("org.freedesktop.DBus.Properties"),
            "Get",
            &("org.freedesktop.NetworkManager", "WirelessEnabled"),
        ) {
            Ok(message) => match message.body::<zbus::export::zvariant::Value<'_>>() {
                Ok(value) => match bool::try_from(value) {
                    Ok(value) => Ok(value),
                    Err(err) => Err(Error::from(err)),
                },
                Err(err) => Err(Error::from(err)),
            },
            Err(err) => Err(err),
        },
        Err(err) => Err(err),
    } {
        Ok(value) => value,
        Err(err) => {
            eprintln!("Error {}", err);
            false
        }
    }
}

pub(crate) fn get_wireless_power_save(iw0: &iw::Iw) -> u8 {
    iw0.get_power_save()
}

pub(crate) fn set_cpu_boost(boost: u8, cpu0: &Dev) {
    cpu0.set("boost", &boost.to_string());
}

// pub(crate) fn set_fan_pwm(mode: u8, fan0: &Dev) {
//     fan0.set("pwm_cpu", &mode.to_string());
// }

// pub(crate) fn set_fan_ttp(mode: u8, fan0: &Dev) {
//     fan0.set("ttp", &mode.to_string())
// }

pub(crate) fn set_fan_curve(curve: &str) {
    match fan_curve::apply_cpu(curve) {
        Ok(_) => {}
        Err(err) => eprintln!("{}", err),
    };
    match fan_curve::apply_gpu(curve) {
        Ok(_) => {}
        Err(err) => eprintln!("{}", err),
    };
}

pub(crate) fn set_bat_limit(limit: u8, bat0: &Dev) {
    bat0.set("limit", &limit.to_string());
}

pub(crate) fn set_kbd_brightness(value: u8) {
    match match zbus::Connection::new_system() {
        Ok(connection) => match connection.call_method(
            Some("org.freedesktop.UPower"),
            "/org/freedesktop/UPower/KbdBacklight",
            Some("org.freedesktop.UPower.KbdBacklight"),
            "SetBrightness",
            &(value as i32),
        ) {
            Ok(_message) => Ok(()),
            Err(err) => Err(err),
        },
        Err(err) => Err(err),
    } {
        Ok(value) => value,
        Err(err) => {
            eprintln!("Error {}", err);
        }
    }
}

pub(crate) fn set_wireless_enabled(enable: bool) {
    let connection = zbus::Connection::new_system().unwrap();
    let message = connection.call_method(
        Some("org.freedesktop.NetworkManager"),
        "/org/freedesktop/NetworkManager",
        Some("org.freedesktop.DBus.Properties"),
        "Set",
        &(
            "org.freedesktop.NetworkManager",
            "WirelessEnabled",
            zbus::export::zvariant::Value::Bool(enable),
        ),
    );
    match message {
        Ok(_message) => {} // println!("{:?}", message.fields()),
        Err(error) => eprintln!("Error {:?}", error),
    }
}

pub(crate) fn set_wireless_power_save(mode: u8, iw0: &iw::Iw) {
    iw0.set_power_save(mode);
}

// Profile

pub(crate) fn get_profile() -> String {
    Data::new().profile_id
}

pub(crate) fn set_profile(id: &str, devs: &Arc<Mutex<BTreeMap<String, Dev>>>, iw0: &iw::Iw) {
    let mut profile = Profile::current();
    if profile.set_profile_id(id) {
        match profile.get("cpu0-boost").parse::<u8>() {
            Ok(value) => devs
                .lock()
                .unwrap()
                .get("cpu0")
                .unwrap()
                .set("boost", &value.to_string()),
            Err(err) => eprintln!("{}", err),
        };

        match profile.get("fan0-pwm_cpu").parse::<u8>() {
            Ok(value) => devs
                .lock()
                .unwrap()
                .get("fan0")
                .unwrap()
                .set("pwm_cpu", &value.to_string()),
            Err(err) => eprintln!("{}", err),
        };

        match profile.get("fan0-ttp").parse::<u8>() {
            Ok(value) => devs
                .lock()
                .unwrap()
                .get("fan0")
                .unwrap()
                .set("ttp", &value.to_string()),
            Err(err) => eprintln!("{}", err),
        };

        let value = profile.get("fan_curve");
        if value.is_empty() {
            let value = devs.lock().unwrap().get("fan0").unwrap().get("ttp");
            devs.lock().unwrap().get("fan0").unwrap().set("ttp", &value);
        } else {
            match fan_curve::apply_cpu(&value) {
                Ok(_) => {}
                Err(err) => eprintln!("CPU curve error: {}", err),
            };
            match fan_curve::apply_gpu(&value) {
                Ok(_) => {}
                Err(err) => eprintln!("GPU curve error: {}", err),
            };
        }

        match profile.get("kbd0-brightness").parse::<u8>() {
            Ok(value) => devs
                .lock()
                .unwrap()
                .get("kbd0")
                .unwrap()
                .set("brightness", &value.to_string()),
            Err(err) => eprintln!("{}", err),
        };

        match profile.get("scr0-brightness").parse::<u8>() {
            Ok(value) => devs
                .lock()
                .unwrap()
                .get("scr0")
                .unwrap()
                .set("brightness", &value.to_string()),
            Err(err) => eprintln!("{}", err),
        };

        match profile.get("wifi-power_save").parse::<u8>() {
            Ok(value) => iw0.set_power_save(value),
            Err(err) => eprintln!("{}", err),
        };

        profile.save();
    }
}

pub(crate) fn prev_profile(devs: &Arc<Mutex<BTreeMap<String, Dev>>>, iw0: &iw::Iw) {
    let profile = Profile::current();
    let id = &profile.get_prev_id();
    if id.is_empty() {
        eprintln!("Prev profile, ID not found.");
    } else {
        set_profile(id, devs, iw0);
    }
}

pub(crate) fn next_profile(devs: &Arc<Mutex<BTreeMap<String, Dev>>>, iw0: &iw::Iw) {
    let profile = Profile::current();
    let id = &profile.get_next_id();
    if id.is_empty() {
        eprintln!("Next profile, ID not found.");
    } else {
        set_profile(id, devs, iw0);
    }
}

pub(crate) fn profile_names() -> Vec<Vec<String>> {
    let mut names: Vec<Vec<String>> = vec![];
    for item in Data::new().profiles {
        match item.get("id") {
            Some(id) => {
                if id.is_empty() {
                    eprintln!("Profile id empty.")
                } else {
                    names.push(vec![
                        id.to_string(),
                        item.get("name").unwrap_or(DEFAULT_STR).to_string(),
                    ]);
                }
            }
            None => eprintln!("Profile, id property not found."),
        }
    }
    names
}
