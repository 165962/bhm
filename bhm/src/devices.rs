use crate::fan_curve;
use crate::utility::{read_file_to_string, root_file_write};
use std::collections::HashMap;
use std::fs;

//pub(crate) const DEV_AC: u8 = 0;
//pub(crate) const DEV_BAT: u8 = 1;
//pub(crate) const DEV_CPU: u8 = 2;
//pub(crate) const DEV_FAN: u8 = 3;
//pub(crate) const DEV_KBD: u8 = 4;
//pub(crate) const DEV_SCR: u8 = 5;
//pub(crate) const DEV_WRL: u8 = 6;
//pub(crate) const DEV_BLT: u8 = 7;

pub(crate) const DEV_TYPES: [&str; 8] = ["ac", "bat", "cpu", "fan", "kbd", "scr", "wrl", "blt"];
pub(crate) const DEV_PATHS: [[&str; 2]; 6] = [
    ["ac", "/sys/class/power_supply/AC0"],          // dir
    ["bat", "/sys/class/power_supply/BAT0"],        // dir
    ["cpu", "/sys/devices/system/cpu"],             // dir
    ["fan", "/sys/devices/platform/asus-nb-wmi"],   // dir
    ["kbd", "/sys/class/leds/asus::kbd_backlight"], // dir
    ["scr", "/sys/class/backlight/amdgpu_bl0"],     // dir
];
pub(crate) const IW_DEVICE: &str = "wlp2s0"; // name

pub(crate) const AC_PATHS: [[&str; 2]; 1] = [
    ["online", "/online"], // u8 0, 1
];
pub(crate) const BAT_PATHS: [[&str; 2]; 7] = [
    ["limit", "/charge_control_end_threshold"], // u8 20 - 100
    ["power_now", "/power_now"],                // uW
    ["voltage", "/voltage_now"],                // mV
    ["capacity", "/capacity"],                  // ?
    ["energy_now", "/energy_now"],              // ?
    ["energy_full", "/energy_full"],            // ?
    ["energy_full_design", "/energy_full_design"], // ?
];
pub(crate) const CPU_PATHS: [[&str; 2]; 1] = [
    ["boost", "/cpufreq/boost"], // u8 0, 1
];
const FAN_PATHS: [[&str; 2]; 1] = [
    ["ttp", "/throttle_thermal_policy"], // u8 0 - 2
];
const FAN_ASUS_CPU_PATHS: [[&str; 2]; 2] = [
    ["pwm_cpu", "/pwm1_enable"], // u8 0, 2
    ["rpm_cpu", "/fan1_input"],  // u16 rpm
];
const KBD_PATHS: [[&str; 2]; 2] = [
    ["brightness", "/brightness"],         // u8 0 - 3
    ["max_brightness", "/max_brightness"], // u8 0 - 3
];
const SCR_PATHS: [[&str; 2]; 2] = [
    ["brightness", "/brightness"],         // u8 0 - 255
    ["max_brightness", "/max_brightness"], // u8 0 - 255
];

pub(crate) struct Dev {
    id: String,
    typ: String,
    paths: HashMap<String, String>,
}

impl Dev {
    pub(crate) fn new(id: &str, typ: &str, path: &str) -> Self {
        // TODO
        let paths_vec = if typ == "ac" {
            AC_PATHS.to_vec()
        } else if typ == "bat" {
            BAT_PATHS.to_vec()
        } else if typ == "cpu" {
            CPU_PATHS.to_vec()
        } else if typ == "fan" {
            FAN_PATHS.to_vec()
        } else if typ == "kbd" {
            KBD_PATHS.to_vec()
        } else if typ == "scr" {
            SCR_PATHS.to_vec()
        } else {
            vec![]
        };

        let mut paths = HashMap::new();
        for item in paths_vec {
            let item_path = path.to_string() + item[1];
            if fs::read_to_string(item_path.to_string()).is_ok() {
                paths.insert(item[0].to_string(), item_path);
            }
        }

        // TODO PATH FAN ASUS
        if typ == "fan" {
            if let Ok(read_dir) = fs::read_dir(format!("{}/hwmon", path)) {
                for dir_entry in read_dir.flatten() {
                    let path_buf = dir_entry.path();
                    if path_buf.is_dir() {
                        let hwmon_n_path = path_buf.to_string_lossy().to_string();
                        let name_path = hwmon_n_path.to_string() + "/name";
                        if let Ok(name) = fs::read_to_string(name_path) {
                            if name.trim() == "asus" {
                                for item in FAN_ASUS_CPU_PATHS {
                                    let item_path = hwmon_n_path.to_string() + item[1];
                                    if fs::read_to_string(item_path.to_string()).is_ok() {
                                        paths.insert(item[0].to_string(), item_path);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        Self {
            id: id.to_string(),
            typ: typ.to_string(),
            paths,
        }
    }

    pub(crate) fn id(&self) -> String {
        self.id.to_string()
    }
    pub(crate) fn typ(&self) -> String {
        self.typ.to_string()
    }

    pub(crate) fn get(&self, key: &str) -> String {
        match self.paths.get(key) {
            Some(path) => read_file_to_string(path),
            None => {
                if &self.typ == "bat" && key == "charge" {
                    self.get_bat_charge().to_string()
                } else if &self.typ == "bat" && key == "health" {
                    self.get_bat_health().to_string()
                } else if self.typ == *"fan" && key == "curve" {
                    // TODO
                    String::new()
                } else {
                    eprintln!("Dev get none path property {}", key);
                    String::new()
                }
            }
        }
    }

    pub(crate) fn set(&self, key: &str, value: &str) {
        match self.paths.get(key) {
            Some(path) => {
                root_file_write(path, value);
            }
            None => {
                if self.typ == *"fan" && key == "curve" {
                    if value.is_empty() {
                        let ttp = self.get("ttp");
                        self.set("ttp", &ttp);
                    } else {
                        fan_curve::apply_cpu(value).ok();
                        fan_curve::apply_gpu(value).ok();
                    }
                } else {
                    eprintln!("Dev set none path property {}", key);
                }
            }
        }
    }

    pub(crate) fn path_by_key(&self, key: &str) -> Option<&String> {
        self.paths.get(key)
    }

    pub(crate) fn paths(&self) -> HashMap<String, String> {
        self.paths.clone()
    }

    pub fn get_bat_charge(&self) -> f32 {
        let now = self.get("energy_now");
        let full = self.get("energy_full");
        if now.is_empty() || full.is_empty() {
            self.get("capacity").parse::<f32>().unwrap_or(0.)
        } else {
            now.parse::<f32>().unwrap() / full.parse::<f32>().unwrap() * 100.0
        }
    }

    pub fn get_bat_health(&self) -> f32 {
        let full = self.get("energy_full");
        let design = self.get("energy_full_design");
        if full.is_empty() || design.is_empty() {
            0.
        } else {
            full.parse::<f32>().unwrap() / design.parse::<f32>().unwrap() * 100.0
        }
    }
}
