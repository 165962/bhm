use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub(crate) struct ScanDir {
    pub(crate) level: i8,
    pub(crate) data: HashMap<String, HashMap<String, String>>,
}

impl ScanDir {
    pub(crate) fn new() -> Self {
        Self {
            level: 2,
            data: HashMap::new(),
        }
    }

    pub(crate) fn scan(&mut self, path: &str) {
        self._scan(path, 1);
    }

    fn _scan(&mut self, path: &str, level: i8) {
        if level <= self.level {
            let paths = fs::read_dir(path).unwrap();
            for dir_entry in paths {
                let path_buf = dir_entry.unwrap().path();
                let p = format!("{}", path_buf.display());
                if path_buf.is_dir() {
                    //println!("Dir {} {}", level, p);
                    self._scan(&p, level + 1);
                } else if path_buf.is_file() {
                    let file_name = path_buf.file_name().unwrap().to_string_lossy();
                    self._file(path, &p, &file_name).ok();
                }
            }
        }
    }

    fn _file(&mut self, path: &str, p: &str, file_name: &str) -> Result<(), Box<dyn Error>> {
        // if file_name == "name" {
        //     let name = fs::read_to_string(p).unwrap().trim().to_string();
        //     if name == "asus" {
        //         let path_rpm = format!("{}/{}", path, "/fan1_input");
        //         let rpm = fs::read_to_string(path_rpm).unwrap();
        //         println!("path: {}, name: {}, rpm: {}.", path, name, rpm.trim());
        //     }
        // }
        let v = fs::read_to_string(&p)?.trim().to_string();
        if !v.is_empty() {
            let k = path.to_string();
            let list: &mut HashMap<String, String>;
            if !self.data.contains_key(&k) {
                self.data.insert(path.to_string(), HashMap::new());
            }
            list = self.data.get_mut(&k).unwrap();
            list.insert(file_name.to_string(), v);
        }
        Ok(())
    }
}
