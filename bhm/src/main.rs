use crate::devices::DEV_TYPES;
use crate::{
    config::{Data, Profile},
    devices::{Dev, DEV_PATHS, IW_DEVICE},
    utility::{min_max_u8, Evt},
};
use clap::{App, Arg};
use std::collections::{BTreeMap, HashMap};
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::{env, process::exit, sync::mpsc::channel, thread};
use users::{get_current_gid, get_current_uid, get_user_by_uid};

mod config;
mod dbus;
mod devices;
mod fan_curve;
mod inotify;
mod methods;
mod monitor;
//mod scanner;
mod service;
mod utility;

mod iw;

//use std::sync::{Arc, atomic::AtomicBool, atomic::{AtomicBool, Ordering}}};

const VERSION: &str = env!("CARGO_PKG_VERSION");
//const HW_PATH: &str = "/sys/class/hwmon";

const DEFAULT_STR: &String = &String::new();
const DEFAULT_CHARGE_LIMIT: u8 = 70;

const DEFAULT_ARGS: [[&str; 3]; 7] = [
    //vec!["monitor", ""],
    ["bat-charge-limit", "bat0-limit", "Integer: 20-100"],
    ["cpu-boost", "cpu0-boost", "Boolean"],
    ["fan-curve", "fan_curve", "String: fan curve"],
    ["fan-pwm-cpu", "fan0-pwm_cpu", "Boolean"],
    ["fan-ttp", "fan0-ttp", "INTEGER: 0, 2"],
    ["wifi-enabled", "wifi-enabled", "Boolean"],
    ["wifi-power-save", "wifi-power_save", "Boolean"],
];

fn main() {
    let mut args: Vec<Arg> = vec![];
    for item in DEFAULT_ARGS.iter() {
        args.push(
            Arg::new(item[0])
                .long(item[0])
                .required(false)
                .takes_value(true)
                .value_name(item[2]),
        );
    }
    let matches = App::new("BAS HWMon")
        .version(VERSION)
        .arg(
            Arg::new("version")
                .short('v')
                .long("version")
                .value_name("Version")
                .about("Print version information")
                .required(false)
                .takes_value(false),
        )
        .arg(
            Arg::new("next")
                .short('n')
                .long("next")
                .value_name("Next")
                .about("Next profile")
                .required(false)
                .takes_value(false),
        )
        .arg(
            Arg::new("monitor")
                .short('m')
                .long("monitor")
                .value_name("Monitor")
                .about("Mini monitoring")
                .required(false)
                .takes_value(false),
        )
        //.arg(Arg::new("cpu-freq-boost").long("cpu-freq-boost").required(false)
        //    .takes_value(false).value_name("CPU freq boost")
        //)
        .args(args)
        .get_matches();

    if matches.is_present("next") {
        methods::next_profile();
        exit(0)
    }

    let is_service = env::var("IS_SERVICE").unwrap_or_default() == "1";
    let is_monitor = matches.is_present("monitor");

    let mut data = Data::new();
    let mut profile = Profile::current();

    let uid = get_current_uid();
    let user = match get_user_by_uid(uid) {
        Some(user) => user.name().to_string_lossy().to_string(),
        None => String::new(),
    };
    println!("Hello, {}! uid: {}, gid: {}.", user, uid, get_current_gid());
    //let args = Args::parse();
    //use std::os::unix::fs::MetadataExt;
    //let m = std::fs::metadata("/proc/self");
    //println!("metadata for {:?}", m.map(|m| m.uid()));
    ////////////////////////////////////////////////////////////////////////////////////////////////
    if is_service {
        // $ cat /proc/sys/kernel/random/uuid
        // 88b4d4b2-41d1-465e-b415-cbe649c1bb32
        // $ cat /proc/sys/kernel/random/boot_id
        // 50b0e3d3-b8f5-4133-9f44-990d957a0fb7
        println!("IS_SERVICE");
        let (id, key, act, min, max) = ("bat0", "limit", "bat0-limit", 20, 100);
        let dev = Dev::new(id, "bat", DEV_PATHS[1][1]);
        let old = dev.get(key);
        let set = match data.bat0_limit.parse::<u8>() {
            Ok(limit) => min_max_u8(limit, min, max),
            Err(_err) => DEFAULT_CHARGE_LIMIT,
        }
        .to_string();
        if old != set {
            dev.set(key, &set);
            let get = dev.get(key);
            println!("{}: {}, {} -> {}.", act, get, old, set);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    let mut counter = HashMap::new();
    let mut indexer = HashMap::new();
    let devs = Arc::new(Mutex::new(BTreeMap::new()));
    for item in DEV_TYPES {
        counter.insert(item.to_string(), 0);
    }
    for item in DEV_PATHS {
        // TODO
        if Path::new(item[1]).is_dir() {
            let count = counter.get_mut(item[0]).unwrap();
            let dev = Dev::new(&format!("{}{}", item[0], count), item[0], item[1]);
            if !dev.paths().is_empty() {
                indexer.insert(dev.id(), devs.lock().unwrap().len());
                //devices.insert(devices.len(), dev);
                devs.lock().unwrap().insert(dev.id().to_string(), dev);
                *count += 1;
            }
        }
    }
    for (_key, dev) in devs.lock().unwrap().iter() {
        println!("{} {} {:?}", dev.id(), dev.typ(), dev.paths());
    }

    let iw0: iw::Iw = iw::Iw::new(IW_DEVICE);
    ////////////////////////////////////////////////////////////////////////////////////////////////
    if is_service {
        ////////////////////////////////////////////////////////////////////////////////////////////
        let mut srv = service::Service::new(devs, profile, iw0);
        srv.init();
        ////////////////////////////////////////////////////////////////////////////////////////////
        nm_check();
        ////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////
        srv.start();
        ////////////////////////////////////////////////////////////////////////////////////////////
    }
    //
    else {
        let mut is_changed = false;
        for item in DEFAULT_ARGS.iter() {
            if let Some(value) = matches.value_of(item[0]) {
                println!("Value for {}: {}", item[0], value);

                let key = item[1];
                let val = value;

                if key == "bat0-limit" {
                    match val.parse::<u8>() {
                        Ok(set) => {
                            if set > 19 && set < 101 {
                                let old = devs.lock().unwrap().get("bat0").unwrap().get("limit");
                                if *val != old {
                                    methods::set_bat_limit(set);
                                }
                                if *val != data.bat0_limit {
                                    data.bat0_limit = set.to_string();
                                    is_changed = true;
                                }
                            }
                        }
                        Err(err) => eprintln!("{}", err),
                    }
                } else if key == "cpu0-boost" {
                    let set = val == "true";
                    let old =
                        service::get_cpu_boost(devs.lock().unwrap().get("cpu0").unwrap()) == 1;
                    if old != set {
                        methods::set_cpu_boost(set);
                    }
                    let set = if set { "true" } else { "false" };
                    if set != profile.get(key) {
                        profile.set(key, set);
                        is_changed = true;
                    }
                } else if key == "fan0-pwm_cpu" {
                    let set = val == "true";
                    let old = service::get_fan_pwm(devs.lock().unwrap().get("fan0").unwrap()) == 2;
                    if old != set {
                        methods::set_fan_pwm(set);
                    }
                    if set.to_string() != profile.get(key) {
                        profile.set(key, &set.to_string());
                        is_changed = true;
                    }
                } else if key == "fan0-ttp" {
                    let set = val.parse::<u8>().unwrap();
                    methods::set_fan_ttp(set);
                    // TODO
                    let fan_curve = profile.get("fan_curve");
                    if !fan_curve.is_empty() {
                        methods::set_fan_curve(fan_curve);
                    }
                    //
                    if set.to_string() != profile.get(key) {
                        profile.set(key, &set.to_string());
                        is_changed = true;
                    }
                } else if key == "fan_curve" {
                    let set = val;
                    methods::set_fan_curve(set.to_string());
                    if set != profile.get(key) {
                        profile.set(key, set);
                        is_changed = true;
                    }
                } else if key == "wifi-enabled" {
                    let set = val == "true";
                    let old = service::get_wireless_enabled();
                    if old != set {
                        methods::set_wireless_enabled(val == "true");
                    }
                    let set = set.to_string();
                    if set != profile.get(key) {
                        profile.set(key, &set);
                        is_changed = true;
                    }
                } else if key == "wifi-power_save" {
                    let set = val.parse::<u8>().unwrap_or_default();
                    let old = service::get_wireless_power_save(&iw0);
                    if old != set {
                        methods::set_wireless_power_save(set);
                    }
                    let set = set.to_string();
                    if set != profile.get(key) {
                        profile.set(key, &set);
                        is_changed = true;
                    }
                }
            }
        }

        if is_changed {
            data.save();
            profile.save();
        }

        if is_monitor {
            let (tx, rx) = channel();
            thread::spawn(move || {
                monitor::start(&devs, iw0);
            });

            //let running = Arc::new(AtomicBool::new(true));
            //let r = running.clone();
            //ctrlc::set_handler(move || { r.store(false, Ordering::SeqCst); })
            //    .expect("Error setting Ctrl-C handler");
            //while running.load(Ordering::SeqCst) {}
            ctrlc::set_handler(move || tx.send(()).expect("Could not send signal on channel."))
                .expect("Error setting Ctrl-C handler");
            println!("Waiting for Ctrl-C...");
            rx.recv().expect("Could not receive from channel.");
            let s = "\rGot it! Exiting...";
            println!("\r{}{}", s, " ".repeat(80 - s.len()));
        }
    }
}

fn nm_check() {
    thread::spawn(move || {
        let mut i = 0;
        let dur = std::time::Duration::from_millis(500);
        loop {
            let nm_version = service::get_network_manager_version();
            if nm_version.is_empty() {
                if i > 20 {
                    break;
                }
                std::thread::sleep(dur);
            } else {
                println!("NetworkManager version: {}.", nm_version);

                // TODO
                // println!("Thread NetworkManager sleep 30s.");
                // std::thread::sleep(std::time::Duration::from_millis(30000));
                // println!("Thread NetworkManager wlp2s0+power_save.");
                // let key = "wlp2s0+power_save";
                // let set = profile.get(key).parse::<u8>().unwrap_or_default();
                // let old = service::get_wireless_power_save(&iw0);
                // if set != old {
                //     service::set_wireless_power_save(set, &iw0);
                //     let get = service::get_wireless_power_save(&iw0);
                //     println!("{}: {}, {} -> {}.", key, get, old, set);
                // }

                break;
            }
            i += 1;
        }
    });
}
