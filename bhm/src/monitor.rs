use crate::{devices::Dev, iw, service};
use std::collections::BTreeMap;
use std::io::Write;
use std::sync::{Arc, Mutex};

pub(crate) fn start(devs: &Arc<Mutex<BTreeMap<String, Dev>>>, iw0: iw::Iw) {
    loop {
        let cpu0_boost = devs.lock().unwrap().get("cpu0").unwrap().get("boost");
        let fan_mode = devs.lock().unwrap().get("fan0").unwrap().get("ttp");
        let fan0_rpm_cpu = devs.lock().unwrap().get("fan0").unwrap().get("rpm_cpu");
        let fan0_pwm_cpu = devs.lock().unwrap().get("fan0").unwrap().get("pwm_cpu");
        let bat_lmt = devs.lock().unwrap().get("bat0").unwrap().get("limit");
        let bat_prc = devs.lock().unwrap().get("bat0").unwrap().get("charge");
        let iw_pws = iw0.get_power_save();

        let we = service::get_wireless_enabled();
        let s = format!(
            "\rturbo: {}; fan: {}, {}, {} rpm; bat: {:.2}/{}; iw: on: {}, ps: {}.",
            cpu0_boost, fan0_pwm_cpu, fan_mode, fan0_rpm_cpu, bat_prc, bat_lmt, we, iw_pws
        );
        let s = format!("{}{}", s, " ".repeat(80 - s.len()));
        print!("{}", s);
        std::io::stdout().flush().unwrap();
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
}
