use crate::DEFAULT_STR;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use std::{fs, fs::OpenOptions, io::Write};
use unique_id::string::StringGenerator;
use unique_id::Generator;

const PATH: &str = "/mnt/data/rust/bhm/bhm/data.yml";

#[derive(Debug, Deserialize, Serialize)]
#[serde(transparent)]
struct SeqIndexMap {
    #[serde(with = "indexmap::serde_seq")]
    map: IndexMap<String, String>,
}

impl PartialEq for SeqIndexMap {
    fn eq(&self, other: &Self) -> bool {
        // explicitly compare items in order
        self.map.iter().eq(&other.map)
    }
}

pub(crate) struct Profile {
    data: Data,
    index: usize,
    is_profile: bool,
    is_changed: bool,
}

impl Profile {
    fn init(id: &str) -> Self {
        let mut obj = Self {
            data: Data::new(),
            index: 0,
            is_profile: false,
            is_changed: false,
        };
        // !obj.data.profiles > 0
        if !obj.data.profiles.is_empty() {
            let id = if id.is_empty() {
                &obj.data.profile_id
            } else {
                id
            };
            if obj.check(id) {
                obj.index = obj.get_index(id);
                obj.is_profile = true;
            }
        }
        obj
    }

    fn check(&self, id: &str) -> bool {
        !id.is_empty()
            // self.data.profiles.len() > 0
            && !self.data.profiles.is_empty()
            && self.data.profiles[self.get_index(id)]
                .get("id")
                .unwrap_or(DEFAULT_STR)
                == id
    }

    pub(crate) fn current() -> Self {
        Self::init("")
    }

    pub(crate) fn from_id(id: &str) -> Self {
        Self::init(id)
    }

    // pub(crate) fn len(&self) -> usize {
    //     self.data.profiles.len()
    // }

    pub(crate) fn get(&self, key: &str) -> String {
        if self.is_profile {
            self.data.profiles[self.index]
                .get(key)
                .unwrap_or(DEFAULT_STR)
                .to_string()
        } else {
            String::new()
        }
    }

    pub(crate) fn set(&mut self, key: &str, value: &str) -> &mut Self {
        if self.is_profile && self.get(key) != *value {
            match self.data.profiles[self.index].get_mut(key) {
                Some(property) => {
                    *property = value.to_string();
                }
                None => {
                    self.data.profiles[self.index].insert(key.to_string(), value.to_string());
                }
            }
            self.is_changed = true;
        }
        self
    }

    pub(crate) fn get_actions(&self) -> IndexMap<String, String> {
        if self.is_profile {
            self.data.profiles[self.index].clone()
        } else {
            IndexMap::new()
        }
    }

    pub(crate) fn is_profile(&self) -> bool {
        self.is_profile
    }
    pub(crate) fn create(&mut self) -> String {
        let mut value = IndexMap::new();
        let id = StringGenerator::default().next_id();
        value.insert("id".to_string(), id.to_string());
        self.data.profiles.push(value);
        self.is_profile = true;
        self.is_changed = true;
        id
    }

    pub(crate) fn get_prev_id(&self) -> String {
        let len = self.data.profiles.len();
        if len > 0 {
            self.data.profiles[if self.index > 0 {
                self.index - 1
            } else {
                len - 1
            }]
            .get("id")
            .unwrap_or(DEFAULT_STR)
            .to_string()
        } else {
            String::new()
        }
    }

    pub(crate) fn get_next_id(&self) -> String {
        let len = self.data.profiles.len();
        if len > 0 {
            self.data.profiles[if self.index + 2 > len {
                0
            } else {
                self.index + 1
            }]
            .get("id")
            .unwrap_or(DEFAULT_STR)
            .to_string()
        } else {
            String::new()
        }
    }

    pub(crate) fn get_index(&self, id: &str) -> usize {
        self.data
            .profiles
            .iter()
            .position(|x| x.get("id").unwrap_or(DEFAULT_STR) == id)
            .unwrap_or(0)
    }

    pub(crate) fn set_profile_id(&mut self, id: &str) -> bool {
        if self.check(id) {
            self.index = self.get_index(id);
            self.is_profile = true;
            self.data.profile_id = id.to_string();
            self.is_changed = true;
            true
        } else {
            false
        }
    }

    pub(crate) fn remove(&mut self, id: &str) {
        if self.check(id) {
            self.data.profiles.remove(self.get_index(id));
            self.is_profile = false;
            self.is_changed = true;
        }
    }

    pub(crate) fn save(&self) {
        self.data.save();
    }

    pub(crate) fn save_changes(&self) {
        if self.is_changed {
            self.data.save();
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub(crate) struct Data {
    pub(crate) bat0_limit: String,
    pub(crate) profile_id: String,
    pub(crate) profiles: Vec<IndexMap<String, String>>,
}

impl Data {
    pub(crate) fn new() -> Self {
        let path = PATH;
        match match fs::read_to_string(path) {
            Ok(deserialized) => match serde_yaml::from_str(&deserialized) {
                Ok(data) => Ok(data),
                Err(err) => {
                    eprintln!("Serialize invalid data format: {}", err);
                    Err(())
                }
            },
            Err(err) => {
                eprintln!("Unable to open data, path: {}, {}", path, err);
                Err(())
            }
        } {
            Ok(data) => data,
            Err(_err) => Self {
                bat0_limit: String::from("70"),
                profile_id: String::new(),
                profiles: vec![],
            },
        }
    }

    pub(crate) fn save(&self) {
        let path = PATH;
        match serde_yaml::to_string(self) {
            Ok(serialize) => match OpenOptions::new()
                .create(true)
                .write(true)
                .append(false)
                .truncate(true)
                .open(path)
            {
                Ok(mut file) => match file.write_all(serialize.as_bytes()) {
                    Ok(_) => (),
                    Err(err) => eprintln!("Unable to save data, path: {}, {}.", path, err),
                },
                Err(err) => eprintln!("Unable to open data, path: {}, {}", path, err),
            },
            Err(err) => eprintln!("Serialize invalid data format: {}", err),
        }
    }
}
