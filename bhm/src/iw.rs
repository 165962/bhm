use crate::utility::{exec, root_exec};

pub(crate) struct Iw {
    device: String,
}

impl Iw {
    pub(crate) fn new(device: &str) -> Self {
        Self {
            device: device.to_string(),
        }
    }

    pub(crate) fn get_power_save(&self) -> u8 {
        let command = format!("/sbin/iw dev {} get power_save", &self.device);
        let result = exec(&command);
        let result = result.trim();
        if result == "Power save: off" {
            0
        } else if result == "Power save: on" {
            1
        } else {
            0
        }
    }

    pub(crate) fn set_power_save(&self, mode: u8) {
        let command = format!("/sbin/iw dev {} set power_save", &self.device);
        if mode == 0 {
            root_exec(&(command + " off"));
        } else if mode == 1 {
            root_exec(&(command + " on"));
        }
    }
}
