##### Daemon dbus policy
```shell
sudo editor /etc/dbus-1/system.conf
```
 or
```shell
sudo editor /etc/dbus-1/dbus.conf
```
###### Edit tag `<policy></policy>`
```xml
<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>
  <policy context="default">
    <allow own="bas.apps.bhm"/>
    <allow send_destination="bas.apps.bhm"/>
    <allow send_interface="org.freedesktop.DBus.Properties"/>
  </policy>
</busconfig>
```

##### Create systemd services to run the daemon
```shell
sudo editor /etc/systemd/system/bhm.service
```
###### Paste
```text
[Unit]
Description=BAS HWMon
After=basic.target

[Service]
User=root
WorkingDirectory=/mnt/data/rust
ExecStart=/mnt/data/rust/bhm/target/debug/bhm --daemon

[Install]
WantedBy=multi-user.target
```

##### The second option is to create a systemd service to run the daemon
```shell
sudo editor /lib/systemd/system/bhm.service
```
###### Paste
```text
[Unit]
Description=BAS HWMon
#StartLimitInterval=200
#StartLimitBurst=2
Before=display-manager.service
#After=basic.target

[Service]
#User=root
#WorkingDirectory=/mnt/data/rust
Environment=IS_SERVICE=1
ExecStart=/mnt/data/rust/bhm/target/debug/bhm
#ExecStartPre=/bin/sleep 2
#Restart=on-failure
#Restart=always
#RestartSec=1
Type=dbus
BusName=bas.apps.bhm
SELinuxContext=system_u:system_r:unconfined_t:s0
##SELinuxContext=system_u:object_r:modules_object_t:s0

[Install]
WantedBy=multi-user.target
```

##### Daemon systemd enable and start service
```shell
sudo systemctl daemon-reload
sudo systemctl reload dbus
sudo systemctl enable bhm.service
sudo systemctl start bhm.service
```
###### Check the service status in systemd
```shell
sudo systemctl status bhm.service
```
##### Monitor
```
/mnt/data/rust/bhm/target/debug/bhm --monitor
```
##### Daemon systemd disable service
```
sudo systemctl disable bhm.service
```

```
--cpu-boost false
--fan-pwm-enable true
--fan-throttle-thermal-policy 2
--bat-charge-control-end-threshold 75
--wireless-enabled true
--wireless-power-save true
```

### dbus bas.apps.bhm
**ВНИМАНИЕ! Я не могу определиться со спецификацией, так что возможно
скоро все измениться.**

### Methods:

-------------------------------------------------------------------------
###### GetVersion () ↦ (`String` version)
>Вернет `String` c версией `bhm` или пустую строку.
-------------------------------------------------------------------------
###### GetWirelessEnabled() ↦ (`bool` enable)
>Вернет `bool` значение `WirelessEnabled` из `Network Manager`.
-------------------------------------------------------------------------
###### SetWirelessEnabled(`bool` enable) ↦ (`bool` enable)
>Попытается отправить `bool` значение `WirelessEnabled` в
>`Network Manager`.

>Вернет `bool` значение `WirelessEnabled` из `Network Manager`.
-------------------------------------------------------------------------
###### GetKbdBrightness() ↦ (`u8` value)
>Вернет `u8` (TODO может `i32`?) значение `GetBrightness` из `UPower`.
-------------------------------------------------------------------------
###### GetKbdMaxBrightness() ↦ (`u8` value)
>Вернет `u8` (TODO может `i32`?) значение `GetMaxBrightness` из `UPower`.
-------------------------------------------------------------------------
###### SetKbdBrightness(`u8` value) ↦ (`u8` value)
>Попытается отправить `i32` значение `SetBrightness` в `UPower`.

>Вернет `u8` (TODO может `i32`?) значение `GetBrightness` из `UPower`.
-------------------------------------------------------------------------
###### GetWirelessPowerSave(`String` device) ↦ (`u8` mode)
>Примет `String` имя устройства.

>Вернет `u8` режим экономии энергии для устройства `device`
>(пока 0 или 1, но возможны варианты).
-------------------------------------------------------------------------
###### SetWirelessPowerSave(`String` device, `u8` mode) ↦ (`u8` mode)
>Примет `String` имя устройства, `u8` режим экономии энергии.

>Попытается изменить режим экономии энергии для устройства `device` через
>`iw` на принятый режим в `mode`.

>Вернет `u8` режим экономии энергии для устройства `device`
>(пока 0 или 1, но возможны варианты).
-------------------------------------------------------------------------

### Profile methods :

-------------------------------------------------------------------------
###### GetProfile() ↦ (`String` id)
>Вернет `String` идентификатор текущего профиля или пустую строку.
-------------------------------------------------------------------------
###### ProfileData(`String` id) ↦ (`Vec<Vec<String>>` data)
>Примет `String` идентификатор профиля.

>Вернет `Vec<Vec<String>>` список (свойство, значение), данные профиля
>или пустой `Vec`.
-------------------------------------------------------------------------
###### ProfileNames() ↦ (`Vec<Vec<String>>` list)
>Вернет `Vec<Vec<String>>` список (ключ, значение)
>(идентификатор профиля, наименование профиля) или пустой `Vec`.
-------------------------------------------------------------------------
###### SetProfile(`String` id) ↦ (`String` id)
>Примет `String` идентификатор профиля.

>Попытается переключиться на профиль по принятому идентификатору.

>Вернет `String` идентификатор текущего профиля или пустую строку.
-------------------------------------------------------------------------
###### PrevProfile() ↦ (`String` id)
>Попытается переключиться на предыдущий профиль по порядку.

>Вернет `String` идентификатор текущего профиля или пустую строку.
-------------------------------------------------------------------------
###### NextProfile() ↦ (`String` id)
>Попытается переключиться на следующий профиль по порядку.

>Вернет `String` идентификатор текущего профиля или пустую строку.
-------------------------------------------------------------------------
###### CreateProfile (`Vec<Vec<String>>` data) ↦ (`String` id)
>Примет `Vec<Vec<String>>` список (свойство, значение).

>Попытается создать профиль в соответствии с `data`.

>Вернет `String` идентификатор созданного профиля или пустую строку.
-------------------------------------------------------------------------
###### UpdateProfile (`String` id, `Vec<Vec<String>>` data) ↦ (`String` id)
>Примет `Vec<Vec<String>>` список (ключ, значение) (свойство, значение).

>Попытается обновить профиль в соответствии с `data`.

>Вернет `String` идентификатор измененного профиля или пустую строку.
-------------------------------------------------------------------------
###### RemoveProfile(`String` id) ↦ (`String` id)
>Примет `String` идентификатор профиля, попытается удалить профиль по
>принятому идентификатору, переключиться на другой профиль.

>Вернет `String` идентификатор текущего профиля или пустую строку.
-------------------------------------------------------------------------

```text
TODO в этих методах возможно нужен идентификатор устройства?
Methods:

GetBatChargeControlEndThreshold() ↦ (Byte limit)
SetBatChargeControlEndThreshold(Byte limit) ↦ (Byte limit)

GetCpuFreqBoost() ↦ (Byte arg_0)
SetCpuFreqBoost(Byte boost) ↦ (Byte arg_1)

GetFanCurve() ↦ (String curve)
SetFanCurve(String curve) ↦ (String curve)

GetFanPwmEnable() ↦ (Byte arg_0)
SetFanPwmEnable(Byte mode) ↦ (Byte arg_1)

GetFanThrottleThermalPolicy() ↦ (Byte arg_0)
SetFanThrottleThermalPolicy(Byte mode) ↦ (Byte arg_1)

Signals:
Action(String, String)
ChangeFanPwmEnable(Byte)
Error(String)
```