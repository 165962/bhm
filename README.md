# `BAS HWMon`

[EN](./README.en.md)

Данный проект
[`bhm`](https://gitlab.com/165962/bhm)
по сути продолжение проекта
[`asusctl-hwmon`](https://gitlab.com/165962/asusctl-hwmon).

* [Daemon](#daemon)
* [GUI](#gui)
* [Cкриншоты](#cкриншоты)
* [`Rust`](#rust)
* [Сборка проекта `bhm`](#сборка-проекта-bhm)
* [От автора](#от-автора)
  * [`Debian 11` и `asusctl`](#debian-11-и-asusctl)

---

### Цели bhm

Вывод данных с датчиков, температуры, скорости вращения вентиляторов,
заряда батарей.

Управление доступными режимами работы оборудования, от обычного
пользователя, а не только от пользователя `root`.

По возможности создать систему профилей для быстрого изменения режимов
работы оборудования на прописанное в профиле.

Основная цель, предоставить быстрый доступ к информации с датчиков для
оценки текущего состояния пользователю, что бы он мог быстро понять, что
больше всего греется или расходует ресурсов.
Ограничить лимит заряда батареи, для увелечения её срока службы.

---
---

### Оборудование

---
#### AC
https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power

* `/sys/class/power_supply/AC0`
  * `/sys/class/power_supply/AC0/online`
  0 - Offline,
  1 - Online.

---
#### Battery
https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power

* `/sys/class/power_supply/BAT0`
  * `/sys/class/power_supply/BAT0/charge_control_end_threshold`
  * `/sys/class/power_supply/BAT0/energy_now`
  * `/sys/class/power_supply/BAT0/energy_full`
  * `/sys/class/power_supply/BAT0/energy_full_design`
  * `/sys/class/power_supply/BAT0/power_now`
  * `/sys/class/power_supply/BAT0/voltage_now`

---
#### CPU
https://www.kernel.org/doc/Documentation/cpu-freq/boost.txt
* `/sys/devices/system/cpu`
  * `/sys/devices/system/cpu/cpufreq`
    * `/sys/devices/system/cpu/cpufreq/boost`
    0 - boosting disabled,
    1 - boosting allowed.

---
#### AMDGPU
https://www.kernel.org/doc/Documentation/ABI/stable/sysfs-class-backlight
```shell
cat /sys/class/backlight/amdgpu_bl0/brightness
```
```shell
cat /sys/class/backlight/amdgpu_bl0/max_brightness
```
```shell
echo 100 | sudo tee /sys/class/backlight/amdgpu_bl0/brightness
```

---
#### NVGPU
`/sys/bus/pci/drivers/nvidia/0000:01:00.0`

---
#### Fan
`/sys/devices/platform/asus-nb-wmi`
<br>
`/sys/devices/platform/asus-nb-wmi/throttle_thermal_policy`

---
#### Kbd
`/sys/class/leds/asus::kbd_backlight`
<br>
`/sys/class/leds/asus::kbd_backlight/brightness`

---
#### Wireless
- Wireless On/Off `dbus` `system` `Network Manager` `WirelessEnabled`
  - `destination`: `org.freedesktop.NetworkManager`
  - `path`: `/org/freedesktop/NetworkManager`
  - `iface`: `org.freedesktop.DBus.Properties`
  - `method_name`: `Get`
  - `body`: (`org.freedesktop.NetworkManager`, `WirelessEnabled`)
- Wireless power save
    - iw get `root`
      ```shell
      iw dev wlp2s0 get power_save
      ```
    - iw set `root`
      ```shell
      iw dev wlp2s0 set power_save on
      ```
    - nmcli
      https://gist.github.com/jcberthon/ea8cfe278998968ba7c5a95344bc8b55
      > Для нормальной работы режима энергосбережения, наверное лучше
      > ставить в `NM` режим энергосбережения в игнорирование? И управлять
      > энергосбережением через `iw`. 
      ```shell
      CONN="conn"; sudo nmcli con mod "$CONN" 802-11-wireless.powersave 1
      ```
      ```shell
      CONN="conn"; nmcli -f 802-11-wireless.powersave con show "$CONN"
      ```
- NM status
  - https://people.freedesktop.org/~lkundrak/nm-docs/nm-dbus-types.html

---
#### Bluetooth
On/Off `dbus` `system` `user`
```shell
dbus-send --system --print-reply --dest="org.bluez" "/org/bluez/hci0" "org.freedesktop.DBus.Properties.Set" string:"org.bluez.Adapter1" string:"Powered" variant:boolean:true
```
`"org.bluez.Adapter1", "Powered", GLib.Variant("b", False)`


On/Off `dbus` `session` `user`

---
---

### Daemon
[`bhm`](./bhm/README.md)

_Позаимствован код у @Yarn1 с проекта
[`Yarn/rog_fan_curve`](https://github.com/Yarn/rog_fan_curve)
, для управления кулерами через `acpi_call`._

---

### GUI
[`bhm-gtk3`](./bhm-gtk3/README.md)

---

### Cкриншоты

###### `bhm-gtk3` `debian 11` `Gnome 3.38.5`

![`Debian 11 Gnome`](./data/screenshots/bhm-gtk3-2021-11-13_20-23.png)

---

### `Rust`
_За более подробной информацией обращаться к официальному ресурсу
[`Rust` Getting started](https://www.rust-lang.org/learn/get-started)._

Установить `Rust`:
```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Обновить `Rust`:
```shell
rustup update
```

Посмотреть версию `Rust`:
```shell
rustup --version
```
```shell
rustup 1.24.3 (ce5817a94 2021-05-31)
info: This is the version for the rustup toolchain manager, not the rustc compiler.
info: The currently active `rustc` version is `rustc 1.56.1 (59eed8a2a 2021-11-01)`
```

### Сборка проекта `bhm`

Извлечь репозиторий `bhm`:
```shell
git clone https://gitlab.com/165962/bhm.git
```

Перейти в директорию извлечённого репозитория `bhm`:
```shell
cd ./bhm
```

**ВНИМАНИЕ!** `bhm-gtk3` на текущий момент использует файл `bhm/data.yml`
для сборки своего `UI`, файл жестко прописан в коде как
`/home/{USER}/.config/bhm/panel.yml`, поэтому необходимо:
* Создать пользовательскую директорию конфигурации `bhm`:
```shell
mkdir /home/$USER/.config/bhm
```

  * Создать файл `/home/$USER/.config/bhm/panel.yml` с корректным
содержимым:
```shell
cp ./data.yml /home/$USER/.config/bhm/panel.yml
```

  * Или сделать символьную ссылку на `./data.yml`, но стоит учесть,
при удалении или изменении `./data.yml` возможны проблемы:
```shell
ln -s $PWD/data.yml /home/$USER/.config/bhm/panel.yml
```

Сборка `bhm` и `bhm-gtk3`:
```shell
cargo build --release
```
После сборки можно наблюдать что-то подобное:
```shell
ls -la ./target/release/
```
```shel
итого 15732
drwxr-xr-x.   7 xxx 1001    4096 ноя  3 08:28 .
drwxr-xr-x.   4 xxx 1001    4096 окт 23 01:52 ..
-rwxrwxr-x.   2 xxx xxx  7303504 ноя  3 08:27 bhm
-rw-r--r--.   1 xxx 1001     543 ноя  3 08:28 bhm.d
-rwxrwxr-x.   2 xxx xxx  8550560 ноя  3 08:28 bhm-gtk3
-rw-r--r--.   1 xxx 1001    1069 ноя  3 08:28 bhm-gtk3.d
drwxr-xr-x. 199 xxx 1001   12288 ноя  3 08:26 build
-rw-r--r--.   1 xxx 1001       0 окт 23 01:52 .cargo-lock
drwxr-xr-x.   2 xxx 1001   81920 ноя  3 08:28 deps
drwxr-xr-x.   2 xxx 1001    4096 окт 23 01:52 examples
drwxr-xr-x. 601 xxx 1001   36864 ноя  3 08:26 .fingerprint
drwxr-xr-x.   2 xxx 1001    4096 окт 23 01:52 incremental
-rw-r--r--.   1 xxx 1001      81 ноя  3 08:28 libbhm.d
-rw-r--r--.   1 xxx 1001      91 окт 23 01:54 libbhm_gtk3.d
-rw-r--r--.   2 xxx 1001   70538 окт 23 01:53 libbhm_gtk3.rlib
-rw-rw-r--.   2 xxx xxx     2696 ноя  3 08:27 libbhm.rlib
```

В директории сборки находятся два файла готовые к употреблению `bhm` и
`bhm-gtk3`.

Подробнее можно узнать на этих страницах:
  * Daemon [`bhm`](./bhm/README.md)
  * GUI [`bhm-gtk3`](./bhm-gtk3/README.md)

---

### От автора

Пишу для своего `ROG Zephyrus G14` `GA401IV` 2020 года, под
[`debian`](https://www.debian.org/) 11
, так как патчи с проекта
[`asus-linux`](https://gitlab.com/asus-linux)
до него скорее всего дойдут нескоро, думаю через 2 года, а то и через
лет 5. Проект
[`asus-linux`](https://gitlab.com/asus-linux)
очень быстро развивается, активно продвигает патчи в ядро, но и часто
меняется `api`, к сожалению для
[`debian`](https://www.debian.org/) 11
все стоит на месте.

Так что возможно лучше использовать это приложение
[`asus-linux`](https://gitlab.com/asus-linux) /
[`asusctl`](https://gitlab.com/asus-linux/asusctl).

#### `Debian 11` и `asusctl` 

Страница модуля
[`hid-asus-rog`](https://gitlab.com/asus-linux/hid-asus-rog)
ПРЕДУПРЕЖДАЕТ, ПОЖАЛУЙСТА, ИСПОЛЬЗУЙТЕ ЯДРО 5.12.x+ ВМЕСТО ЭТОГО МОДУЛЯ.
[`Debian`](https://www.debian.org/) 11
используется ядро 5.10, поэтому необходим данный модуль.
В репозитории 
https://download.opensuse.org/repositories/home:/luke_nukem:/asus/
данного пакета уже не существует, но у меня осталась сохранённая копия
`dkms-hid-asus-rog_1.0.2-2.21_all.deb`
[скачать](./data/packages/dkms-hid-asus-rog_1.0.2-2.21_all.deb).

Версия `asusctl` которая на текущий момент стабильно работает с
[`debian`](https://www.debian.org/) 11
[`asusctl 3.7.2`](https://gitlab.com/asus-linux/asusctl/-/tree/3.7.2)
, но для работы `fan curve` в этой версии потребуется `acpi_call`.

---

### Товарные знаки

Торговая марка ASUS и ROG является зарегистрированной торговой маркой США
или торговой маркой ASUSTeK Computer Inc. в Соединенных Штатах и/или
других странах.

Ссылки на любые продукты, услуги, процессы ASUS или другую информацию
и/или использование товарных знаков ASUS не означает и не подразумевает
одобрения, спонсорства или рекомендации со стороны ASUS.

Использование торговых марок ROG и ASUS на этом веб-сайте и связанных с
ними инструментов и библиотек предназначено только для предоставления
пользователям узнаваемого идентификатора, позволяющего им связать, что
эти инструменты будут работать с ноутбуками ASUS ROG Zephyrus G14
GA401IV.

---
