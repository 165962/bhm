# `BAS HWMon`

[RU](./README.md)

This project
[`bhm`](https://gitlab.com/165962/bhm)
is essentially a continuation project
[`asusctl-hwmon`](https://gitlab.com/165962/asusctl-hwmon).

* [Daemon](#daemon)
* [GUI](#gui)
* [Screenshots](#screenshots)
* [`Rust`](#rust)
* [Build project `bhm`](#build-project-bhm)
* [From author](#from-author)
  * [`Debian 11` and `asusctl`](#debian-11-and-asusctl)

---

### Daemon
[`bhm`](./bhm/README.md)

_Code borrowed from @Yarn1 from the project
[`Yarn/rog_fan_curve`](https://github.com/Yarn/rog_fan_curve)
, for control coolers via `acpi_call`._

---

### GUI
[`bhm-gtk3`](./bhm-gtk3/README.md)

---

### Screenshots

###### `bhm-gtk3` `debian 11` `Gnome 3.38.5`

![`Debian 11 Gnome`](./data/screenshots/bhm-gtk3-2021-11-02_04-07.png)

---

### `Rust`
_For more information, refer to the official resource
[`Rust` Getting started](https://www.rust-lang.org/learn/get-started)._

Install `Rust`:
```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Update `Rust`:
```shell
rustup update
```

View the `Rust` version:
```shell
rustup --version
```
```shell
rustup 1.24.3 (ce5817a94 2021-05-31)
info: This is the version for the rustup toolchain manager, not the rustc compiler.
info: The currently active `rustc` version is `rustc 1.56.1 (59eed8a2a 2021-11-01)`
```

###### Build the `bhm` project

Extract the `bhm` repository:
```shell
git clone https://gitlab.com/165962/bhm.git
```

Change to the directory of the checked out `bhm` repository:
```shell
cd ./bhm
```

**WARNING!** `bhm-gtk3` currently uses the file `bhm/data.yml` to build
your `UI`, the file is hardcoded as `/home/{USER}/.config/bhm/panel.yml`,
so you need:
* Create custom `bhm` configuration directory:
```shell
mkdir /home/$USER/.config/bhm
```

  * Create a file `/home/$USER/.config/bhm/panel.yml' with the correct
contents:
```shell
cp ./data.yml /home/$USER/.config/bhm/panel.yml
```

  * Or make a symbolic link to `./data.yml`, but be aware that when
deleting or changing `./data.yml`, there may be problems:
```shell
ln -s $PWD/data.yml /home/$USER/.config/bhm/panel.yml
```

Building `bhm` and `bhm-gtk3`:
```shell
cargo build --release
```
After assembly, you can see something like this:
```shell
ls -la ./target/release/
```
```shel
total 15732
drwxr-xr-x.   7 xxx 1001    4096 nov  3 08:28 .
drwxr-xr-x.   4 xxx 1001    4096 oct 23 01:52 ..
-rwxrwxr-x.   2 xxx xxx  7303504 nov  3 08:27 bhm
-rw-r--r--.   1 xxx 1001     543 nov  3 08:28 bhm.d
-rwxrwxr-x.   2 xxx xxx  8550560 nov  3 08:28 bhm-gtk3
-rw-r--r--.   1 xxx 1001    1069 nov  3 08:28 bhm-gtk3.d
drwxr-xr-x. 199 xxx 1001   12288 nov  3 08:26 build
-rw-r--r--.   1 xxx 1001       0 oct 23 01:52 .cargo-lock
drwxr-xr-x.   2 xxx 1001   81920 nov  3 08:28 deps
drwxr-xr-x.   2 xxx 1001    4096 oct 23 01:52 examples
drwxr-xr-x. 601 xxx 1001   36864 nov  3 08:26 .fingerprint
drwxr-xr-x.   2 xxx 1001    4096 oct 23 01:52 incremental
-rw-r--r--.   1 xxx 1001      81 nov  3 08:28 libbhm.d
-rw-r--r--.   1 xxx 1001      91 oct 23 01:54 libbhm_gtk3.d
-rw-r--r--.   2 xxx 1001   70538 oct 23 01:53 libbhm_gtk3.rlib
-rw-rw-r--.   2 xxx xxx     2696 nov  3 08:27 libbhm.rlib
```

In the build directory there are two files ready for use `bhm` and
`bhm-gtk3`.

You can find out more on these pages:
  * Daemon [`bhm`](./bhm/README.md)
  * GUI [`bhm-gtk3`](./bhm-gtk3/README.md)

---

### From author

I am writing for my `ROG Zephyrus G14` `GA401IV` 2020, under
[`debian`](https://www.debian.org/) 11
, since patches from the project
[`asus-linux`](https://gitlab.com/asus-linux)
will most likely reach it not soon, I think in 2 years, or even in 5
years. Project
[`asus-linux`](https://gitlab.com/asus-linux)
is developing very quickly, actively promotes patches to the kernel, but
also often changes `api`, to Unfortunately for
[`debian`](https://www.debian.org/) 11
everything is still.

So it might be better to use this app
[`asus-linux`](https://gitlab.com/asus-linux) /
[`asusctl`](https://gitlab.com/asus-linux/asusctl).

#### `Debian 11` and `asusctl` 

Module page
[`hid-asus-rog`](https://gitlab.com/asus-linux/hid-asus-rog)
WARNS, PLEASE USE KERNEL 5.12.x+ INSTEAD OF THIS MODULE.
[`Debian`](https://www.debian.org/) 11
uses the 5.10 kernel, so this module is required.
This package no longer exists in the repository
https://download.opensuse.org/repositories/home:/luke_nukem:/asus/
, but I still have a backup copy of
`dkms-hid-asus-rog_1.0.2-2.21_all.deb`
[download](./data/packages/dkms-hid-asus-rog_1.0.2-2.21_all.deb).

The version of `asusctl` which currently works stably with
[`debian`](https://www.debian.org/) 11
[`asusctl 3.7.2`](https://gitlab.com/asus-linux/asusctl/-/tree/3.7.2)
but for `fan curve` to work in this version, `acpi_call` is required.

---

### Trademarks

ASUS and ROG Trademark is either a US registered trademark or trademark
of ASUSTeK Computer Inc. in the United States and/or
other countries.

Reference to any ASUS products, services, processes, or other information
and/or use of ASUS Trademarks does not constitute or imply endorsement,
sponsorship, or recommendation thereof by ASUS.

The use of ROG and ASUS trademarks within this website and associated
tools and libraries is only to provide a recognisable identifier to users
to enable them to associate that these tools will work with
ASUS ROG Zephyrus G14 GA401IV
laptop.

---
