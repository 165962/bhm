use lazy_static;
use std::sync::Mutex;
use gtk::{glib, prelude::*, ApplicationWindow, Orientation};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use std::sync::mpsc::Sender;
use unique_id::string::StringGenerator;
use unique_id::Generator;
use crate::bhm;

lazy_static! {
    pub static ref PROFILES_JSON: Mutex<String> = Mutex::new(String::new());
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(transparent)]
struct SeqIndexMap {
    #[serde(with = "indexmap::serde_seq")]
    map: IndexMap<String, String>,
}

impl PartialEq for SeqIndexMap {
    fn eq(&self, other: &Self) -> bool {
        // explicitly compare items in order
        self.map.iter().eq(&other.map)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct ProfileJson {
    id: String,
    data: IndexMap<String, IndexMap<String, String>>,
}

impl ProfileJson {
    pub fn new(id: &str) -> Self {
        let mut data = IndexMap::new();
        for item in bhm::data_profiles().iter() {
            if data.get(&item[0]).is_none() {
                let mut profile = IndexMap::new();
                profile.insert("id".to_string(), item[0].to_string());
                profile.insert("name".to_string(), item[1].to_string());
                profile.insert("action".to_string(), "inited".to_string());
                data.insert(item[0].to_string(), profile);
            }
        }
        Self {
            id: id.to_string(),
            data,
        }
    }
    fn current() -> Self {
        serde_json::from_str(&PROFILES_JSON.lock().unwrap()).unwrap()
    }
    fn open_from_id(id: &str) -> Self {
        let mut profile: ProfileJson = serde_json::from_str(&PROFILES_JSON.lock().unwrap()).unwrap();
        profile.id = id.to_string();
        profile
    }
    fn create(&mut self) -> String {
        let id = &StringGenerator::default().next_id();
        let mut profile = IndexMap::new();
        profile.insert("id".to_string(), id.to_string());
        profile.insert("name".to_string(), "New Profile".to_string());
        profile.insert("action".to_string(), "create".to_string());
        self.data.insert(id.to_string(), profile);
        self.set_id(id);
        id.to_string()
    }
    fn remove(&mut self) -> String {
        let id = if self.index() < 1 {
            self.next_id()
        } else {
            self.prev_id()
        };
        self.set("action", "remove").set_id(&id);
        id
    }
    fn profile_ids(&self) -> Vec<&String> {
        let mut data = vec![];
        for (id, profile) in self.data.iter() {
            if profile.get("action").unwrap() != "remove" {
                data.push(id);
            }
        }
        data
    }
    fn get_profile(&self) -> &IndexMap<String, String> {
        self.data.get(&self.id).unwrap()
    }
    fn set_id(&mut self, id: &str) -> &mut Self {
        self.id = id.to_string();
        self
    }
    fn prev_id(&self) -> String {
        let def = &String::new();
        let ids = self.profile_ids();
        let len = ids.len();
        let idx = ids
            .iter()
            .position(|id| &id.to_string() == &self.id)
            .unwrap_or(0);
        String::from(if len > 1 {
            &ids[if idx < 1 { len - 1 } else { idx - 1 }]
        } else {
            def
        })
    }
    fn next_id(&self) -> String {
        let def = &String::new();
        let ids = self.profile_ids();
        let len = ids.len();
        let idx = ids
            .iter()
            .position(|id| &id.to_string() == &self.id)
            .unwrap_or(0);
        String::from(if len > 1 {
            ids[if idx + 2 > len { 0 } else { idx + 1 }]
        } else {
            def
        })
    }
    fn index(&self) -> usize {
        self.profile_ids()
            .iter()
            .position(|id| &id.to_string() == &self.id)
            .unwrap_or(0)
    }
    fn get(&self, key: &str) -> String {
        match self.data.get(&self.id) {
            Some(profile) => match profile.get(key) {
                Some(property) => property.to_string(),
                None => String::new(),
            },
            None => String::new(),
        }
    }
    fn set(&mut self, key: &str, value: &str) -> &mut Self {
        match self.data.get_mut(&self.id) {
            Some(profile) => match profile.get_mut(key) {
                Some(property) => *property = value.to_string(),
                None => {
                    profile.insert(key.to_string(), value.to_string());
                }
            },
            None => println!("Profile id: '{}', not found.", &self.id),
        }
        self
    }
    fn len(&self) -> usize {
        self.profile_ids().len()
    }
    fn items_profiles(&self) -> Vec<Vec<String>> {
        let mut data = vec![];
        for (id, profile) in self.data.iter() {
            if profile.get("action").unwrap() != "remove" {
                data.push(vec![
                    id.to_string(),
                    profile.get("name").unwrap().to_string(),
                ]);
            }
        }
        data
    }
    fn save(&self) {
        match serde_json::to_string(&self) {
            Ok(serialize) => {
                *PROFILES_JSON.lock().unwrap() = serialize;
            }
            Err(err) => eprintln!("Serialize: {}", err),
        };
    }
}