use crate::{
    bhm, profile_vec::ProfileVec, /*utl::Evt, */ widget,
    window_fan_curve_editor::DialogFanCurveEditor,
};
use gtk::{glib, prelude::*, Orientation};
use std::{cell::RefCell, collections::HashMap, rc::Rc};
//use std::sync::mpsc::Sender;

pub(crate) struct ProfileEditor {}

impl ProfileEditor {
    pub fn window(
        widget: &gtk::Widget,
        //tx: Sender<Evt>,
        id: &str,
        windows: &Rc<RefCell<HashMap<String, gtk::Window>>>,
    ) -> gtk::Window {
        let title = "Profile editor";

        let parent = widget::to_top(widget)
            .downcast::<gtk::ApplicationWindow>()
            .unwrap();
        let application = parent.application().unwrap();

        let window = gtk::Window::new(gtk::WindowType::Toplevel);
        window.set_role("edit");
        window.set_title(title);
        window.set_position(gtk::WindowPosition::Mouse);
        window.set_default_size(400, 200);
        window.connect_delete_event(
            glib::clone!(@weak windows => @default-return Inhibit(false), move |_, _| {
                println!("Closing Profile Editor window.");
                ProfileVec::clear();
                windows.borrow_mut().remove("profile_editor");
                Inhibit(false)
            }),
        );
        application.add_window(&window);

        ProfileVec::new(id);
        let profile_selected = move |id: &str, widget: &gtk::Widget| {
            // TODO
            if id.is_empty() {
                //find_set(widget, "lbl_id", "", None);
                //find_set(widget, "name", "", None);
                return;
            }

            let profile = ProfileVec::from_id(id);
            if &profile.get("action") == "remove" {
                return;
            }
            profile.set_status("selection");
            let profile_data = profile.profile_data();

            let widget = &widget::to_top(widget);
            let keys = vec![
                "action",
                "id",
                "name",
                "cpu0-boost",
                "fan0-pwm_cpu",
                "fan0-ttp",
                "fan_curve",
                "wifi-power_save",
            ];
            for key in keys.iter() {
                widget::find_set(widget, key, "", None);
            }
            for item in profile_data {
                if &item[0] == "id" {
                    widget::find_set(widget, "lbl_id", &item[1], None);
                }
                for key in keys.iter() {
                    if key == &item[0] {
                        widget::find_set(widget, key, &item[1], None);
                    }
                }

                // if &item[0] == "fan_curve" {
                //     //.set_active(&item[1] == "true");
                // } else if &item[0] == "wireless_enabled" {
                //     //.set_active(&item[1] == "true");
                // }
            }
            profile.set_status("selected");
        };
        let cmb_connect_changed = move |combo: &gtk::ComboBoxText| {
            combo.connect_changed(move |combo| {
                let profile = ProfileVec::current();
                if &profile.get_status() == "selection" {
                    return;
                }
                let key = &combo.widget_name().to_string();
                let value = &match combo.active_id() {
                    Some(value) => value.to_string(),
                    None => String::new(),
                };
                let action = &profile.get("action");
                if action == "inited" {
                    return;
                } else if action != "create" && action != "remove" {
                    profile.set("action", "touched");
                    widget::find_set(combo.as_ref(), "action", "touched", None);
                }
                profile.set(key, value);
            });
        };
        let swt_connect_changed = move |switch: &gtk::Switch| {
            switch.connect_changed_active(move |switch| {
                let profile = ProfileVec::current();
                if &profile.get_status() == "selection" {
                    return;
                }
                let key = &switch.widget_name().to_string();
                let value = if switch.is_active() { "1" } else { "0" };
                let action = &profile.get("action");
                if action == "inited" {
                    return;
                } else if action != "create" && action != "remove" {
                    profile.set("action", "touched");
                    widget::find_set(switch.as_ref(), "action", "touched", None);
                }

                // TODO
                let value = if key == "fan0-cpu_pvm" && value == "1" {
                    "2"
                } else {
                    value
                };

                profile.set(key, value);
            });
        };
        let ent_connect_changed = move |entry: &gtk::Entry| {
            entry.connect_changed(move |entry| {
                let profile = ProfileVec::current();
                if &profile.get_status() == "selection" {
                    return;
                }
                let id = &profile.get("id");
                let key = &entry.widget_name().to_string();
                let value = &entry.text().to_string();
                let widget = entry.as_ref();
                let action = &profile.get("action");
                if action == "inited" {
                    return;
                } else if action != "create" && action != "remove" {
                    profile.set("action", "touched");
                    widget::find_set(widget, "action", "touched", None);
                }
                profile.set(key, value);
                match widget::find_get(widget, "id") {
                    Some(widget) => match widget.downcast::<gtk::ComboBoxText>() {
                        Ok(combo) => match combo.model() {
                            Some(model) => match model.downcast::<gtk::ListStore>() {
                                Ok(store) => store.foreach(|_tree_model, _tree_path, tree_iter| {
                                    match store.value(tree_iter, 1).get::<String>() {
                                        Ok(iter_id) => {
                                            if &iter_id == id {
                                                store.set_value(
                                                    tree_iter,
                                                    0,
                                                    &glib::Value::from(value),
                                                );
                                                return true;
                                            }
                                        }
                                        Err(err) => {
                                            eprintln!("TreeModel Value {}", err);
                                            return false;
                                        }
                                    }
                                    false
                                }),
                                Err(err) => eprintln!("Error ListStore downcast: {}", err),
                            },
                            None => eprintln!("None TreeModel downcast::<gtk::ListStore>."),
                        },
                        Err(err) => eprintln!("Error TreeModel: {}", err),
                    },
                    None => eprintln!("None ComboBoxText downcast::<gtk::ComboBoxText>."),
                }
            });
        };
        let ent_connect_changed1 = move |entry: &gtk::Entry| {
            entry.connect_changed(move |entry| {
                let profile = ProfileVec::current();
                if &profile.get_status() == "selection" {
                    return;
                }
                //let id = &profile.get("id");
                let key = &entry.widget_name().to_string();
                let value = &entry.text().to_string();
                let widget = entry.as_ref();
                let action = &profile.get("action");
                if action == "inited" {
                    return;
                } else if action != "create" && action != "remove" {
                    profile.set("action", "touched");
                    widget::find_set(widget, "action", "touched", None);
                }
                profile.set(key, value);
            });
        };

        // -------------------------------------------
        let dialog = gtk::Dialog::with_buttons(
            Some(title),
            Some(&parent),
            gtk::DialogFlags::MODAL,
            &[
                ("Cancel", gtk::ResponseType::No),
                ("Apply", gtk::ResponseType::Yes), //, ("Custom", gtk::ResponseType::Other(0))
            ],
        );
        let content_area = dialog.content_area();

        // -------------------------------------------
        let vbox = gtk::Box::new(Orientation::Vertical, 8);

        // let hbox100 = gtk::Box::new(Orientation::Horizontal, 0);
        // let label = gtk::Label::new(Some("Hide"));
        // let entry = gtk::Entry::new();
        // entry.set_text(&bhm::profile_name(id));
        // hbox100.pack_start(&label, false, false, 0);
        // hbox100.pack_end(&entry, false, false, 0);
        // vbox0.pack_start(&hbox100, false, false, 0);

        //let vbox1000 = gtk::Box::new(Orientation::Vertical, 0);
        let hbox1000 = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Data"));
        let ent_data = gtk::Entry::new();
        ent_data.set_widget_name("data");
        hbox1000.pack_start(&lbl0, false, false, 0);
        hbox1000.pack_end(&ent_data, false, false, 0);
        vbox.pack_start(&hbox1000, false, false, 0);
        //content_area.pack_start(&vbox1000, false, false, 16);

        let vbox2000 = gtk::Box::new(Orientation::Vertical, 0);
        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let cmb0 = widget::Cmb::new(&bhm::data_profiles());
        let btn0 = gtk::Button::new();
        //let btn1 = gtk::Button::new();
        let btn2 = gtk::Button::new();
        //let btn3 = gtk::Button::new();

        let btn3 = gtk::Button::new();
        cmb0.set_widget_name("id");
        cmb0.widget.style_context().add_class("id");
        cmb0.set_value(id);
        cmb0.connect_changed(move |combo: &gtk::ComboBoxText| {
            if let Some(id) = combo.active_id() {
                profile_selected(&*id.to_string(), combo.as_ref());
            }
        });
        btn0.set_widget_name("minus");
        btn0.style_context().add_class("plus");
        btn0.set_label("+");
        btn0.connect_clicked(move |button| {
            let widget: &gtk::Widget = button.as_ref();
            let profile = ProfileVec::current();
            let id = &profile.create();
            profile.set_id(id);
            widget::find_set(widget, "id", id, Some(&profile.items_profiles()));
            if profile.len() > 1 {
                match widget::find_get(button.as_ref(), "prev") {
                    Some(widget) => match widget.downcast::<gtk::Button>() {
                        Ok(btn) => btn.set_sensitive(true),
                        Err(err) => eprintln!("Error: {:?}.", err),
                    },
                    None => eprintln!("None widget prev."),
                }
                match widget::find_get(widget, "next") {
                    Some(widget) => match widget.downcast::<gtk::Button>() {
                        Ok(btn) => btn.set_sensitive(true),
                        Err(err) => eprintln!("Error: {:?}.", err),
                    },
                    None => eprintln!("None widget next."),
                }
            }
            if profile.len() > 0 {
                match widget::find_get(widget, "minus") {
                    Some(widget) => match widget.downcast::<gtk::Button>() {
                        Ok(btn) => btn.set_sensitive(true),
                        Err(err) => eprintln!("Error: {:?}.", err),
                    },
                    None => eprintln!("None widget minus."),
                }
            }
        });
        // btn1.set_widget_name("minus");
        // btn1.set_label("-");
        // btn1.connect_clicked(move |button| {
        //     let profile = ProfileVec::current();
        //     let id = &if profile.get_index() + 1 == profile.len() {
        //         profile.prev_id()
        //     } else {
        //         profile.next_id()
        //     };
        //     if profile.remove() {
        //         profile.set_id(id);
        //         let widget = button.as_ref();
        //         widget_find_set(widget, "id", id, Some(&profile.items_profiles()));
        //         if profile.len() < 2 {
        //             match widget_find_get(widget, "prev") {
        //                 Some(widget) => match widget.downcast::<gtk::Button>() {
        //                     Ok(btn) => btn.set_sensitive(false),
        //                     Err(err) => eprintln!("Error: {:?}.", err),
        //                 },
        //                 None => eprintln!("None widget prev."),
        //             }
        //             match widget_find_get(widget, "next") {
        //                 Some(widget) => match widget.downcast::<gtk::Button>() {
        //                     Ok(btn) => btn.set_sensitive(false),
        //                     Err(err) => eprintln!("Error: {:?}.", err),
        //                 },
        //                 None => eprintln!("None widget next."),
        //             }
        //         }
        //         if profile.len() < 1 {
        //             button.set_sensitive(false);
        //             widget_find_set(button.as_ref(), "name", "", None);
        //             widget_find_set(button.as_ref(), "lbl_id", "", None);
        //         }
        //     }
        // });
        btn2.set_widget_name("prev");
        btn2.style_context().add_class("prev");
        btn2.set_label("←");
        btn2.connect_clicked(move |button| {
            let profile = ProfileVec::current();
            let id = &profile.prev_id();
            profile.set_id(id);
            widget::find_set(button.as_ref(), "id", id, None);
        });
        btn3.set_widget_name("next");
        btn3.style_context().add_class("next");
        btn3.set_label("→");
        btn3.connect_clicked(move |button| {
            let profile = ProfileVec::current();
            let id = &profile.next_id();
            profile.set_id(id);
            widget::find_set(button.as_ref(), "id", id, None);
        });
        hbox.pack_start(&btn2, false, false, 0);
        hbox.pack_start(&btn3, false, false, 0);
        hbox.pack_start(&cmb0.widget, false, false, 0);
        hbox.pack_end(&btn0, false, false, 0);
        //hbox.pack_end(&btn1, false, false, 0);
        vbox2000.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Profile name "));
        let ent0 = gtk::Entry::new();
        ent0.set_widget_name("name");
        //ent0.set_size_request(200, 0);
        ent_connect_changed(&ent0);
        ent0.set_max_length(16);
        ent0.set_max_width_chars(22);
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&ent0, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Processor boosting control "));
        let swt0 = gtk::Switch::new();
        swt0.set_widget_name("cpu0-boost");
        swt_connect_changed(&swt0);
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&swt0, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Fan CPU Pulse Width Modulation "));
        let swt0 = gtk::Switch::new();
        swt0.set_widget_name("fan0-pwm_cpu");
        swt_connect_changed(&swt0);
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&swt0, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Fan throttle thermal policy "));
        let cmb0 = widget::Cmb::new(&bhm::data_fan_ttp());
        cmb0.set_widget_name("fan0-ttp");
        cmb_connect_changed(&cmb0.widget);
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&cmb0.widget, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Fan curve "));
        let ent_fan_curve = gtk::Entry::new();
        let button = gtk::Button::with_label("CPU and GPU");
        ent_fan_curve.set_widget_name("fan_curve");
        ent_fan_curve.hide();
        ent_fan_curve.set_sensitive(false);
        //ent0.set_size_request(200, 0);
        //ent0.set_max_length(16);
        //ent0.set_max_width_chars(22);
        ent_connect_changed1(&ent_fan_curve);
        button.connect_clicked(glib::clone!(@weak window => move |_button: &gtk::Button| {
            let curve = match widget::find_get(window.as_ref(), "fan_curve") {
                Some(widget) => widget.downcast::<gtk::Entry>().unwrap().text().to_string(),
                None => String::new(),
            };
            // TODO
            let parent = window.clone();
            DialogFanCurveEditor::new(Some(window.as_ref()), &curve).open(move |dialog, curve| {
                match widget::find_get(parent.as_ref(), "fan_curve") {
                    Some(widget) => {
                        widget.downcast::<gtk::Entry>().unwrap().set_text(curve);
                        dialog.close();
                    }
                    None => eprintln!("Error set curve"),
                }
            });
        }));
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&button, false, false, 0);
        hbox.pack_end(&ent_fan_curve, true, true, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Wireless power save "));
        let swt0 = gtk::Switch::new();
        swt0.set_widget_name("wifi-power_save");
        swt_connect_changed(&swt0);
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&swt0, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("ID "));
        let lbl1 = gtk::Label::new(Some(id));
        lbl1.set_widget_name("lbl_id");
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&lbl1, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox = gtk::Box::new(Orientation::Horizontal, 0);
        let lbl0 = gtk::Label::new(Some("Action: "));
        let lbl1 = gtk::Label::new(Some(""));
        lbl1.set_widget_name("action");
        hbox.pack_start(&lbl0, false, false, 0);
        hbox.pack_end(&lbl1, false, false, 0);
        vbox.pack_start(&hbox, false, false, 0);

        let hbox4000 = gtk::Box::new(Orientation::Horizontal, 0);
        let btn0 = gtk::Button::with_label("Apply");
        let btn1 = gtk::Button::with_label("Cancel");
        let btn2 = gtk::Button::with_label("Remove");
        btn0.style_context().add_class("apply");
        btn2.set_widget_name("minus");
        btn2.style_context().add_class("remove");
        btn2.connect_clicked(move |button| {
            let profile = ProfileVec::current();
            let id = &if profile.get_index() + 1 == profile.len() {
                profile.prev_id()
            } else {
                profile.next_id()
            };
            if profile.remove() {
                profile.set_id(id);
                let widget = button.as_ref();
                widget::find_set(widget, "id", id, Some(&profile.items_profiles()));
                if profile.len() < 2 {
                    match widget::find_get(widget, "prev") {
                        Some(widget) => match widget.downcast::<gtk::Button>() {
                            Ok(btn) => btn.set_sensitive(false),
                            Err(err) => eprintln!("Error: {:?}.", err),
                        },
                        None => eprintln!("None widget prev."),
                    }
                    match widget::find_get(widget, "next") {
                        Some(widget) => match widget.downcast::<gtk::Button>() {
                            Ok(btn) => btn.set_sensitive(false),
                            Err(err) => eprintln!("Error: {:?}.", err),
                        },
                        None => eprintln!("None widget next."),
                    }
                }
                if profile.len() < 1 {
                    // TODO
                    //button.set_sensitive(false);
                    widget::find_set(button.as_ref(), "name", "", None);
                    widget::find_set(button.as_ref(), "lbl_id", "", None);
                }
            }
        });
        btn0.connect_clicked(glib::clone!(@weak window => move |_button: &gtk::Button| {
            Self::profile_apply(window.as_ref());
        }));
        btn1.connect_clicked(glib::clone!(@weak window => move |_button: &gtk::Button| {
            window.close();
        }));
        hbox4000.pack_start(&btn2, false, false, 0);
        hbox4000.pack_end(&btn0, false, false, 0);
        hbox4000.pack_end(&btn1, false, false, 0);

        let vbox3000 = gtk::Box::new(Orientation::Vertical, 0);
        vbox3000.pack_start(&vbox2000, false, false, 0);
        vbox3000.pack_start(&vbox, false, false, 0);
        vbox3000.pack_end(&hbox4000, false, false, 0);
        vbox.set_margin(16);
        hbox4000.set_margin_start(8);
        hbox4000.set_margin_end(8);
        hbox4000.set_margin_bottom(8);
        window.add(&vbox3000);
        window.show_all();
        window.resize(1, 1);

        //content_area.pack_start(&vbox, false, false, 8);

        content_area.set_margin_top(0);
        content_area.set_margin_start(8);
        content_area.set_margin_end(8);
        content_area.set_margin_bottom(8);

        profile_selected(id, ent_data.as_ref());

        dialog.connect_response(move |dialog, response| {
            if response == gtk::ResponseType::Yes {
                //let profile = ProfileVec::current();
                //*profile.get_mut("action").unwrap() = "remove".to_string();
                //let profiles = arc_data.lock().unwrap();
                // for (_id, item) in profile.data.iter() {
                //     let action = item.get("action").unwrap();
                //     if action == "create" {
                //         // TODO
                //         println!("{:?}", profile);
                //         //bhm::create_profile(&value);
                //     } else if action == "touched" {
                //         //             let mut profile_data = vec![];
                //         //             for (key, value) in profile {
                //         //                 if key == "action" {
                //         //                     // TODO
                //         //                     println!("id: {}.", id);
                //         //                     continue;
                //         //                 }
                //         //                 let val = if key == "fan_pwm_cpu_enable" {
                //         //                     if value == "true" {
                //         //                         "2"
                //         //                     } else {
                //         //                         "0"
                //         //                     }
                //         //                 } else {
                //         //                     &value
                //         //                 };
                //         //                 profile_data.push(vec![String::from(key), String::from(val)]);
                //         //                 // TODO
                //         //                 println!("    {}: {}.", key, val);
                //         //             }
                //         //
                //         //             //tx.send(Evt::ListenerProfile(id.to_string(), profile)).ok();
                //         //         } else if action == "remove" {
                //         //             // TODO
                //         //             bhm::remove_profile(id);
                //     }
                // }
                //let mut profile: Vec<Vec<String>> = vec![];
                //profile.push(vec![String::from("id"), id.to_string()]);
                //profile.push(vec![String::from("name"), name.to_string()]);
                //profile.push(vec![
                //    String::from("cpu0-boost"),
                //    cpu0_boost.to_string(),
                //]);
                //profile.push(vec![String::from("fan_pwm_cpu_enable"), fan_pwm_cpu_enable.to_string()]);
                //profile.push(vec![
                //    String::from("fan0-ttp"),
                //    fan0_ttp.to_string(),
                //]);
            }
            ProfileVec::clear();
            dialog.close();
        });

        //dialog.show_all();

        // TODO grab_focus and hide data entry serialize.
        dialog.resize(1, 1);
        hbox1000.hide();
        ent_fan_curve.hide();
        window
    }

    fn profile_apply(widget: &gtk::Widget) {
        for id in ProfileVec::current().profile_all_ids().iter() {
            let profile = ProfileVec::from_id(id);
            let action = profile.get("action");

            // TODO
            if action == "create" {
                let mut profile_data: Vec<Vec<String>> = vec![];
                for item in profile.profile_data().iter() {
                    profile_data.push(vec![item[0].to_string(), item[1].to_string()]);
                }
                bhm::create_profile(profile_data);
            }
            // TODO
            else if action == "remove" {
                bhm::remove_profile(id);
            }
            // TODO
            else if action == "touched" {
                let mut profile_data: Vec<Vec<String>> = vec![];
                for item in profile.profile_data().iter() {
                    profile_data.push(vec![item[0].to_string(), item[1].to_string()]);
                }
                bhm::update_profile(id, profile_data);
            }
        }
        let window = widget::to_top(widget).downcast::<gtk::Window>().unwrap();
        window.close();
    }
}
