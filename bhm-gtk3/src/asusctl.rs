use zbus::{Proxy, Message};
use std::process::Command;
//use std::rusb;
use crate::bus;

#[cfg(feature = "dbus")]
use zvariant_derive::Type;

#[cfg_attr(feature = "dbus", derive(Type))]
#[derive(Deserialize, Serialize, Type, Default, Debug, Clone)]
pub struct CurveData {
    pub(crate) fan: u32,
    pub(crate) temp: [u8; 8],
    pub(crate) pwm: [u8; 8],
}

pub fn cmd(command: &str) -> String {
    let mut result = "".to_string();
    let output = Command::new("sh")
        .arg("-c")
        .arg(command)
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
    if output.status.success() {
        result = String::from_utf8_lossy(&output.stdout).to_string();
    } else {
        let stderr = String::from_utf8_lossy(&output.stderr);
        print!("stderr: {}", stderr);
    }
    result
}

pub struct Asusctl {
    version: String,
}

impl Asusctl {
    pub fn new() -> Self {

        //let mut enumerator = udev::Enumerator::new().unwrap();
        //println!("{}", enumerator);

        let versions = cmd("asusctl -v");
        let versions: Vec<&str> = versions.split('\n').collect();
        let mut version: String = String::new();
        for v in versions {
            let pats: Vec<&str> = v.trim().split(" v").collect();
            if pats[0] == "asusd" {
                version = pats[1].trim().to_string();
            }
        }
        println!("asusd version: {:?}", version);
        Asusctl {
            version
        }
    }

    pub fn get_version(&self) -> &str {
        &self.version
    }

    pub fn get_version_major(&self) -> usize {
        if self.version.len() > 0 {
            let v = self.version.split('.').collect::<Vec<&str>>();
            v[0].parse::<usize>().unwrap()
        } else {
            0
        }
    }

    pub fn get_profile_id(&self) -> String {
        if self.get_version_major() == 4 {
            self.active_profile()
        } else if self.get_version_major() == 3 {
            self.active_name()
        } else {
            String::new()
        }
    }

    pub fn set_profile_id(&self, profile: &str) {
        if self.get_version_major() == 4 {
            self.set_active_profile(profile);
        } else if self.get_version_major() == 3 {
            self.set_profile(profile);
        }
    }

    fn call_method<B>(&self, path: &str, method: &str, body: &B) -> Option<zbus::Message>
        where
            B: serde::ser::Serialize + zvariant::Type,
    {
        let connection = zbus::Connection::new_system().unwrap();
        let query = connection.call_method(
            Some("org.asuslinux.Daemon"),
            path,
            Some("org.asuslinux.Daemon"),
            method,
            body,
        );
        match query {
            Ok(result) => Some(result),
            Err(e) => {
                eprintln!("Error {} {} {}", path, method, e);
                Option::None
            }
        }
    }

    // Profile 3
    pub fn get_profile_names(&self) -> Vec<String> {
        let mut names: Vec<String> = vec![];
        if self.get_version_major() == 4 {
            let profiles = self.profiles();
            if !profiles.is_empty() {
                for id in profiles.iter() {
                    names.push(id.to_string());
                }
            }
        } else {
            names = self.profile_names();
        }
        names
    }

    pub fn active_name(&self) -> String {
        let message = self.call_method(
            "/org/asuslinux/Profile",
            "ActiveName",
            &(),
        );
        match message {
            Some(message) => {
                match message.body::<String>() {
                    Ok(body) => body,
                    Err(error) => {
                        eprintln!("Error active_name body: {}.", error);
                        String::new()
                    }
                }
            }
            None => {
                eprintln!("Error active_name to next active_profile.");
                self.active_profile()
            }
        }
    }
    pub fn active_data(&self) -> bus::Profile {
        let message = self.call_method("/org/asuslinux/Profile", "ActiveData", &());
        //bus::Profile::new(p.0, p.1, p.2, p.3, p.4, p.5)
        match message {
            Some(message) => {
                match message.body::<bus::Profile>() {
                    Ok(body) => body,
                    Err(error) => {
                        eprintln!("Error active_data body: {}.", error);
                        bus::Profile::default()
                    }
                }
            }
            None => {
                eprintln!("Error active_data.");
                bus::Profile::default()
            }
        }
    }
    pub fn new_or_modify(&self, profile: bus::Profile) -> Option<Message> {
        self.call_method(
            "/org/asuslinux/Profile",
            "NewOrModify",
            &(profile),
        )
    }
    pub fn next_profile(&self) {
        let reply = self.call_method(
            "/org/asuslinux/Profile",
            "NextProfile",
            &(),
        );
        if reply.is_some() {
            println!("Asusctl next_profile {:?}", reply.unwrap().fields());
        }
    }
    pub fn set_profile(&self, profile: &str) {
        let reply = self.call_method(
            "/org/asuslinux/Profile",
            "SetProfile",
            &(profile),
        );
        if reply.is_some() {
            println!("Asusctl set_profile {}, {:?}", profile, reply.unwrap().fields());
        }
    }

    pub fn profile_names(&self) -> Vec<String> {
        let message = self.call_method("/org/asuslinux/Profile", "ProfileNames", &());
        match message {
            Some(message) => {
                match message.body::<Vec<String>>() {
                    Ok(body) => body,
                    Err(error) => {
                        eprintln!("Error profile_names body: {}.", error);
                        vec![]
                    }
                }
            }
            None => {
                eprintln!("Error profile_names.");
                let profiles = self.profiles();
                let mut data = vec![];
                if !profiles.is_empty() {
                    for id in profiles.iter() {
                        data.push(id.to_string());
                    }
                }
                data
            }
        }
    }
    pub fn profile_remove(&self, profile: &str) {
        let reply = self.call_method("/org/asuslinux/Profile", "Remove", &(profile));
        if reply.is_some() {
            println!("Asusctl profile_remove {}, {:?}", profile, reply.unwrap().fields());
        }
    }
    pub fn data_profiles(&self) -> Vec<Vec<String>> {
        let mut data: Vec<Vec<String>> = Vec::new();
        if self.get_version_major() == 4 {
            data.push(vec![String::from("0"), String::from("Balanced")]);
            data.push(vec![String::from("1"), String::from("Performance")]);
            data.push(vec![String::from("2"), String::from("Quiet")]);
        } else if self.get_version_major() == 3 {
            for (i, item) in self.get_profile_names().iter().enumerate() {
                data.push(vec![i.to_string(), item.to_string()]);
            }
        }
        data
    }

    pub fn data_fan_presets(&self) -> Vec<Vec<String>> {
        // let fan_presets: Vec<Vec<String>> = vec![
        //     vec![String::from("1"), String::from("🔴 Boost")],
        //     vec![String::from("0"), String::from("🟡 Normal")],
        //     vec![String::from("2"), String::from("🟢 Silent")],
        // ];
        let fan_presets: Vec<Vec<String>> = vec![
            vec![String::from("1"), String::from("Boost")],
            vec![String::from("0"), String::from("Normal")],
            vec![String::from("2"), String::from("Silent")],
        ];
        fan_presets
    }

    // Profile 4
    pub fn profiles(&self) -> Vec<u32> {
        let message = self.call_method("/org/asuslinux/Profile", "Profiles", &());
        match message {
            Some(message) => {
                match message.body::<Vec<u32>>() {
                    Ok(body) => body,
                    Err(error) => {
                        eprintln!("Error profiles body: {}.", error);
                        vec![]
                    }
                }
            }
            None => {
                eprintln!("Error profiles.");
                vec![]
            }
        }
    }
    pub fn active_profile(&self) -> String {
        let message = self.call_method(
            "/org/asuslinux/Profile",
            "ActiveProfile",
            &(),
        );
        match message {
            Some(message) => {
                match message.body::<u32>() {
                    Ok(body) => body.to_string(),
                    Err(error) => {
                        eprintln!("Error active_profile body: {}.", error);
                        String::from("")
                    }
                }
            }
            None => {
                eprintln!("Error active_profile.");
                String::new()
            }
        }
    }
    pub fn set_active_profile(&self, profile: &str) {
        let reply = self.call_method(
            "/org/asuslinux/Profile",
            "SetActiveProfile",
            &(profile.parse::<u32>().unwrap()),
        );
        if reply.is_some() {
            println!("Asusctl set_active_profile {}, {:?}", profile, reply.unwrap().fields());
        }
    }
    pub fn set_fan_curve(&self, profile: u32, curve: CurveData) {
        let body: (u32, CurveData) = (profile, curve);
        //let curve: (u32, [u8;8], [u8;8]) = (0, a, b);
        //let body: (u32, (u32, [u8;8], [u8;8])) = (2, (0, a, b));
        let message = self.call_method(
            "/org/asuslinux/Profile",
            "SetFanCurve",
            &body
        );
        if message.is_some() {
            println!("Asusctl SetFanCurve {:?}", message.unwrap().fields());
        }
    }
    pub fn set_fan_curve_enabled(&self, profile: u32, enabled: bool) {
        let message = self.call_method(
            "/org/asuslinux/Profile",
            "SetFanCurveEnabled",
            &(profile, enabled)
        );
        if message.is_some() {
            println!("Asusctl SetFanCurveEnabled {:?}", message.unwrap().fields());
        }
    }


    // Charge limit
    pub fn get_charge_limit(&self) -> i16 {
        let message = self.call_method(
            "/org/asuslinux/Charge",
            "Limit",
            &(),
        );
        match message {
            Some(message) => {
                match message.body::<i16>() {
                    Ok(body) => body,
                    Err(error) => {
                        eprintln!("Error get_charge_limit body: {}.", error);
                        0
                    }
                }
            }
            None => {
                eprintln!("Error get_charge_limit.");
                0
            }
        }
    }
    pub fn set_charge_limit(&self, limit: u8) {
        let reply = self.call_method(
            "/org/asuslinux/Charge",
            "SetLimit",
            &(limit),
        );
        if reply.is_some() {
            println!("Asusctl set_charge_limit {}, {:?}", limit, reply.unwrap().fields());
        }
    }
    pub fn data_charge_limits(&self) -> Vec<Vec<String>> {
        let mut charge_limits: Vec<Vec<String>> = Vec::new();
        for i in 0..81 {
            charge_limits.push(vec![format!("{}", i + 20), format!("{}", i + 20)]);
        }
        charge_limits
    }

    // Led
    pub fn get_brightness(&self) -> i16 {
        let connection = zbus::Connection::new_system().unwrap();
        let p = Proxy::new(
            &connection,
            "org.asuslinux.Daemon",
            "/org/asuslinux/Led",
            "org.asuslinux.Daemon",
        )
            .unwrap();
        let mut brightness: i16 = 0;
        match p.get_property("LedBrightness") {
            Ok(reply) => brightness = reply,
            Err(e) => eprintln!("{:?}", e)
        }
        println!("asusctl get_brightness {:?}", brightness - 48);
        brightness - 48
    }
    pub fn set_brightness(&self, brightness: u32) {
        let reply = self.call_method(
            "/org/asuslinux/Led",
            "SetBrightness",
            &(brightness),
        );
        if reply.is_some() {
            println!(
                "asusctl set_brightness {}, {:?}",
                brightness,
                reply.unwrap().fields()
            );
        }
    }
}

    // pwm
    // 1)   0 - 51
    // 2)  51 - 58
    // 3)  58 - 71
    // 4)  71 - 90
    // 5)  89 - 102
    // 6) 102 - 114
    // 7) 114 - 159
    // 8) 135 - 255
    // temp
    // 1)  0 - 50
    // 2) 50 - 55
    // 3) 55 - 60
    // 4) 60 - 65
    // 5) 65 - 70
    // 6) 70 - 75
    // 7) 75 - 98
    // 8) 98 - 255
    // cat /sys/class/hwmon/hwmon7/pwm1_auto_point*_pwm
    // echo 0 | sudo tee /sys/class/hwmon/hwmon7/pwm1_auto_point{1..8}_pwm
    // cat /sys/class/hwmon/hwmon7/pwm1_auto_point*_pwm
    // echo 2 | sudo tee /sys/class/hwmon/hwmon7/pwm1_enable
    // echo 1 | sudo tee /sys/class/hwmon/hwmon7/pwm1_enable
    // echo 255 | sudo tee /sys/class/hwmon/hwmon7/pwm1_auto_point{8..1}_pwm
    // cat /sys/class/hwmon/hwmon7/pwm1_auto_point*_pwm
    // echo 2 | sudo tee /sys/class/hwmon/hwmon7/pwm1_enable
    // echo 1 | sudo tee /sys/class/hwmon/hwmon7/pwm1_enable
    // echo 2 | sudo tee /sys/class/hwmon/hwmon7/pwm1_enable