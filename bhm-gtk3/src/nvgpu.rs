// cat /sys/bus/pci/devices/0000:01:00.0/power/runtime_status
// query = "nvidia-smi --query-gpu={units} --format=csv,noheader"

use std::process::Command;

pub fn cmd(command: &str) -> String {
    let mut result = "".to_string();
    let output = Command::new("sh")
        .arg("-c")
        .arg(command)
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
    if output.status.success() {
        result = String::from_utf8_lossy(&output.stdout).to_string();
    } else {
        let stderr = String::from_utf8_lossy(&output.stderr);
        print!("stderr: {}", stderr);
    }
    result
}

#[derive(Clone, Debug)]
pub struct Nvgpu {}

impl Nvgpu {
    pub fn new() -> Self {
        Nvgpu {}
    }
    pub fn data(&self) -> Vec<f32> {
        let query = "nvidia-smi --query-gpu=temperature.gpu,utilization.gpu,utilization.memory,power.draw --format=csv,noheader";
        let result = cmd(query)
            .trim()
            .replace(", ", ",")
            .replace(" %", "")
            .replace(" W", "");
        let mut data: Vec<f32> = vec![0f32, 0f32, 0f32, 0f32];
        if !result.is_empty() {
            let pats: Vec<&str> = result.split(',').collect();
            data = vec![
                pats[0].parse::<f32>().unwrap(),
                pats[1].parse::<f32>().unwrap(),
                pats[2].parse::<f32>().unwrap(),
                pats[3].parse::<f32>().unwrap(),
            ];
        }
        // data.push(pats[0].parse::<f32>().unwrap());
        // data.push(pats[1].parse::<f32>().unwrap());
        // data.push(pats[2].parse::<f32>().unwrap());
        // data.push(pats[3].parse::<f32>().unwrap());
        data
    }
    // pub fn gpu_temp(&self) -> f32 {
    //     let query = "nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader";
    //     let result = cmd(query)
    //         .trim().parse::<f32>().unwrap();
    //     return result;
    // }
    // pub fn gpu_power(&self) -> f32 {
    //     let query = "nvidia-smi --query-gpu=power.draw --format=csv,noheader";
    //     let result = cmd(query)
    //         .trim().replace(" W", "").parse::<f32>().unwrap();
    //     return result;
    // }
    // pub fn gpu_percent(&self) -> f32 {
    //     let query = "nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader";
    //     let result = cmd(query)
    //         .trim().replace(" %", "").parse::<f32>().unwrap();
    //     return result;
    // }
    // pub fn mem_percent(&self) -> f32 {
    //     let query = "nvidia-smi --query-gpu=utilization.memory --format=csv,noheader";
    //     let result = cmd(query)
    //         .trim().replace(" %", "").parse::<f32>().unwrap();
    //     return result;
    // }
}
