use std::{collections::HashMap, sync::mpsc::Sender};

use gtk::{glib, prelude::*};

use crate::utl::{read_file_to_number, Evt};

use crate::data::{C_ACTION, C_ID, C_LABEL, C_PACK, C_PID, C_ROLE, C_TYPE};
use crate::{amdgpu, bat, cpu, nvgpu};
use crate::{bhm, tree, widget};

pub struct Panel {
    is: HashMap<String, bool>,
    acts: HashMap<String, Vec<String>>,
    pub system: cpu::Cpu,
    pub agpu: amdgpu::Amdgpu,
    pub ngpu: nvgpu::Nvgpu,
    pub bat: bat::Bat,

    pub is_tick: bool,
    pub interval: u64,
    pub is_source: bool,
    pub source_id: glib::SourceId,

    pub widget: widget::Vbx,
    pub items: HashMap<String, widget::Box>,
    pub vbx: HashMap<String, widget::Vbx>,
    pub spn: HashMap<String, gtk::SpinButton>,
    pub scl: HashMap<String, widget::Scl>,
    pub lbl: HashMap<String, widget::Lbl>,
    pub btn: HashMap<String, gtk::Button>,
    pub swt: HashMap<String, widget::Swt>,
    pub cmb: HashMap<String, widget::Cmb>,

    pub tree: tree::Tree,

    pub path_temp_nvme: String,
    pub path_temp_wifi: String,
    pub path_fan0_rpm_cpu: String,

    temps: Vec<Vec<isize>>,
    pub data: Vec<Vec<String>>,
}

impl Panel {
    pub fn new(
        acts: HashMap<String, Vec<String>>,
        cpu: cpu::Cpu,
        agpu: amdgpu::Amdgpu,
        ngpu: nvgpu::Nvgpu,
        bat: bat::Bat,
    ) -> Self {
        let mut is: HashMap<String, bool> = HashMap::new();
        is.insert(String::from("system"), true); // run all       > 4 ms.
        is.insert(String::from("agpu"), false); // read all file ~ 1 ms.
        is.insert(String::from("ngpu"), false); // run all       one query ~12-15 ms, ~ 47-52 ms.
        is.insert(String::from("nvme"), false); // read file     ~ 5-6 ms.
        is.insert(String::from("wifi"), false); // read file     < 1 ms.
        is.insert(String::from("fan"), false); // read file     < 1ms
        is.insert(String::from("bat"), false); // read all file ~ 2 ms
        is.insert(String::from("tree"), true); // run all       ~ 15-17 ms.
        Panel {
            is,
            acts,
            system: cpu,
            agpu,
            ngpu,
            bat,

            is_tick: true,
            interval: 1000,
            is_source: true,
            source_id: glib::timeout_add_local(std::time::Duration::from_millis(1), move || {
                glib::Continue(false)
            }),

            tree: tree::Tree::new(),
            widget: widget::Vbx::new(),

            items: HashMap::new(),
            vbx: HashMap::new(),
            spn: HashMap::new(),
            scl: HashMap::new(),
            lbl: HashMap::new(),
            btn: HashMap::new(),
            swt: HashMap::new(),
            cmb: HashMap::new(),

            path_fan0_rpm_cpu: String::new(),
            path_temp_nvme: String::new(),
            path_temp_wifi: String::new(),

            temps: vec![],
            data: vec![],
        }
    }

    pub fn is(&self, key: &str) -> bool {
        *self.is.get(key).unwrap()
    }
    pub fn set_is(&mut self, key: &str, value: bool) {
        *self.is.get_mut(key).unwrap() = value;
    }
    pub fn build_ui(&mut self, tx: &Sender<Evt>, data: &[Vec<String>]) {
        let profiles = &bhm::data_profiles();
        let profile_id = bhm::get_profile();
        //let cpu0_boost = bhm::get_cpu_boost();
        //let fan0_ttp = bhm::get_fan_ttp();
        //let limit = bhm::get_bat_limit();
        //let brightness = bhm::get_kbd_brightness();

        self.widget.set_widget_name("panel");
        self.widget.widget.style_context().add_class("bas-panel");

        self.data = data.to_owned();

        for item in self.data.iter() {
            let id = &item[C_ID];
            let pid = &item[C_PID];
            if id == "ID" {
                continue;
            } else if &item[C_TYPE] == "itm" {
                let obj = widget::Box::new();
                obj.set_widget_name(id);
                obj.widget.style_context().add_class("item");
                obj.widget.style_context().add_class(&item[C_ROLE]);
                self.pack(&item[C_PACK], &obj.widget, false, false, 0);
                self.items.insert(id.to_string(), obj);
            } else if &item[C_TYPE] == "vbx" {
                let obj = widget::Vbx::new();
                obj.set_widget_name(id);
                obj.widget.style_context().add_class("vbx");
                obj.widget.style_context().add_class(&item[C_ROLE]);
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            false,
                            false,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            true,
                            false,
                            0,
                        );
                    }
                }
                self.vbx.insert(id.to_string(), obj);
            } else if &item[C_TYPE] == "lbl" {
                let role = &item[C_ROLE];
                let xalign: f32;
                let class_name = role;
                let obj = widget::Lbl::new(&item[C_LABEL]);
                if role == "left" {
                    xalign = 1.0;
                } else {
                    xalign = 0.0;
                }
                obj.set_widget_name(id);
                obj.widget.set_xalign(xalign);
                if !class_name.is_empty() {
                    obj.widget.style_context().add_class(class_name);
                }
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            false,
                            false,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            true,
                            false,
                            0,
                        );
                    }
                }
                self.lbl.insert(id.to_string(), obj);
            } else if &item[C_TYPE] == "cmb" {
                let mut obj = widget::Cmb::new(&[]);
                obj.set_widget_name(id);
                obj.widget.style_context().add_class("cmb");
                obj.widget.style_context().add_class(&item[C_ROLE]);
                if &item[C_ACTION] == "bat0-limit" {
                    obj.set_data(&bhm::data_bat_limit());
                    //obj.set_value(&limit.to_string());
                } else if &item[C_ACTION] == "profile" {
                    obj.set_data(profiles);
                    for item1 in profiles.iter().filter(|x| x[0] == profile_id) {
                        obj.set_value(&item1[0]);
                    }
                } else if &item[C_ACTION] == "fan0-ttp" {
                    obj.set_data(&bhm::data_fan_ttp());
                    //     obj.set_value(&format!("{}", fan0-ttp));
                    //     //let mode = sysfs::Sysfs::get_throttle_thermal_policy();
                    //     let mode = bhm::get_fan_ttp().to_string();
                    //     if obj.get_value() != mode {
                    //         obj.set_value(&mode.to_string());
                    //     }
                }
                if !&item[C_ACTION].is_empty() {
                    widget::connect_cmb(id.to_string(), tx.clone(), &obj);
                }
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            false,
                            false,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            true,
                            false,
                            0,
                        );
                    }
                }
                self.cmb.insert(id.to_string(), obj);
            } else if &item[C_TYPE] == "btn" {
                let widget = gtk::Button::new();
                widget.set_label(&item[C_LABEL]);
                widget.set_widget_name(id);
                widget.style_context().add_class(&item[C_ROLE]);
                if !&item[C_ACTION].is_empty() {
                    widget::connect_btn(id.to_string(), tx.clone(), &widget);
                }
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &widget,
                            false,
                            false,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx
                            .get_mut(pid)
                            .unwrap()
                            .pack(&item[C_PACK], &widget, true, false, 0);
                    }
                }
                self.btn.insert(id.to_string(), widget);
            } else if &item[C_TYPE] == "swt" {
                let mut obj = widget::Swt::new();
                obj.set_widget_name(id);

                // TODO
                if item[C_ACTION] == "is_tree" {
                    obj.set_value(self.is("tree"));
                } else if item[C_ACTION] == "is_system" {
                    obj.set_value(self.is("system"));
                } else if item[C_ACTION] == "is_agpu" {
                    obj.set_value(self.is("agpu"));
                } else if item[C_ACTION] == "is_ngpu" {
                    obj.set_value(self.is("ngpu"));
                } else if item[C_ACTION] == "is_nvme" {
                    obj.set_value(self.is("nvme"));
                } else if item[C_ACTION] == "is_wifi" {
                    obj.set_value(self.is("wifi"));
                } else if item[C_ACTION] == "is_fan" {
                    obj.set_value(self.is("fan"));
                } else if item[C_ACTION] == "is_bat" {
                    obj.set_value(self.is("bat"));
                } else if item[C_ACTION] == "cpu0-boost" {
                    let cpu0_boost = bhm::get_cpu_boost() == 1;
                    if cpu0_boost != obj.get_value() {
                        obj.set_value(cpu0_boost);
                    }
                }
                // else if item[C_ACTION] == "wlp2s0+power_save" {
                //     //let mode = sysfs::Sysfs::get_wireless_power_save("wlp2s0");
                //     let mode = bhm::get_wireless_power_save("wlp2s0");
                //     obj.set_value(mode == 1);
                // }
                if !&item[C_ACTION].is_empty() {
                    widget::connect_swt(id.to_string(), tx.clone(), &obj);
                }
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            false,
                            false,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            true,
                            false,
                            0,
                        );
                    }
                }
                self.swt.insert(id.into(), obj);
            } else if &item[C_TYPE] == "spn" {
                let adjustment = gtk::Adjustment::new(1.0, 0.05, 60.0, 0.05, 0.0, 0.0);
                //let widget = gtk::SpinButton::with_range(0.0, 1000.0, 1.0);
                let widget = gtk::SpinButton::new(Option::Some(&adjustment), 0.0, 2);
                widget.set_widget_name(id);
                if !&item[C_ACTION].is_empty() {
                    widget::connect_spn(id.to_string(), tx.clone(), &widget);
                }
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &widget,
                            false,
                            false,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx
                            .get_mut(pid)
                            .unwrap()
                            .pack(&item[C_PACK], &widget, true, false, 0);
                    }
                }
                self.spn.insert(id.into(), widget);
            } else if &item[C_TYPE] == "scl" {
                //let obj = widget::Scl::new(brightness as f64);

                let max = if &item[C_ACTION] == "scr0-brightness" {
                    bhm::get_scr_max_brightness() as f64
                } else if &item[C_ACTION] == "kbd0-brightness" {
                    bhm::get_kbd_max_brightness() as f64
                } else {
                    0.0
                };

                let obj = widget::Scl::new(0.0, 0.0, max);
                obj.set_widget_name(id);
                if !&item[C_ACTION].is_empty() {
                    widget::connect_scl(id.to_string(), tx.clone(), &obj);
                }
                for parent in self.data.iter().filter(|x| &x[C_ID] == pid) {
                    if &parent[C_TYPE] == "itm" {
                        self.items.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            true,
                            true,
                            0,
                        );
                    } else if &parent[C_TYPE] == "vbx" {
                        self.vbx.get_mut(pid).unwrap().pack(
                            &item[C_PACK],
                            &obj.widget,
                            true,
                            false,
                            0,
                        );
                    }
                }
                self.scl.insert(id.into(), obj);
                //scale.set_value(1.0);
                // scale.connect_change_value(move |scale, st, value| {
                //     //let digits = scale.digits() as usize;
                //     //let value = scale.to_value();
                //     //format!("<{}>", value);
                // });
            }
        }
    }

    pub fn set_cb_data(&mut self, id: &str, data: &[Vec<String>]) {
        self.cmb.get_mut(id).unwrap().set_data(data);
    }
    pub fn set_cb_state(&mut self, id: &str, value: &str) {
        self.cmb.get_mut(id).unwrap().set_state(value);
    }
    pub fn set_sw_state(&mut self, id: &str, value: bool) {
        self.swt.get_mut(id).unwrap().set_state(value);
    }

    // TODO action panel?
    pub fn id_to_action(&self, id: &str) -> String {
        let actions = self
            .data
            .iter()
            .filter(|x| x[C_ID] == id)
            .collect::<Vec<&Vec<String>>>();
        if actions.is_empty() {
            ""
        } else {
            &actions[0][C_ACTION]
        }
        .to_string()
    }
    pub fn action_to_group(&self, action: &str) -> String {
        if action == "is_system" {
            "system"
        } else if action == "is_agpu" {
            "agpu"
        } else if action == "is_ngpu" {
            "ngpu"
        } else if action == "is_nvme" {
            "nvme"
        } else if action == "is_wifi" {
            "wifi"
        } else if action == "is_fan" {
            "fan"
        } else if action == "is_bat" {
            "bat"
        } else {
            ""
        }
        .to_string()
    }
    pub fn group_action_from_value(&mut self, group: &str, value: bool) {
        self.set_is(group, value);
        if self.is(group) {
            self.group_call_fn(group);
            if self.is_group_temp(group) {
                self.temp();
            }
        } else {
            self.ind_group_clean(group);
            if group == "system" {
                self.ind_group_clean("temp.cpu");
            } else if self.is_group_temp(group) {
                self.ind_group_clean(&format!("temp.{}", group));
            }
        }
    }

    pub fn update(&mut self) {
        let timestamp_nanos_start = timestamp_nanos();

        if self.is("system") {
            self.sys()
        }
        if self.is("agpu") {
            self.agpu();
        }
        if self.is("ngpu") {
            self.ngpu();
        }
        if self.is("fan") {
            self.fan();
        }
        if self.is("bat") {
            self.bat();
        }
        // if self.is_nvme {
        //     self.temps.push(vec![3, read_file_to_number(&self.path_temp_nvme) as isize]);
        // }
        // if self.is_wifi {
        //     self.temps.push(vec![4, read_file_to_number(&self.path_temp_wifi) as isize]);
        // }
        // TEMP ////////////////////////////////////////////////////////////////////////////////////
        self.temp();
        self.temps = vec![];
        // TREE ////////////////////////////////////////////////////////////////////////////////////
        if self.is("tree") {
            self.tree.update();
        }
        // CPU /////////////////////////////////////////////////////////////////////////////////////
        if self.is("system") {
            self.system.cpu_load_aggregate();
        }
        // End /////////////////////////////////////////////////////////////////////////////////////
        let action = "tick_time";
        for id in self.acts.get(action).unwrap().iter() {
            self.lbl.get_mut(id).unwrap().set_value(&format!(
                "{:.03}",
                (timestamp_nanos() - timestamp_nanos_start) as f32 / 1000000.0
            ));
        }
    }

    fn is_group_temp(&self, group: &str) -> bool {
        group == "system"
            || group == "agpu"
            || group == "ngpu"
            || group == "nvme"
            || group == "wifi"
    }
    fn group_call_fn(&mut self, group: &str) {
        if group == "system" {
            self.sys();
        } else if group == "agpu" {
            self.agpu();
        } else if group == "ngpu" {
            self.ngpu();
        } else if group == "fan" {
            self.fan();
        } else if group == "bat" {
            self.bat();
        }
    }
    fn ind_group_clean(&mut self, group: &str) {
        let predicate = |x: &&Vec<String>| -> bool {
            x[C_ACTION].len() >= group.len() && &x[C_ACTION][..group.len()] == group
        };
        for item in self.data.iter().filter(predicate) {
            if item[C_TYPE] == "lbl" {
                let w = self.lbl.get_mut(&item[C_ID]).unwrap();
                for class_name in w.widget.style_context().list_classes() {
                    if &class_name[..4] == "ind_" {
                        w.widget.style_context().remove_class(&class_name);
                    }
                }
                let mut join: Vec<String> = vec![];
                for line in w.widget.text().split('\n').collect::<Vec<&str>>() {
                    join.push(" ".repeat(line.len()));
                }
                w.set_value(&join.join("\n"));
            }
        }
    }

    fn sys(&mut self) {
        self.act_value(
            "system_cpu_percent",
            "0",
            " %",
            "",
            self.system.cpu_percentage(),
        );
        self.act_value(
            "system_mem_percent",
            "0",
            " %",
            "",
            self.system.mem_percentage(),
        );
    }
    fn agpu(&mut self) {
        let agpu_ram_used = self.agpu.gpu_ram_used().unwrap_or(0.0);
        let agpu_ram_total = self.agpu.gpu_ram_total().unwrap_or(0.0);
        let value = self.agpu.gpu_percent().unwrap_or(0.0);
        self.act_value("agpu_gpu_percent", "0", " %", "", value);
        let value = agpu_ram_used / agpu_ram_total * 100.0;
        self.act_value("agpu_mem_percent", "0", " %", "", value);
        self.act_power(
            "agpu_power_watt",
            " W",
            self.agpu.gpu_power().unwrap_or(0.0),
        );
    }
    fn ngpu(&mut self) {
        let data = self.ngpu.data();
        self.act_value("ngpu_gpu_percent", "0", " %", "", data[1]);
        self.act_value("ngpu_mem_percent", "0", " %", "", data[2]);
        self.act_power("ngpu_power_watt", " W", data[3]);
    }
    fn fan(&mut self) {
        // self.act_label(
        //     "fan0-rpm_cpu",
        //     &format!("{} RPM", read_file_to_string(&self.path_fan0_rpm_cpu)),
        // );
    }
    fn bat(&mut self) {
        let power_now = self.bat.power_now().unwrap_or(0.0);
        //let energy_now = self.bat.energy_now().unwrap_or(0.0);
        //let energy_full = self.bat.energy_full().unwrap_or(0.0);
        //let energy_full_design = self.bat.energy_full_design().unwrap_or(0.0);
        //let voltage_now = self.bat.voltage_now().unwrap_or(0.0);

        use systemstat::{Platform, System};
        let sys = System::new();
        // match sys.on_ac_power() {
        //     Ok(power) => {
        //         self.act_label("ac0-online", &format!("AC power: {}. ", power));
        //     }
        //     Err(x) => eprintln!(", AC power: error: {}", x),
        // }
        match sys.battery_life() {
            Ok(battery) => {
                //let value = battery.remaining_capacity * 100.0;
                //self.act_value("bat_system_percentage", "1", " %", "", value);
                self.act_label(
                    "bat0-system_remaining",
                    &format!(
                        " {}H {}M", // remaining
                        battery.remaining_time.as_secs() / 3600,
                        battery.remaining_time.as_secs() % 60
                    ),
                );
            }
            Err(x) => print!("\nBattery: error: {}", x),
        }

        // let value = energy_now / energy_full * 100.0;
        // self.act_value("bat0-charge", "1", " %", "", value);
        // let value = energy_full / energy_full_design * 100.0;
        // self.act_value("bat0-status", "1", " %", "", value);
        //self.act_power("bat0-voltage", " V", voltage_now / 1000000.0);
        self.act_power("bat0-power_watt", " W", power_now / 1000000.0);
    }
    fn temp(&mut self) {
        let types: Vec<String> = vec![
            String::from("cpu"),
            String::from("agpu"),
            String::from("ngpu"),
            String::from("nvme"),
            String::from("wifi"),
        ];

        self.temps = vec![];
        if self.is("system") {
            self.temps
                .push(vec![0, (self.system.cpu_temp() * 1000.0) as isize]);
        }
        if self.is("agpu") {
            self.temps.push(vec![
                1,
                (self.agpu.gpu_temp().unwrap_or(0.0) * 1000.0) as isize,
            ]); // 🌡
        }
        if self.is("ngpu") {
            let data = self.ngpu.data(); // TODO
            self.temps.push(vec![2, (data[0] * 1000.0) as isize]); // 🌡
        }
        if self.is("nvme") {
            self.temps
                .push(vec![3, read_file_to_number(&self.path_temp_nvme) as isize]);
        }
        if self.is("wifi") {
            self.temps
                .push(vec![4, read_file_to_number(&self.path_temp_wifi) as isize]);
        }

        let mut max: f32 = 0.0;
        let mut inds = vec![];
        for (i, item) in self.temps.clone().iter().enumerate() {
            let temp = format!("{:.1}", item[1] as f32 / 1000.0)
                .parse::<f32>()
                .unwrap();

            let a = &types[item[0] as usize];
            let action = "temp.".to_owned() + a;
            for item1 in self.data.clone().iter().filter(|x| x[C_ACTION] == action) {
                self.label_ind(&item1[C_ID], "v1", "°C", temp, &a.to_uppercase());
            }

            if (temp - max).abs() < f32::EPSILON {
                inds.push(i);
            } else if temp > max {
                inds = vec![i];
                max = temp;
            }
        }
        for ind in inds.iter() {
            let action = "temp.".to_owned() + &types[*ind];
            for item in self.data.clone().iter().filter(|x| x[C_ACTION] == action) {
                self.lbl
                    .get_mut(&item[C_ID])
                    .unwrap()
                    .widget
                    .style_context()
                    .add_class("ind_max");
            }
        }
    }

    fn act_label(&mut self, action: &str, value: &str) {
        for id in self.acts.get(action).unwrap().iter() {
            self.lbl.get_mut(id).unwrap().set_value(value);
        }
    }
    fn act_value(&mut self, action: &str, role: &str, unit: &str, prefix: &str, value: f32) {
        for id in self.acts.get(action).unwrap().clone().iter() {
            self.label_ind(id, role, unit, value, prefix);
        }
    }
    fn act_power(&mut self, action: &str, unit: &str, value: f32) {
        for id in self.acts.get(action).unwrap().iter() {
            self.lbl
                .get_mut(id)
                .unwrap()
                .set_value(&format!("{:6.2}{}", value, unit));
        }
    }
    fn label_ind(&mut self, id: &str, role: &str, unit: &str, value: f32, prefix: &str) {
        let lbl = self.lbl.get_mut(id).unwrap();
        let context = lbl.widget.style_context();
        let p;
        if value > 0.0 && value < 1.0 {
            p = 1
        } else {
            p = value as i32;
        }
        for c in context.list_classes().iter() {
            if c.contains("ind_") {
                context.remove_class(c);
            }
        }
        if role == "v1" {
            lbl.set_value(&format!("{}\n{:5.1}", prefix, value));
        } else {
            lbl.set_value(&format!("{}{:6.2}{}", prefix, value, unit));
        }
        context.add_class(&format!("ind_{}_{:03}", role, p));
    }
    fn pack<P: IsA<gtk::Widget>>(
        &self,
        pack: &str,
        child: &P,
        expand: bool,
        fill: bool,
        padding: u32,
    ) {
        if pack == "end" {
            self.widget.widget.pack_end(child, expand, fill, padding);
        } else {
            self.widget.widget.pack_start(child, expand, fill, padding);
        }
    }

    pub(crate) fn set_label_ind(
        label: gtk::Label,
        role: &str,
        unit: &str,
        value: f32,
        prefix: &str,
    ) {
        let context = label.style_context();
        let p;
        if value > 0.0 && value < 1.0 {
            p = 1
        } else {
            p = value as i32;
        }
        for c in context.list_classes().iter() {
            if c.contains("ind_") {
                context.remove_class(c);
            }
        }
        if role == "v1" {
            label.set_text(&format!("{}\n{:5.1}", prefix, value));
        } else {
            label.set_text(&format!("{}{:6.2}{}", prefix, value, unit));
        }
        context.add_class(&format!("ind_{}_{:03}", role, p));
    }
}

fn timestamp_nanos() -> i64 {
    chrono::Local::now().timestamp_nanos()
}
