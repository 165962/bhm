use crate::{scanner, utl::read_file_to_string};
use gtk::{glib, prelude::*};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;

const HW_PATH: &str = "/sys/class/hwmon";

const C_LABEL: u32 = 0;
const C_NAME: u32 = 1;
const C_NUM: u32 = 2;
const C_KEY: u32 = 3;
const C_INPUT: u32 = 4;
const C_VALUE: u32 = 5;
const C_UNIT: u32 = 6;
const C_PATH: u32 = 7;

const COLUMNS: &str = "{\"columns\": [\n\
    [\"Label\", \"1\", \"0.0\"],\n\
    [\"Name\", \"0\", \"0.0\"],\n\
    [\"Num\", \"0\", \"0.0\"],\n\
    [\"Key\", \"0\", \"0.0\"],\n\
    [\"Input\", \"0\", \"0.0\"],\n\
    [\"Value\", \"1\", \"0.0\"],\n\
    [\"Unit\", \"1\", \"0.0\"],\n\
    [\"PATH\", \"0\", \"0.0\"]\n\
]}";

#[derive(Serialize, Deserialize, Debug)]
pub(crate) struct Data {
    pub(crate) columns: Vec<Vec<String>>,
}

impl Data {
    pub fn read() -> Data {
        let serialized: String = COLUMNS.to_string();
        serde_json::from_str(&serialized).unwrap()
    }
}

#[derive(Clone)]
pub struct Tree {
    pub widget: gtk::TreeView,
    pub store: gtk::TreeStore,
}

impl Tree {
    pub fn new() -> Self {
        let mut column_types = [String::static_type(); 8];
        let data = Data::read();
        for (i, _item) in data.columns.iter().enumerate() {
            column_types[i] = String::static_type();
        }
        Tree {
            store: gtk::TreeStore::new(&column_types),
            widget: gtk::TreeView::new(),
        }
    }

    pub fn format_value(&self, name: &str, key: &str, input: &str) -> (String, String) {
        let mut unit: String = String::new();
        let mut value = input.to_string();
        if input.is_empty() {
        } else if key == "in" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000.0);
                unit = String::from("V");
            }
        } else if key == "fan" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                //let num = input.parse::<f64>().unwrap();
                //value = (num / 1000.0).to_string();
                unit = String::from("RPM");
            }
        } else if key == "curr" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000.0);
                unit = String::from("A");
            }
        } else if key == "freq" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000000.0);
                unit = String::from("MHz");
            }
        } else if key == "temp" {
            if name == "input"
                || name == "min"
                || name == "max"
                || name == "crit"
                || name == "alarm"
            {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000.0);
                unit = String::from("℃");
            }
        } else if key == "power" && name == "average" {
            // TODO
            use semver::{Version, VersionReq};
            let req = VersionReq::parse(">=5.15.10").unwrap();
            let ver = sys_info::os_release().unwrap();
            let ver = ver.split('-').collect::<Vec<&str>>();
            let version = Version::parse(ver[0]).unwrap();
            let divisor = if req.matches(&version) {
                1000.0
            } else {
                1000000.0
            };

            let num = input.parse::<f64>().unwrap();
            value = format!("{:.2}", num / divisor);
            unit = String::from("W");
        }

        (value, unit)
    }

    pub fn build_ui(&mut self) {
        self.widget.set_model(Some(&self.store));
        self.widget.set_widget_name("tree");
        self.widget.set_headers_visible(true);
        self.widget.set_enable_tree_lines(true);
        //self.widget.set_enable_grid_lines(TreeViewGridLines::Both);

        let data = Data::read();
        for (i, item) in data.columns.iter().enumerate() {
            self.append_text_column(
                i as i32,
                &item[0],
                &item[1] == "1",
                item[2].parse::<f32>().unwrap_or_default(),
            );
        }

        //let mut path_fan = String::new();
        let mut scan_dir = scanner::ScanDir::new().unwrap();
        scan_dir.scan(HW_PATH);

        use regex::Regex;
        let re = Regex::new(r"^([a-z]+)([0-9]+)$").unwrap();

        let mut data0: HashMap<String, String> = HashMap::new();
        for k0 in &scan_dir.data {
            let name = read_file_to_string(&format!("{}/name", k0.0));
            data0.insert(name.trim().to_string(), k0.0.to_string());
        }

        let mut sorted0: Vec<_> = data0.iter().collect();
        sorted0.sort_by_key(|a| a.0 /*.to_lowercase()*/);
        for (name0, path0) in sorted0.iter() {
            // TODO
            let label = name0.replace("ucsi_source_psy_", "").to_uppercase();

            let iter0 = self.store.insert_with_values(
                None,
                None,
                &[
                    (C_LABEL, &label),
                    (C_NAME, &name0),
                    (C_NUM, &""),
                    (C_KEY, &""),
                    (C_INPUT, &""),
                    (C_VALUE, &""),
                    (C_UNIT, &""),
                    (C_PATH, &format!("{}/name", path0)),
                ],
            );
            let data1 = scan_dir.data.get(&path0.to_string()).unwrap();
            //println!("{:?}", data1);
            let mut sorted1: Vec<_> = data1.iter().collect();
            sorted1.sort_by_key(|a| a.0);
            for (key1, val1) in sorted1.iter() {
                //if key1.to_string() == "name" && &val1.to_string() == "asus" {
                //    path_fan = path0.to_string();
                //}
                if *key1 != "name" {
                    let pats: Vec<&str> = key1.split('_').collect();
                    if pats[1] == "label"
                        || pats[1] == "min"
                        || pats[1] == "max"
                        || pats[1] == "alarm"
                        || pats[1] == "crit"
                    {
                        continue;
                    }
                    if pats[1] != "label" {
                        let caps = re.captures(pats[0]).unwrap();
                        let input = val1.trim();
                        let (value, unit) = self.format_value(pats[1], &caps[1], input);

                        let key = &caps[1].to_string();
                        let num = &caps[2].to_string();
                        let p = format!("{}/{}{}_label", path0, key, num);
                        let label = fs::read_to_string(p)
                            .unwrap_or_else(|_| key.to_string())
                            .trim()
                            .to_owned();

                        self.store.insert_with_values(
                            Some(&iter0),
                            None,
                            &[
                                (C_LABEL, &label),
                                (C_NAME, &pats[1]),
                                (C_NUM, &caps[2].to_string()),
                                (C_KEY, &caps[1].to_string()),
                                (C_INPUT, &input),
                                (C_VALUE, &value),
                                (C_UNIT, &unit),
                                (C_PATH, &format!("{}/{}", path0, &key1)),
                            ],
                        );
                    }
                }
            }
        }

        /*
        for k0 in &scan_dir.data {
            let string = fs::read_to_string(format!("{}/name", k0.0)).unwrap();
            let value = string.trim();
            let iter = self.store.insert_with_values(None, None, &[
                (C_NAME, &value),
                (C_NUM, &""),
                (C_KEY, &""),
                (C_INPUT, &""),
                (C_VALUE, &""),
                (C_UNIT, &""),
                (C_PATH, &format!("{}/name", k0.0))
            ]);

            let mut sorted: Vec<_> = k0.1.iter().collect();
            sorted.sort_by_key(|a| a.0);
            for (key, val) in sorted.iter() {
                if key.to_string() == "name" && &val.to_string() == "asus" {
                    path_fan = k0.0.to_string();
                }
                if key.to_string() != "name" {
                    let pats: Vec<&str> = key.split("_").collect();
                    if pats[1] != "label" {
                        let caps = re.captures(pats[0]).unwrap();
                        let input = val.trim();
                        let (value, unit) = self.format_value(&pats[1], &caps[1], input);
                        self.store.insert_with_values(Some(&iter), None, &[
                            (C_NAME, &pats[1]),
                            (C_NUM, &caps[2].to_string()),
                            (C_KEY, &caps[1].to_string()),
                            (C_INPUT, &input),
                            (C_VALUE, &value),
                            (C_UNIT, &unit),
                            (C_PATH, &format!("{}/{}", k0.0, &key))
                        ]);
                    }
                }
            }
        }
        */

        // selection and path manipulation
        // let main_selection = self.widget.selection();
        // main_selection.connect_changed(move |tree_selection| {
        //     tree_selection.unselect_all();
        //     //let (left_model, iter) = tree_selection.selected().expect("Couldn't get selected");
        //     //let mut path = left_model.path(&iter).expect("Couldn't get path");
        //     // get the top-level element path
        //     //while path.depth() > 1 {
        //     //    path.up();
        //     //}
        // });

        self.widget.expand_all();

        // TODO column resize
        // self.widget.connect_size_allocate(move|tree_view, b|{
        //     for (i, column) in tree_view.columns().iter().enumerate() {
        //         println!("{} {} {}", i, column.title().unwrap(), column.width());
        //     }
        // });
    }

    pub fn update(&self) {
        self.store
            .foreach(move |tree_store, _tree_path, tree_iter| {
                let x_name = tree_store.value(tree_iter, C_NAME as i32);
                let s_name = x_name.get::<String>().expect("Treeview, column.");
                let x_key = tree_store.value(tree_iter, C_KEY as i32);
                let s_key = x_key.get::<String>().expect("Treeview, column.");
                //let x_input = tree_store.value(tree_iter, C_INPUT);
                //let s_input = x_input.get::<String>().expect("Treeview, column");
                let x_path = tree_store.value(tree_iter, C_PATH as i32);
                let s_path = x_path.get::<String>().expect("Treeview, column.");

                let input = &read_file_to_string(&s_path);

                if s_name == "input"
                    || s_name == "min"
                    || s_name == "max"
                    || s_name == "crit"
                    || s_name == "alarm"
                    || s_name == "enable"
                    || s_name == "average"
                {
                    self.store
                        .set_value(tree_iter, C_INPUT as u32, &glib::Value::from(input));
                    let (value, unit) = self.format_value(&s_name, &s_key, input);
                    let g_value = glib::Value::from(&value);
                    self.store.set_value(tree_iter, C_VALUE, &g_value);
                    let g_value = glib::Value::from(&unit);
                    self.store.set_value(tree_iter, C_UNIT, &g_value);
                }
                //let mut l0 = tree_store.set_value(tree_iter, 0, &v0);
                //let mut l1 = tree_store.set_value(tree_iter, 0, &v1);
                //self.store.set(tree_iter, &[(0, &v0), (1, &v1)]);
                //println!("tree update {}", s);
                false
            });
    }

    fn append_text_column(&self, i: i32, title: &str, visible: bool, xalign: f32) {
        let column = gtk::TreeViewColumn::new();
        let cell = gtk::CellRendererText::new();
        //cell.set_alignment(pango::Alignment::Right);
        //normal = Gtk.CellRendererText(weight=Pango.Weight.NORMAL)
        //normal1 = Gtk.CellRendererText(weight=Pango.Weight.NORMAL)
        //normal1.set_property("xalign", Pango.Alignment.CENTER)

        column.set_title(title);
        column.set_visible(visible);
        column.set_clickable(true);
        column.set_resizable(true);
        column.set_alignment(xalign);
        column.pack_start(&cell, true);
        column.add_attribute(&cell, "text", i);

        let header_button = column.button().unwrap();

        header_button.connect_button_press_event(move |widget, event_button| {
            if event_button.button() == 3 {
                let menu = gtk::Menu::new();
                for column in widget
                    .parent()
                    .unwrap()
                    .downcast::<gtk::TreeView>()
                    .unwrap()
                    .columns()
                {
                    let menu_item = gtk::CheckMenuItem::with_label(&column.title().unwrap());
                    menu_item.set_active(column.is_visible());
                    let parent = widget.parent().unwrap();
                    menu_item.connect_activate(move |menu_item| {
                        let tree_view = parent.to_owned().downcast::<gtk::TreeView>().unwrap();
                        for column in tree_view.columns() {
                            if menu_item.label() == column.title() {
                                column.set_visible(!column.is_visible());
                            }
                        }
                    });
                    menu.append(&menu_item);
                }
                menu.show_all();
                menu.popup(
                    gtk::NONE_MENU_SHELL,
                    gtk::NONE_MENU_ITEM,
                    move |a: &gtk::Menu, b: &mut i32, c: &mut i32| {
                        println!("{:?} {:?} {:?} ", a, b, c);
                        true
                    },
                    event_button.button(),
                    event_button.time(),
                );
            }
            Inhibit(false)
        });

        self.widget.append_column(&column);
    }
}
