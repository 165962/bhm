use crate::bhm;
use std::sync::Mutex;
use unique_id::string::StringGenerator;
use unique_id::Generator;

lazy_static! {
    pub static ref PROFILES_VEC: Mutex<Vec<Vec<Vec<String>>>> = Mutex::new(vec![]);
}

pub(crate) struct ProfileVec {}

impl ProfileVec {
    pub(crate) fn new(id: &str) -> Self {
        match PROFILES_VEC.lock() {
            Ok(mut data) => {
                data.push(vec![
                    vec!["index".to_string(), "0".to_string()],
                    vec!["status".to_string(), "selection".to_string()],
                ]);
                for (i, item) in bhm::data_profiles().iter().enumerate() {
                    if item[0] == id {
                        match data[0][0].get_mut(1) {
                            Some(value) => *value = i.to_string(),
                            None => eprintln!("Error profile new set index."),
                        }
                    }
                    data.push(vec![
                        vec!["action".to_string(), "inited".to_string()],
                        vec!["id".to_string(), item[0].to_string()],
                        vec!["name".to_string(), item[1].to_string()],
                    ]);
                }
            }
            Err(err) => eprintln!("{}", err),
        }
        Self {}
    }
    pub(crate) fn current() -> Self {
        // match PROFILES_VEC_INDEX.lock() {
        //     Ok(index) => match PROFILES_VEC.lock() {
        //         Ok(data) => match data.get(*index) {
        //             Some(item) => {
        //                 let id = item[1][1].to_string();
        //             }
        //             None => {}
        //         },
        //         Err(err) => eprintln!("{}", err)
        //     }
        //     Err(err) => eprintln!("{}", err)
        // }
        Self {}
    }
    pub(crate) fn from_id(id: &str) -> Self {
        match PROFILES_VEC.lock() {
            Ok(mut data) => {
                let index = data.iter().position(|item| item[1][1] == *id).unwrap_or(0);
                // Profile set index.
                match data[0][0].get_mut(1) {
                    Some(value) => *value = index.to_string(),
                    None => eprintln!("Profile: {}, set index, none.", index),
                }
            }
            Err(err) => eprintln!("{}", err),
        };
        Self {}
    }

    pub(crate) fn create(&self) -> String {
        match PROFILES_VEC.lock() {
            Ok(mut data) => {
                let id = StringGenerator::default().next_id();
                data.push(vec![
                    vec!["action".to_string(), "create".to_string()],
                    vec!["id".to_string(), id.to_string()],
                    vec!["name".to_string(), "New Profile".to_string()],
                ]);
                id
            }
            Err(err) => {
                eprintln!("{}", err);
                String::new()
            }
        }
    }
    pub(crate) fn remove(&self) -> bool {
        match PROFILES_VEC.lock() {
            Ok(mut data) => {
                let index = data[0][0][1].parse::<usize>().unwrap_or(0);
                let action = &data[index][0][1].to_string();
                if action == "create" {
                    data.remove(index);
                } else {
                    // Profile set action.
                    match data[index][0].get_mut(1) {
                        Some(value) => *value = "remove".to_string(),
                        None => eprintln!("Profile: {}, set action: {}, none.", index, action),
                    }
                }
                true
            }
            Err(err) => {
                eprintln!("{}", err);
                false
            }
        }
    }
    pub(crate) fn get_index(&self) -> usize {
        let id = &self.id();
        let ids = self.profile_ids();
        ids.iter().position(|x| x == id).unwrap_or(0)
    }
    pub(crate) fn get_status(&self) -> String {
        match PROFILES_VEC.lock() {
            Ok(data) => data[0][1][1].to_string(),
            Err(err) => {
                eprintln!("{}", err);
                String::new()
            }
        }
    }
    pub(crate) fn get_profile(&self) -> Vec<Vec<String>> {
        match PROFILES_VEC.lock() {
            Ok(data) => match data[0][0][1].parse::<usize>() {
                Ok(index) => data[index].clone(),
                Err(err) => {
                    eprintln!("{}", err);
                    vec![]
                }
            },
            Err(err) => {
                eprintln!("{}", err);
                vec![]
            }
        }
    }
    pub(crate) fn set_id(&self, id: &str) -> &Self {
        match PROFILES_VEC.lock() {
            Ok(mut data) => {
                let index = data
                    .iter()
                    .position(|item| item[1][1] == *id && &item[0][1] != "remove")
                    .unwrap_or(0);
                // Profile set index.
                match data[0][0].get_mut(1) {
                    Some(value) => *value = index.to_string(),
                    None => eprintln!("Profile set index: {}, none.", index),
                }
            }
            Err(err) => eprintln!("{}", err),
        };
        self
    }
    pub(crate) fn set_status(&self, status: &str) {
        match PROFILES_VEC.lock() {
            // Profile set status.
            Ok(mut data) => match data[0][1].get_mut(1) {
                Some(value) => *value = status.to_string(),
                None => eprintln!("Profile set index: {}, none.", status),
            },
            Err(err) => eprintln!("{}", err),
        }
    }
    pub(crate) fn prev_id(&self) -> String {
        match PROFILES_VEC.lock() {
            Ok(data) => {
                let index = data[0][0][1].parse::<usize>().unwrap_or(0);
                let mut idx = 0;
                let mut ids = vec![];
                for (i, item) in data.iter().enumerate() {
                    if i > 0 && &item[0][1] != "remove" {
                        if i == index {
                            idx = ids.len();
                        }
                        ids.push(&item[1][1]);
                    }
                }
                let len = ids.len();
                if len > 1 {
                    ids[if idx < 1 { len - 1 } else { idx - 1 }].to_string()
                } else {
                    String::new()
                }
            }
            Err(err) => {
                eprintln!("{}", err);
                String::new()
            }
        }
    }
    pub(crate) fn next_id(&self) -> String {
        match PROFILES_VEC.lock() {
            Ok(data) => {
                let index = data[0][0][1].parse::<usize>().unwrap_or(0);
                let mut idx = 0;
                let mut ids = vec![];
                for (i, item) in data.iter().enumerate() {
                    if i > 0 && &item[0][1] != "remove" {
                        if i == index {
                            idx = ids.len();
                        }
                        ids.push(&item[1][1]);
                    }
                }
                let len = ids.len();
                if len > 1 {
                    ids[if idx + 2 > len { 0 } else { idx + 1 }].to_string()
                } else {
                    String::new()
                }
            }
            Err(err) => {
                eprintln!("{}", err);
                String::new()
            }
        }
    }

    pub(crate) fn get(&self, key: &str) -> String {
        match PROFILES_VEC.lock() {
            Ok(data) => {
                let index = data[0][0][1].parse::<usize>().unwrap_or(0);
                if index > 0 {
                    match data[index].iter().position(|x| x[0] == *key) {
                        Some(idx) => data[index][idx][1].to_string(),
                        None => {
                            eprintln!("Profile get index: {} none, key: {}.", index, key);
                            String::new()
                        }
                    }
                } else {
                    String::new()
                }
            }
            Err(err) => {
                eprintln!("{}", err);
                String::new()
            }
        }
    }
    pub(crate) fn set(&self, key: &str, value: &str) -> &Self {
        match PROFILES_VEC.lock() {
            Ok(mut data) => match data[0][0][1].parse::<usize>() {
                Ok(index) => match data[index].iter().position(|x| x[0] == *key) {
                    Some(idx) => match data[index][idx].get_mut(1) {
                        Some(val) => *val = value.to_string(),
                        None => {
                            eprintln!("Error profile set, index: {}, {} = {}.", index, key, value)
                        }
                    },
                    None => data[index].push(vec![key.to_string(), value.to_string()]),
                },
                Err(err) => eprintln!("{}", err),
            },
            Err(err) => eprintln!("{}", err),
        };
        self
    }
    pub(crate) fn len(&self) -> usize {
        self.profile_ids().len()
    }
    pub(crate) fn profile_data(&self) -> Vec<Vec<String>> {
        let id = &self.id();
        if &self.get("action") == "inited" {
            for item in bhm::profile_data(id) {
                self.set(&item[0], &item[1]);
            }
            self.set("action", "selected");
        }
        self.get_profile()
    }
    pub(crate) fn items_profiles(&self) -> Vec<Vec<String>> {
        let mut items = vec![];
        match PROFILES_VEC.lock() {
            Ok(data) => {
                for (i, item) in data.iter().enumerate() {
                    if i == 0 {
                        continue;
                    }
                    if item[0][1] != "remove" {
                        items.push(vec![
                            item[1][1].to_string(), // id
                            item[2][1].to_string(), // name
                        ]);
                    }
                }
                items
            }
            Err(err) => {
                eprintln!("{}", err);
                items
            }
        }
    }
    pub(crate) fn clear() {
        match PROFILES_VEC.lock() {
            Ok(mut data) => *data = vec![],
            Err(err) => eprintln!("{}", err),
        }
    }
    fn id(&self) -> String {
        match PROFILES_VEC.lock() {
            Ok(data) => {
                let index = data[0][0][1].parse::<usize>().unwrap_or(0);
                if index > 0 && index < data.len() {
                    data[index][1][1].to_string()
                } else {
                    String::new()
                }
            }
            Err(err) => {
                eprintln!("{}", err);
                String::new()
            }
        }
    }
    // fn index(&self) -> usize {
    //     match PROFILES_VEC.lock() {
    //         Ok(data) => data[0][0][1].parse::<usize>().unwrap_or(0),
    //         Err(err) => {
    //             eprintln!("{}", err);
    //             0
    //         }
    //     }
    // }
    pub(crate) fn profile_ids(&self) -> Vec<String> {
        let mut ids = vec![];
        match PROFILES_VEC.lock() {
            Ok(data) => {
                for (i, item) in data.iter().enumerate() {
                    if i > 0 && &item[0][1] != "remove" {
                        ids.push(item[1][1].to_string());
                    }
                }
            }
            Err(err) => eprintln!("{}", err),
        };
        ids
    }
    pub(crate) fn profile_all_ids(&self) -> Vec<String> {
        let mut ids = vec![];
        match PROFILES_VEC.lock() {
            Ok(data) => {
                for (i, item) in data.iter().enumerate() {
                    if i > 0 {
                        ids.push(item[1][1].to_string());
                    }
                }
            }
            Err(err) => eprintln!("{}", err),
        };
        ids
    }
}
