use std::io::Read;
use std::{fs, io};

fn read_file(path: &str) -> std::io::Result<String> {
    let mut s = String::new();
    fs::File::open(path)
        .and_then(|mut f| f.read_to_string(&mut s))
        .map(|_| s)
}

#[derive(Clone, Debug)]
pub struct Bat {
    pub path: String,
}

impl Bat {
    pub fn new(path: String) -> Self {
        Bat { path }
    }
    pub fn power_now(&self) -> io::Result<f32> {
        read_file(&format!("{}/{}", self.path, "power_now"))
            .and_then(|data| match data.trim().parse::<f32>() {
                Ok(x) => Ok(x),
                Err(_) => Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Could not parse float",
                )),
            })
            .map(|num| num / 1.)
    }
    // pub fn voltage_now(&self) -> io::Result<f32> {
    //     read_file(&format!("{}/{}", self.path, "voltage_now"))
    //         .and_then(|data| match data.trim().parse::<f32>() {
    //             Ok(x) => Ok(x),
    //             Err(_) => Err(std::io::Error::new(
    //                 std::io::ErrorKind::Other,
    //                 "Could not parse float",
    //             )),
    //         })
    //         .map(|num| num / 1.)
    // }
    // pub fn energy_now(&self) -> io::Result<f32> {
    //     read_file(&format!("{}/{}", self.path, "energy_now"))
    //         .and_then(|data| match data.trim().parse::<f32>() {
    //             Ok(x) => Ok(x),
    //             Err(_) => Err(std::io::Error::new(
    //                 std::io::ErrorKind::Other,
    //                 "Could not parse float",
    //             )),
    //         })
    //         .map(|num| num / 1.)
    // }
    // pub fn energy_full(&self) -> io::Result<f32> {
    //     read_file(&format!("{}/{}", self.path, "energy_full"))
    //         .and_then(|data| match data.trim().parse::<f32>() {
    //             Ok(x) => Ok(x),
    //             Err(_) => Err(std::io::Error::new(
    //                 std::io::ErrorKind::Other,
    //                 "Could not parse float",
    //             )),
    //         })
    //         .map(|num| num / 1.)
    // }
    // pub fn energy_full_design(&self) -> io::Result<f32> {
    //     read_file(&format!("{}/{}", self.path, "energy_full_design"))
    //         .and_then(|data| match data.trim().parse::<f32>() {
    //             Ok(x) => Ok(x),
    //             Err(_) => Err(std::io::Error::new(
    //                 std::io::ErrorKind::Other,
    //                 "Could not parse float",
    //             )),
    //         })
    //         .map(|num| num / 1.)
    // }
}
