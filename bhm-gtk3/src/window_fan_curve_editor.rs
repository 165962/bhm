use crate::widget;
use gtk::prelude::*;
use std::{cell::RefCell, collections::HashMap, rc::Rc};

pub struct DialogFanCurveEditor {
    dialog: gtk::Dialog,
    grid: gtk::Grid,
    head: Vec<Vec<String>>,
    head_widgets: Vec<Vec<gtk::Label>>,
    data: Vec<Vec<f64>>,
    store: Rc<RefCell<HashMap<String, (gtk::SpinButton, f64)>>>,
    step: f64,
    config: Vec<Vec<f64>>,
    is_empty: Rc<RefCell<bool>>,
}

impl DialogFanCurveEditor {
    pub fn new(parent: Option<&gtk::Widget>, curve: &str) -> Self {
        let title = "Fan curve";
        let parent = parent
            .as_ref()
            .map(|widget| widget::to_top(widget).downcast::<gtk::Window>().unwrap());

        Self {
            store: Rc::new(RefCell::new(HashMap::new())),
            is_empty: Rc::new(RefCell::new(curve.is_empty())),
            dialog: gtk::Dialog::with_buttons(
                Some(title),
                parent.as_ref(),
                gtk::DialogFlags::MODAL,
                &[],
            ),
            grid: gtk::Grid::new(),
            head: vec![
                vec![
                    String::from("PAIR"),
                    String::from("№"),
                    String::from("CELSIUS"),
                    String::from("PERCENTAGE OF ROTATION"),
                ],
                vec![
                    String::from("MIN"),
                    String::from("MAX"),
                    String::from("VALUE"),
                    String::from("MIN"),
                    String::from("MAX"),
                    String::from("VALUE"),
                ],
            ],
            head_widgets: vec![],
            data: Self::_parse_data(curve),
            step: 10.,
            config: vec![
                vec![30., 0., 100.],
                vec![49., 0., 100.],
                vec![50., 0., 100.],
                vec![69., 0., 100.],
                vec![70., 34., 100.],
                vec![89., 51., 100.],
                vec![90., 61., 100.],
                vec![109., 61., 100.],
            ],
        }
    }
    pub fn open<F: Fn(&gtk::Dialog, &str) + 'static>(&mut self, on_apply: F) {
        self.build_ui();

        let content_area = self.dialog.content_area();

        let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        let btn0 = gtk::Button::with_label("Apply");
        let btn1 = gtk::Button::with_label("Clear");
        let btn2 = gtk::Button::with_label("Cancel");
        let dialog0 = self.dialog.clone();
        let dialog1 = self.dialog.clone();
        let dialog2 = self.dialog.clone();

        let is_empty = self.is_empty.to_owned();
        let store = self.store.to_owned();

        btn0.connect_clicked(move |_| {
            on_apply(
                &dialog0,
                &if *is_empty.borrow() {
                    String::new()
                } else {
                    Self::_get_data(dialog0.as_ref())
                },
            );
        });

        let is_empty = self.is_empty.to_owned();
        btn1.connect_clicked(move |_| {
            Self::_on_empty(dialog1.as_ref(), &store);
            *is_empty.borrow_mut() = true;
        });
        btn2.connect_clicked(move |_| {
            dialog2.close();
        });

        hbox.pack_end(&btn0, false, false, 0);
        hbox.pack_end(&btn1, false, false, 0);
        hbox.pack_end(&btn2, false, false, 0);

        content_area.pack_start(&self.grid, false, false, 0);
        content_area.pack_end(&hbox, false, false, 0);

        content_area.set_margin_top(8);
        content_area.set_margin_start(16);
        content_area.set_margin_end(16);
        content_area.set_margin_bottom(8);

        self.dialog.show_all();

        // TODO
        if *self.is_empty.borrow() {
            Self::_on_empty(self.dialog.as_ref(), &self.store);
            *self.is_empty.borrow_mut() = true;
        }
    }

    fn build_ui(&mut self) {
        self.grid.set_row_spacing(0);
        self.grid.set_column_spacing(0);
        self.grid.set_column_homogeneous(false);
        self.grid.style_context().add_class("table");
        self._create_table_head();
        self._create_table_body();
    }

    fn _create_table_head(&mut self) {
        for (i, data) in self.head.iter().enumerate() {
            self.head_widgets.push(vec![]);
            for (j, label) in data.iter().enumerate() {
                let widget = gtk::Label::new(Some(label));
                let context = widget.style_context();
                context.add_class("item");
                if i == 0 {
                    context.add_class("top");
                    if j == 0 {
                        context.add_class("left");
                    }
                }
                self.head_widgets[i].push(widget);
            }
        }
        self.grid.attach(&self.head_widgets[0][0], 0, -2, 1, 2);
        self.grid.attach(&self.head_widgets[0][1], 1, -2, 1, 2);
        self.grid.attach(&self.head_widgets[0][2], 2, -2, 3, 1);
        self.grid.attach(&self.head_widgets[0][3], 5, -2, 3, 1);

        self.grid.attach(&self.head_widgets[1][0], 2, -1, 1, 1);
        self.grid.attach(&self.head_widgets[1][1], 3, -1, 1, 1);
        self.grid.attach(&self.head_widgets[1][2], 4, -1, 1, 1);
        self.grid.attach(&self.head_widgets[1][3], 5, -1, 1, 1);
        self.grid.attach(&self.head_widgets[1][4], 6, -1, 1, 1);
        self.grid.attach(&self.head_widgets[1][5], 7, -1, 1, 1);
    }
    fn _create_table_body(&mut self) {
        for i in 0..8 {
            let top = i;
            let num = i + 1;
            let even = num & 1;
            let pair = ((i / 2 + 1) as f64).ceil();
            let mut start: isize = -1;
            let mut labels: Vec<gtk::Label> = vec![];
            let mut widgets: Vec<Vec<String>> = vec![];
            let min_p = self.config[i][1];
            let max_p = self.config[i][2];
            let min_c: f64;
            let max_c: f64;
            let get_c: f64;
            let get_p: f64;
            if even > 0 {
                min_c = self.config[i][0];
                max_c = self.config[(i + 1)][0] - self.step;
            } else {
                min_c = self.config[(i - 1)][0] + self.step;
                max_c = self.config[i][0];
            }
            if self.data.is_empty() {
                get_c = min_c;
                get_p = min_p;
            } else {
                get_c = self.data[i][0];
                get_p = self.data[i][1];
            }
            if even > 0 {
                widgets.push(vec![labels.len().to_string(), "label".to_string()]);
                labels.push(gtk::Label::new(Some(&format!("{}", pair))));
            }
            widgets.push(vec![labels.len().to_string(), "label".to_string()]);
            labels.push(gtk::Label::new(Some(&num.to_string())));
            widgets.push(vec![labels.len().to_string(), "label".to_string()]);
            labels.push(gtk::Label::new(Some(&format!("{}", min_c))));
            widgets.push(vec![labels.len().to_string(), "label".to_string()]);
            labels.push(gtk::Label::new(Some(&format!("{}", max_c))));
            widgets.push(vec!["0".to_string(), "spn".to_string(), "0".to_string()]);
            widgets.push(vec![labels.len().to_string(), "label".to_string()]);
            labels.push(gtk::Label::new(Some(&format!("{}", min_p))));
            widgets.push(vec![labels.len().to_string(), "label".to_string()]);
            labels.push(gtk::Label::new(Some(&format!("{}", max_p))));
            widgets.push(vec!["0".to_string(), "spn".to_string(), "1".to_string()]);
            self._create_spn(&Self::n("celsius", i), get_c, min_c, max_c);
            self._create_spn(&Self::n("percent", i), get_p, min_p, max_p);
            for i1 in 0..8 {
                let mut h = 1;
                if i1 == 0 {
                    if even > 0 {
                        start = 0;
                        h = 2;
                    } else {
                        continue;
                    }
                }
                let wid = (start + i1) as usize;
                let cid = widgets[wid][0].parse::<usize>().unwrap();
                if widgets[wid][1] == "label" {
                    labels[cid].style_context().add_class("item");
                    self.grid.attach(&labels[cid], i1 as i32, top as i32, 1, h);
                } else {
                    let prefix = if widgets[wid][2].parse::<usize>().unwrap() == 0 {
                        "celsius"
                    } else {
                        "percent"
                    };
                    let name = &Self::n(prefix, i);
                    self.store
                        .borrow()
                        .get(name)
                        .unwrap()
                        .0
                        .style_context()
                        .add_class("item");
                    self.grid.attach(
                        &self.store.borrow().get(name).unwrap().0,
                        i1 as i32,
                        top as i32,
                        1,
                        h,
                    );
                }
            }
            for (wid, widget) in widgets.iter().enumerate() {
                let cid = widget[0].parse::<usize>().unwrap();
                let context: gtk::StyleContext;
                if widgets[wid][1] == "label" {
                    context = labels[cid].style_context();
                    context.add_class("item");
                    if even > 0 && wid == 0 {
                        context.add_class("left");
                    }
                }
                if widgets[wid][1] == "label" {}
            }
        }
    }
    fn _create_spn(&self, name: &str, value: f64, min: f64, max: f64) {
        let spin = gtk::SpinButton::with_range(min, max, 1f64);
        spin.set_value(value);
        spin.set_widget_name(name);
        self.store
            .borrow_mut()
            .insert(name.to_string(), (spin, value));
        let store = self.store.to_owned();
        let is_empty = self.is_empty.to_owned();
        self.store
            .borrow_mut()
            .get_mut(name)
            .unwrap()
            .0
            .connect_changed(move |spin: &gtk::SpinButton| {
                let name = &spin.widget_name().to_string();

                // TODO
                if spin.text().is_empty() {
                    return;
                } else if *is_empty.borrow() {
                    *is_empty.borrow_mut() = false;
                    let mut d = vec![];
                    for item in store.borrow().iter() {
                        d.push(item.0.to_string());
                    }
                    for name1 in d.iter() {
                        if name1 == name {
                            continue;
                        }
                        let min = store.borrow().get(name1).unwrap().0.range().0;
                        let w = widget::find_get(spin.as_ref(), name1).unwrap();
                        w.downcast::<gtk::SpinButton>().unwrap().set_value(min);
                    }
                }

                //if spin.value() != store.borrow_mut().get_mut(name).unwrap().1 {
                if (spin.value() - store.borrow_mut().get_mut(name).unwrap().1).abs() > f64::EPSILON
                {
                    let pats = name.split('_').collect::<Vec<&str>>();
                    let prefix = pats[0];
                    let index = pats[1].parse::<usize>().unwrap();
                    Self::_on_change(prefix, index, &store);
                    store.borrow_mut().get_mut(name).unwrap().1 = spin.value();
                }
            });
    }

    fn n(prefix: &str, index: usize) -> String {
        format!("{}_{}", prefix.to_string(), index)
    }

    fn _get_data(widget: &gtk::Widget) -> String {
        let mut store = vec![];
        for i in 0..8 {
            let name = &Self::n("celsius", i);
            let w = widget::find_get(widget, name).unwrap();
            let spin_celsius = w.downcast::<gtk::SpinButton>().unwrap();
            let name = &Self::n("percent", i);
            let w = widget::find_get(widget, name).unwrap();
            let spin_percent = w.downcast::<gtk::SpinButton>().unwrap();
            let celsius = format!("{}c", spin_celsius.value().to_string());
            let percent = format!("{}%", spin_percent.value().to_string());
            store.push(format!("{}:{}", celsius, percent));
        }
        store.join(",")
    }
    fn _parse_data(curve: &str) -> Vec<Vec<f64>> {
        let mut data = vec![];
        if !curve.is_empty() {
            for line in curve.split(',') {
                let pats: Vec<&str> = line.split("c:").collect();
                data.push(vec![
                    pats[0].parse::<f64>().unwrap(),
                    pats[1][..pats[1].len() - 1].parse::<f64>().unwrap(),
                ]);
            }
        }
        data
    }
    fn _on_empty(top: &gtk::Widget, store: &Rc<RefCell<HashMap<String, (gtk::SpinButton, f64)>>>) {
        let mut d = vec![];
        for item in store.borrow().iter() {
            d.push(item.0.to_string());
        }
        for name in d.iter() {
            if let Some(widget) = widget::find_get(top, name) {
                if let Ok(spin) = widget.downcast::<gtk::SpinButton>() {
                    spin.set_value(spin.range().0);
                    spin.set_text("");
                }
            }
        }
    }
    fn _on_change(
        prefix: &str,
        index: usize,
        store: &Rc<RefCell<HashMap<String, (gtk::SpinButton, f64)>>>,
    ) {
        let mut data = vec![];
        for i in 0..8 {
            let name = &Self::n(prefix, i);
            let item = store.borrow().get(name).unwrap().to_owned();
            let range = item.0.range();
            // min, val, max, old
            data.push(vec![range.0, item.0.value(), range.1, item.1]);
        }
        for item in if prefix == "celsius" {
            let step = 10f64;
            Self::_check_celsius(&data, index, step)
        } else {
            Self::_check_percent(&data, index)
        } {
            let name = &Self::n(prefix, item.0);
            store.borrow_mut().get_mut(name).unwrap().1 = item.1;
            let spin = store.borrow_mut().get(name).unwrap().0.to_owned();
            spin.set_value(item.1);
        }
    }
    fn _check_celsius(store: &[Vec<f64>], index: usize, step: f64) -> Vec<(usize, f64)> {
        let j = ((index / 2) as f64).ceil();
        let i = (j * 2.) as usize;
        let value = store[index][1];
        let mut value1 = store[i][1];
        let mut value2 = store[i + 1][1];
        if value1 < store[i][0] {
            value1 = store[i][0];
        }
        if value1 > store[i + 1][2] - step {
            value1 = store[i + 1][2] - step;
        }
        if value2 > store[i + 1][2] {
            value2 = store[i + 1][2];
        }
        if value2 < store[i][0] + step {
            value2 = store[i][0] + step;
        }
        if value < store[index][3] {
            if value2 < value1 + step {
                value1 = value2 - step;
            }
        } else if value > store[index][3] && value1 > value2 - step {
            value2 = value1 + step;
        }
        vec![(i, value1), (index, value), (i + 1, value2)]
    }
    fn _check_percent(store: &[Vec<f64>], index: usize) -> Vec<(usize, f64)> {
        let mut data = vec![];
        let mut value = store[index][1];
        if value < store[index][3] {
            for i in (0..=index).step_by(1) {
                let i = index - i;
                // max_value = &self.config[i][2]
                if value < store[i][0] {
                    value = store[i][0];
                    data.push((i, value));
                }
                if value < store[i][1] {
                    data.push((i, value));
                }
            }
        } else if value > store[index][3] {
            for i in (index + 1..8).step_by(1) {
                if value > store[i][1] {
                    data.push((i, value));
                }
            }
        }
        //self.old[index][1] = widget.value() as usize;
        data
    }
}
