use crate::{
    amdgpu, bhm, bus, cpu, data::Data, gnome, nvgpu, scanner, tree, utl, utl::read_file_to_string,
    utl::PnlEvt, widget, win::Win, window_fan_curve_editor::DialogFanCurveEditor,
    window_profile_editor::ProfileEditor,
};
use gtk::{
    glib::{GString, Sender, Type},
    prelude::*,
};
use indexmap::IndexMap;
use std::{cell::RefCell, collections::HashMap, fs, rc::Rc};

const CB_ID: usize = 2;
const CB_TYPE: usize = 3;
const CB_KEY: usize = 4;
const CB_VALUE: usize = 1;
const CB_ACTION: usize = 0;

const CT_ID: usize = 0;
const CT_PID: usize = 1;
const CT_ROLE: usize = 4;
const CT_TYPE: usize = 3;
const CT_OBJECT: usize = 5;
const CT_PROP: usize = 6;
const CT_PACK: usize = 2;
const CT_LABEL: usize = 7;

const NM_STATES: [[&str; 2]; 8] = [
    ["0", "Unknown"],                    // networking state is unknown
    ["10", "Networking is not enabled"], // NM_STATE_ASLEEP - networking is not enabled
    ["20", "Disconnected"],              // there is no active network connection
    ["30", "Disconnecting"],             // network connections are being cleaned up
    ["40", "Connecting"],                // a network connection is being started
    ["50", "Connected local"],           // there is only local IPv4 and/or IPv6 connectivity
    ["60", "Connected site"],            // there is only site-wide IPv4 and/or IPv6 connectivity
    ["70", "Connected global"],          // there is global IPv4 and/or IPv6 Internet connectivity
];

// TODO
const ASUS_FAN_PWM_CPU_DATA: [[&str; 4]; 2] =
    [["0", "0", "false", "Off"], ["1", "2", "true", "On"]];

pub struct Pnl {
    tx: Sender<PnlEvt>,
    now: std::time::Instant,
    top: gtk::Widget,
    win: Win,
    size: (i32, i32),
    data: Data,
    window: gtk::Window,
    windows: Rc<RefCell<HashMap<String, gtk::Window>>>,
    //actions: HashMap<String, String>,
    signals: HashMap<String, String>,
    timeouts: HashMap<String, u128>,
    nm_states: HashMap<String, String>,

    counter: usize,
    acts: HashMap<String, String>,
    olds: HashMap<String, String>,
    cmb: HashMap<String, gtk::ComboBoxText>,
    btn: HashMap<String, gtk::Button>,
    lbl: HashMap<String, gtk::Label>,
    prb: HashMap<String, gtk::ProgressBar>,
    scl: HashMap<String, gtk::Scale>,
    swt: HashMap<String, gtk::Switch>,
    swt_act_data: HashMap<String, HashMap<bool, String>>,

    temps: Vec<[String; 3]>,
    sys: cpu::Cpu,
    agpu: String,
    tree: tree::Tree,
}

impl Pnl {
    pub(crate) fn new(
        win: Win,
        data: Data,
        tx: Sender<PnlEvt>,
        windows: Rc<RefCell<HashMap<String, gtk::Window>>>,
    ) -> Self {
        let mut nm_states = HashMap::new();
        for item in NM_STATES {
            nm_states.insert(item[0].to_string(), item[1].to_string());
        }

        Self {
            tx,
            now: std::time::Instant::now(),
            window: widget::to_top(win.get_widget().as_ref())
                .downcast::<gtk::Window>()
                .unwrap(),
            windows,
            top: widget::to_top(win.get_widget().as_ref()),
            win,
            size: (0, 0),
            data,
            signals: HashMap::new(),
            nm_states,
            timeouts: HashMap::new(),

            counter: 0,
            acts: HashMap::new(),
            btn: HashMap::new(),
            cmb: HashMap::new(),
            lbl: HashMap::new(),
            prb: HashMap::new(),
            scl: HashMap::new(),
            swt: HashMap::new(),
            swt_act_data: HashMap::new(),
            olds: HashMap::new(),

            temps: vec![],
            sys: cpu::Cpu::new(),
            agpu: String::new(),
            tree: tree::Tree::new(),
        }
    }

    pub(crate) fn init(&mut self) {
        {
            let paned0 = gtk::Paned::new(gtk::Orientation::Horizontal);

            //self.panel.build_ui(&self.tx, &self.data.widgets);
            self.tree.build_ui();

            let scrolled_window0 =
                gtk::ScrolledWindow::new(gtk::NONE_ADJUSTMENT, gtk::NONE_ADJUSTMENT);
            scrolled_window0.set_policy(gtk::PolicyType::Never, gtk::PolicyType::Automatic);
            scrolled_window0.set_min_content_width(-1);
            //scrolled_window0.set_man_content_width(-1);
            let scrolled_window1 =
                gtk::ScrolledWindow::new(gtk::NONE_ADJUSTMENT, gtk::NONE_ADJUSTMENT);
            scrolled_window1.set_policy(gtk::PolicyType::Automatic, gtk::PolicyType::Automatic);
            //scrolled_window1.set_min_content_width(510);
            let pnl_box = gtk::Box::new(gtk::Orientation::Vertical, 0);
            pnl_box.set_widget_name("panel");
            pnl_box.style_context().add_class("bas-panel");
            scrolled_window0.add(&pnl_box);
            scrolled_window1.add(&self.tree.widget);
            ////////////////////////////////////////////////////////////////////////////////////////////
            paned0.pack1(&scrolled_window0, false, true);
            paned0.pack2(&scrolled_window1, true, true);
            let hbox = gtk::Box::new(gtk::Orientation::Horizontal, 0);
            hbox.pack_start(&paned0, true, true, 0);
            self.win.add(&hbox);
            self.win.show_all();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////

        //
        // Navigation, Temperature, System, Objects.
        //

        let pnl_wdg = widget::find_get(&self.top, "panel").unwrap();
        let parent = pnl_wdg.downcast::<gtk::Box>().unwrap();
        let mut pns = HashMap::new();
        if let Some(rows) = self.data.templates.get("panels") {
            for row in rows.iter() {
                let parent = pns.get(&row[CT_PID]).unwrap_or(&parent);
                if row[CT_TYPE] == "hbx" {
                    let child = gtk::Box::new(gtk::Orientation::Horizontal, 0);
                    child.style_context().add_class(&row[CT_ROLE]);
                    Self::pack(&row[CT_PACK], parent, &child, false, false, 0);
                    pns.insert(row[CT_ID].to_string(), child);
                } else if row[CT_TYPE] == "vbx" {
                    let child = gtk::Box::new(gtk::Orientation::Vertical, 0);
                    child.style_context().add_class(&row[CT_ROLE]);
                    Self::pack(&row[CT_PACK], parent, &child, false, false, 0);
                    pns.insert(row[CT_ID].to_string(), child);
                } else {
                    let child = gtk::Label::new(Some(&row[CT_LABEL]));
                    child.style_context().add_class(&row[CT_ROLE]);
                    Self::pack(&row[CT_PACK], parent, &child, false, false, 0);
                }
            }
        }

        let mut map: IndexMap<String, IndexMap<String, String>> = IndexMap::new();
        let pre = [
            "profile",
            "temp",
            "system",
            "ac",
            "bat",
            "cpu",
            "fan",
            "nm",
            "wifi",
            "bluetooth",
            "scr",
            "kbd",
            "gnome",
        ];

        for row in bhm::get_supported() {
            if map.get(&row[CB_ID]).is_none() {
                let mut obj = IndexMap::new();
                obj.insert("id".to_string(), row[CB_ID].to_string());
                obj.insert("type".to_string(), row[CB_TYPE].to_string());
                map.insert(row[CB_ID].to_string(), obj);
            }
            if let Some(obj) = map.get_mut(&row[CB_ID]) {
                obj.insert(row[CB_KEY].to_string(), row[CB_VALUE].to_string());
            }
            self.signals
                .insert(row[CB_ACTION].to_string(), row[CB_ACTION].to_string());

            // TODO
            if row[0] == "bat0-health" || row[0] == "bat0-charge" || row[0] == "bat0-voltage" {
                self.tx
                    .send(PnlEvt::Listener(row[0].to_string(), row[1].to_string()))
                    .ok();
            }
        }
        // Bluetooth
        {
            let mut is_bluetooth_airplane_mode = false;
            match bus::get_bluetooth_airplane_mode() {
                Ok(reply) => {
                    let mut obj = IndexMap::new();
                    obj.insert("id".to_string(), "bluetooth".to_string());
                    obj.insert("type".to_string(), "bluetooth".to_string());
                    obj.insert("enabled".to_string(), (!reply as u8).to_string());
                    map.insert("bluetooth".to_string(), obj);
                    self.signals.insert(
                        "bluetooth-enabled".to_string(),
                        "bluetooth-enabled".to_string(),
                    );
                    is_bluetooth_airplane_mode = true;
                }
                Err(err) => eprintln!("{}", err),
            }
            println!("is_bluetooth_airplane_mode: {}", is_bluetooth_airplane_mode);
        }
        // Gnome
        {
            let id = "gnome";
            let mut obj = IndexMap::new();
            obj.insert("id".to_string(), id.to_string());
            obj.insert("type".to_string(), "gnome".to_string());
            obj.insert("theme".to_string(), gnome::get_theme());
            map.insert(id.to_string(), obj);
            self.signals.insert(format!("{}-theme", id), "".to_string());
        }
        // Temperature
        {
            self.find_temps();
            for (i, row) in self.temps.to_owned().iter().enumerate() {
                let label = if row[0] == *"K10TEMP" {
                    "CPU"
                } else if row[0] == *"AMDGPU" {
                    "iGPU"
                } else if row[0] == *"NVME" {
                    "NVME"
                } else if row[0] == *"IWLWIFI_1" {
                    "WiFi"
                } else {
                    &row[0]
                };
                self.obj_temp(&format!("temp{}", i), label, &mut map);
            }
        }
        // System
        {
            self.obj_sys("system0", "CPU", &mut map);
            if !self.agpu.is_empty() {
                self.obj_sys("system1", "iGPU", &mut map);
            }
            self.obj_sys("system2", "dGPU", &mut map);
        }

        {
            let id = format!("temp{}", self.temps.len());
            self.obj_temp(&id, "dGPU", &mut map);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////
        let mut cnt = 0;
        let mut tmp = IndexMap::new();
        let mut shm = IndexMap::new();
        for row in pre {
            match self.data.templates.get(row) {
                Some(_template) => {
                    let mut v = vec![];
                    for (obj_id, item) in map.iter() {
                        if item.get("type").unwrap() == row {
                            v.push(obj_id);
                        }
                    }
                    if !v.is_empty() {
                        tmp.insert(row.to_string(), v);
                    }
                }
                None => eprintln!("None {:?}", row),
            }
        }
        //println!("{:#?}", tmp);
        for (template_id, row) in tmp.iter() {
            for obj_id in row {
                let items = self.data.templates.get(template_id).unwrap().clone();
                let mut new_items = vec![];
                for item in items.iter() {
                    let mut new_item = vec![];
                    //println!("{:?}", &item);
                    for (i, value) in item.iter().enumerate() {
                        if !value.is_empty() && i == CT_OBJECT {
                            new_item.push(obj_id.to_string());
                        } else {
                            new_item.push(value.to_string());
                        }
                    }
                    new_items.push(new_item);
                }
                shm.insert(cnt, new_items);
                cnt += 1;
            }
        }
        //println!("{:#?}", shm);
        self.counter = 1;
        ////////////////////////////////////////////////////////////////////////////////////////////
        for (_i, rows) in shm.iter() {
            let mut parents = pns.clone();
            for row in rows {
                let prop = &row[CT_PROP];
                let pack = &row[CT_PACK];
                let role = &row[CT_ROLE];
                let elm_type = &row[CT_TYPE];
                let obj_id = &row[CT_OBJECT];
                let parent = parents.get(&row[CT_PID]).unwrap();
                let action = &format!("{}-{}", obj_id, prop);
                // itm, hbx
                if elm_type == "itm" || elm_type == "hbx" {
                    let child = gtk::Box::new(gtk::Orientation::Horizontal, 0);
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, &child, true, true, 0);
                    parents.insert(row[CT_ID].to_string(), child);
                }
                // vbx
                else if elm_type == "vbx" {
                    let child = gtk::Box::new(gtk::Orientation::Vertical, 0);
                    child.set_valign(gtk::Align::Center);
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, &child, true, true, 0);
                    parents.insert(row[CT_ID].to_string(), child);
                }
                // vbs
                else if elm_type == "vbs" {
                    let child = gtk::Box::new(gtk::Orientation::Vertical, 0);
                    child.set_valign(gtk::Align::Center);
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, &child, false, false, 0);
                    parents.insert(row[CT_ID].to_string(), child);
                }
                // btn
                else if elm_type == "btn" {
                    let wid = self.create_btn(action, &row[CT_LABEL]);
                    let child = self.btn.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, child, false, false, 0);
                }
                // cmb
                else if elm_type == "cmb" {
                    let obj = map.get(obj_id).unwrap();
                    let value = obj.get(prop).unwrap();

                    let obj_type = obj.get("type").unwrap();

                    let items = if obj_type == "bat" && prop == "limit" {
                        bhm::data_bat_limit()
                    } else if obj_type == "fan" && prop == "ttp" {
                        bhm::data_fan_ttp()
                    } else if obj_type == "profile" && prop == "set" {
                        bhm::data_profiles()
                    } else if obj_type == "gnome" && prop == "theme" {
                        gnome::get_themes()
                    } else {
                        vec![]
                    };
                    let wid = self.create_cmb(action, value, &items);
                    let child = self.cmb.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    //Self::set_cmb_data(child, &items, val);
                    Self::pack(pack, parent, child, false, false, 0);
                }
                // scl
                else if elm_type == "scl" {
                    let obj = map.get(obj_id).unwrap();
                    let value = obj.get(prop).unwrap();

                    let min = 0.;
                    let max = obj
                        .get("max_brightness")
                        .unwrap()
                        .parse::<f64>()
                        .unwrap_or_default();
                    let wid = self.create_scl(action, value, min, max);
                    let child = self.scl.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, child, true, true, 0);
                }
                // swt
                else if elm_type == "swt" {
                    let obj = map.get(obj_id).unwrap();
                    let value = obj.get(prop).unwrap();

                    let wid = self.create_swt(action, value);
                    let child = self.swt.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    // TODO
                    if action == "fan0-pwm_cpu" {
                        let mut map = HashMap::new();
                        for item in ASUS_FAN_PWM_CPU_DATA.iter() {
                            if item[2] == "true" {
                                map.insert(true, item[1].to_string());
                            } else if item[2] == "false" {
                                map.insert(false, item[1].to_string());
                            }
                        }
                        self.swt_act_data.insert(action.to_string(), map);
                    }
                    Self::pack(pack, parent, child, false, false, 0);
                }
                // prb
                else if elm_type == "prb" {
                    let obj = map.get(obj_id).unwrap();
                    let value = obj.get(prop).unwrap();
                    let value = &format!("{} ", value);

                    let wid = self.create_prb(action, value);
                    let child = self.prb.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, child, true, true, 0);
                }
                // prp, prc
                else if elm_type == "prp" || elm_type == "prc" {
                    let obj = map.get(obj_id).unwrap();
                    let value = obj.get(prop).unwrap();
                    let wid = if elm_type == "prc" {
                        self.create_prc(action, value)
                    } else {
                        self.create_prp(action, value)
                    };
                    let child = self.prb.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, child, false, false, 0);
                }
                // Other
                else if obj_id.is_empty() {
                    let child = &gtk::Label::new(Some(&row[CT_LABEL]));
                    //child.style_context().add_class("string");
                    if elm_type == "slb" {
                        child.style_context().add_class("slb");
                    }
                    child.style_context().add_class(role);
                    Self::pack(pack, parent, child, false, false, 0);
                } else {
                    let def = &String::new();

                    let obj = map.get(obj_id).unwrap();
                    let value = obj.get(prop).unwrap_or(def);

                    let typ = obj.get("type").unwrap();
                    let value = &if typ == "nm" && prop == "state" {
                        self.nm_states.get(value).unwrap().to_string()
                    } else if typ == "ac" && prop == "online" {
                        if value == "1" { "On" } else { "Off" }.to_string()
                    } else if typ == "fan" && prop == "rpm_cpu" {
                        format!("{} RPM", value)
                    } else {
                        value.to_string()
                    };

                    let wid = self.create_lbl(action, value);
                    let child = self.lbl.get(&wid).unwrap();
                    child.style_context().add_class(role);
                    if elm_type == "ind" {
                        child.style_context().add_class("ind");
                    }
                    if elm_type == "slb" {
                        child.style_context().add_class("slb");
                    }
                    Self::pack(pack, parent, child, false, false, 0);
                }
            }
        }

        // TODO
        self.tock();

        for (_id, hbx) in pns {
            hbx.show_all();
        }
    }

    pub(crate) fn tick(&mut self) {
        let now_ms = self.now.elapsed().as_millis();
        for (act, ms) in self.timeouts.to_owned().iter() {
            if &now_ms > ms {
                self.timeouts.remove(act);
                if act == "window_resize" {
                    self.out_window_resize();
                } else if act == "tock" {
                    self.tock();
                }
            }
        }
    }
    pub(crate) fn listener_btn(&mut self, id: &str) {
        // TODO
        match self.acts.get(id) {
            Some(act) => {
                if act == "fan0-curve" {
                    //let tx = self.tx.clone();
                    let act = act.to_string();
                    DialogFanCurveEditor::new(Some(&self.top), &bhm::get_fan_curve()).open(
                        move |dialog, curve| {
                            bhm::set_action(&act, curve);
                            //tx.send(PnlEvt::Listener(format!("profile.fan_curve_apply:{}", curve))).ok();
                            dialog.close();
                        },
                    );
                } else if act == "profile-prev" {
                    bhm::prev_profile();
                } else if act == "profile-next" {
                    bhm::next_profile();
                } else if act == "profile-refresh" {
                    bhm::set_action(act, "");
                } else if act == "profile-editor" {
                    if self.windows.borrow().get("profile_editor").is_some() {
                        self.windows
                            .borrow_mut()
                            .get_mut("profile_editor")
                            .unwrap()
                            .present();
                    } else {
                        let w = ProfileEditor::window(
                            self.win.get_widget().as_ref(),
                            //self.tx.clone(),
                            &bhm::get_profile(),
                            &self.windows,
                        );
                        self.windows.borrow_mut().insert("profile_editor".into(), w);
                    }
                } else {
                    eprintln!("listener_btn {}, TODO action: {}", id, act)
                }
            }
            None => eprintln!("listener_btn None: {}", id),
        }
    }
    pub(crate) fn listener_cmb(&mut self, id: &str) {
        match self.cmb.get(id) {
            Some(cmb) => {
                let value = match cmb.active_id() {
                    Some(value) => value.to_string(),
                    None => String::new(),
                };
                self.set_action(id, &value);
            }
            None => eprintln!("listener_cmb None: {}", id),
        }
    }
    pub(crate) fn listener_scl(&mut self, id: &str) {
        match self.scl.get(id) {
            Some(scl) => {
                let value = (scl.value() as u8).to_string();
                self.set_action(id, &value);
            }
            None => eprintln!("listener_scl None: {}", id),
        }
    }
    pub(crate) fn listener_swt(&mut self, id: &str) {
        match self.swt.get(id) {
            Some(swt) => {
                let val = swt.is_active();
                let act = match self.acts.get(id) {
                    Some(act) => act.to_string(),
                    None => String::new(),
                };
                let value = match self.swt_act_data.get(&act) {
                    Some(map) => match map.get(&val) {
                        Some(value) => value.to_string(),
                        None => "0".to_string(),
                    },
                    None => (val as u8).to_string(),
                };
                self.set_action(id, &value);
            }
            None => eprintln!("listener_swt None: {}", id),
        }
    }

    pub(crate) fn signal(&mut self, action: &str, value: &str) {
        for (id, act) in self.acts.iter() {
            if act == action {
                // Cmb
                if id[..3] == *"Cmb" {
                    let cmb = self.cmb.get(id).unwrap();
                    if cmb.active_id().unwrap_or_else(|| GString::from("")) != value {
                        cmb.set_sensitive(false);
                        cmb.set_active_id(Some(value));
                    }
                }
                // Prb
                else if id[..3] == *"Prb" {
                    let prb = self.prb.get(id).unwrap();
                    let v = value.parse::<f64>().unwrap_or_default();
                    let a = prb.text().unwrap();
                    let a: Vec<&str> = a.split('\n').collect();
                    if value.is_empty() {
                        prb.set_text(Some(&format!("{}\n{}", a[0], "     ")));
                    } else {
                        prb.set_text(Some(&format!("{}\n{:5.1}", a[0], v / 1000.)));
                    }
                    prb.set_fraction(v / 100000.);
                }
                // Prp
                else if id[..3] == *"Prp" {
                    let prb = self.prb.get(id).unwrap();
                    if let Ok(v) = value.parse::<f64>() {
                        prb.set_text(Some(&format!("{:5.1} %", v / 1.)));
                        prb.set_fraction(v / 100.);
                    } else {
                        prb.set_text(Some("       "));
                        prb.set_fraction(0.);
                    }
                }
                // Prc
                else if id[..3] == *"Prc" {
                    let prb = self.prb.get(id).unwrap();
                    let v = value.parse::<f64>().unwrap_or_default();

                    if action == "bat0-health" {
                        let value = value.parse::<f64>().unwrap_or_default();
                        //Panel::set_label_ind(lbl, "1", " %", value, "");
                        //let value = format!("{ten:.ws$}%", ten = value, ws = 0);
                        let v = value;
                        prb.set_text(Some(&format!("{:5.1} %", v / 1.)));
                        prb.set_fraction(v / 100.);
                    } else {
                        prb.set_text(Some(&format!("{:5.1} %", v / 1.)));
                        prb.set_fraction(v / 100.);
                    }
                }
                // Lbl
                else if id[..3] == *"Lbl" {
                    if let Some(lbl) = self.lbl.get(id) {
                        if action == "ac0-online" {
                            lbl.set_text(if value == "1" { "On" } else { "Off" });
                        } else if action == "system1-power"
                            || action == "system2-power"
                            || action == "bat0-power_now"
                        {
                            let value = value.parse::<f64>().unwrap_or_default();
                            let format =
                                format!("{ten:ns$.ws$} W", ten = value / 1000000., ns = 5, ws = 1);
                            lbl.set_text(&format);
                        } else if action == "bat0-voltage" {
                            let value = value.parse::<f64>().unwrap_or_default();
                            let format =
                                format!("{ten:ns$.ws$} V", ten = value / 1000000., ns = 5, ws = 1);
                            lbl.set_text(&format);
                        } else if action == "fan0-rpm_cpu" {
                            let format = format!("{} RPM", value);
                            lbl.set_text(&format);
                        } else if action == "nm-state" {
                            let format = self
                                .nm_states
                                .get(value)
                                .unwrap_or(&String::new())
                                .to_string();
                            lbl.set_text(&format);
                        } else {
                            lbl.set_text(value);
                        }
                    }
                }
                // Scl
                else if id[..3] == *"Scl" {
                    let scl = self.scl.get(id).unwrap();
                    let val = value.parse::<f64>().unwrap_or_default();
                    //if scl.value() != val {
                    if (scl.value() - val).abs() > f64::EPSILON {
                        scl.set_value(val);
                    }
                }
                // Swt
                else if id[..3] == *"Swt" {
                    let swt = self.swt.get_mut(id).unwrap();
                    // TODO
                    let val = value != "0";
                    if swt.is_active() != val {
                        swt.set_sensitive(false);
                        swt.set_active(val);
                    }
                }
            }
        }
    }
    pub(crate) fn is_signal(&self, signal: &str) -> bool {
        self.signals.get(signal).is_some()
    }

    pub(crate) fn window_resize(&mut self) {
        let size = self.window.size();
        if self.size.0 != size.0 || self.size.1 != size.1 {
            self.size = size;
            self.timeout("window_resize", 500);
        }
    }

    pub(crate) fn profile(&self, data: Vec<Vec<String>>) {
        // TODO
        let profile_id = data[0][1].to_string();
        for (id, act) in self.acts.iter() {
            if act == "profile-set" && id[..3] == *"Cmb" {
                let cmb = self.cmb.get(id).unwrap();
                Self::cmb_data(cmb, &bhm::data_profiles(), &profile_id);
            }
        }
        for row in data.iter() {
            self.tx
                .send(PnlEvt::Listener(row[0].to_string(), row[1].to_string()))
                .ok();
        }
        println!("{:#?}", &data);
    }

    fn set_action(&mut self, id: &str, value: &str) {
        match self.olds.get_mut(id) {
            Some(old) => {
                if old != value {
                    *old = value.to_string();
                    match self.acts.get(id) {
                        Some(action) => {
                            if action == "bluetooth-enabled" {
                                bus::set_bluetooth_airplane_mode(value != "1").ok();
                            } else if action == "gnome-theme" {
                                gnome::set_theme(value);
                            } else {
                                bhm::set_action(action, value);
                            }
                        }
                        None => eprintln!("set_action None: {} {}", id, value),
                    }
                }
            }
            None => eprintln!("set_action old None: {} {}", id, value),
        }
    }

    fn out_window_resize(&mut self) {
        self.win.resize();
    }

    fn timeout(&mut self, action: &str, milliseconds: u128) {
        let ms = self.now.elapsed().as_millis() + milliseconds;
        match self.timeouts.get_mut(action) {
            Some(timeout) => *timeout = ms,
            None => {
                self.timeouts.insert(action.to_string(), ms);
            }
        }
    }

    fn obj_sys(
        &mut self,
        id: &str,
        label: &str,
        map: &mut IndexMap<String, IndexMap<String, String>>,
    ) {
        let mut obj = IndexMap::new();
        obj.insert("id".to_string(), id.to_string());
        obj.insert("type".to_string(), "system".to_string());
        obj.insert("p_label".to_string(), label.to_string());
        obj.insert("processing".to_string(), "".to_string());
        obj.insert("m_label".to_string(), "MEM".to_string());
        obj.insert("memory".to_string(), "".to_string());
        obj.insert("power".to_string(), "".to_string());
        map.insert(id.to_string(), obj);
        self.signals
            .insert(format!("{}-processing", id), "".to_string());
        self.signals
            .insert(format!("{}-memory", id), "".to_string());
        self.signals.insert(format!("{}-power", id), "".to_string());
    }
    fn obj_temp(
        &mut self,
        id: &str,
        label: &str,
        map: &mut IndexMap<String, IndexMap<String, String>>,
    ) {
        let mut obj = IndexMap::new();
        obj.insert("id".to_string(), id.to_string());
        obj.insert("type".to_string(), "temp".to_string());
        obj.insert("temp".to_string(), label.to_string());
        map.insert(id.to_string(), obj);
        self.signals.insert(format!("{}-temp", id), "".to_string());
    }

    fn create_btn(&mut self, action: &str, value: &str) -> String {
        let wid = format!("Btn{}", self.counter);
        let btn = gtk::Button::with_label(value);
        btn.set_widget_name(&wid);
        self.connect_btn(&wid, &btn);
        self.btn.insert(wid.to_string(), btn);
        self.acts.insert(wid.to_string(), action.to_string());
        //self.olds.insert(wid.to_string(), val.to_string());
        self.counter += 1;
        wid
    }
    fn create_cmb(&mut self, action: &str, value: &str, items: &[Vec<String>]) -> String {
        let wid = format!("Cmb{}", self.counter);
        let cmb = gtk::ComboBoxText::new();
        let store = gtk::ListStore::new(&[Type::STRING, Type::STRING]);
        for item in items.iter() {
            store.insert_with_values(None, &[(0, &item[1]), (1, &item[0])]);
        }
        cmb.set_model(Some(&store));
        cmb.set_active_id(Some(value));
        cmb.set_widget_name(&wid);
        self.connect_cmb(&wid, &cmb);
        self.cmb.insert(wid.to_string(), cmb);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn create_lbl(&mut self, action: &str, value: &str) -> String {
        let wid = format!("Lbl{}", self.counter);
        let lbl = gtk::Label::new(Some(value));
        lbl.set_widget_name(&wid);
        self.lbl.insert(wid.to_string(), lbl);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn create_prb(&mut self, action: &str, value: &str) -> String {
        let wid = format!("Prb{}", self.counter);
        let prb = gtk::ProgressBar::new();
        prb.set_widget_name(&wid);
        prb.set_orientation(gtk::Orientation::Vertical);
        prb.set_inverted(true);
        prb.set_text(Some(&format!("{}\n  -  ", value)));
        let v = value.parse::<f64>().unwrap_or_default();
        prb.style_context().add_class("prb");
        prb.set_fraction(v);
        prb.set_show_text(true);
        prb.set_ellipsize(gtk::pango::EllipsizeMode::None);
        self.prb.insert(wid.to_string(), prb);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn create_prp(&mut self, action: &str, value: &str) -> String {
        let wid = format!("Prp{}", self.counter);
        let prb = gtk::ProgressBar::new();
        //prb.set_orientation(gtk::Orientation::Vertical);
        //prb.set_inverted(true);
        if let Ok(v) = value.parse::<f64>() {
            prb.set_text(Some(&format!("{:5.1} %", value)));
            prb.set_fraction(v);
        } else {
            prb.set_text(Some("   -   "));
            prb.set_fraction(0.);
        }
        prb.set_widget_name(&wid);
        prb.style_context().add_class("prp");
        prb.set_show_text(true);
        self.prb.insert(wid.to_string(), prb);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn create_prc(&mut self, action: &str, value: &str) -> String {
        let wid = format!("Prc{}", self.counter);
        let prb = gtk::ProgressBar::new();
        if let Ok(v) = value.parse::<f64>() {
            prb.set_text(Some(&format!("{:5.1} %", value)));
            prb.set_fraction(v);
        } else {
            prb.set_text(Some("   -   "));
            prb.set_fraction(0.);
        }
        prb.set_widget_name(&wid);
        prb.style_context().add_class("prc");
        prb.set_show_text(true);
        self.prb.insert(wid.to_string(), prb);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn create_scl(&mut self, action: &str, value: &str, min: f64, max: f64) -> String {
        let wid = format!("Scl{}", self.counter);
        let scl = gtk::Scale::with_range(gtk::Orientation::Horizontal, min, max, 1.);
        scl.set_value_pos(gtk::PositionType::Left);
        scl.set_value(value.parse::<f64>().unwrap());
        scl.set_widget_name(&wid);
        self.connect_scl(&wid, &scl);
        self.scl.insert(wid.to_string(), scl);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn create_swt(&mut self, action: &str, value: &str) -> String {
        let wid = format!("Swt{}", self.counter);
        let swt = gtk::Switch::new();
        // TODO
        swt.set_active(value != "0");
        swt.set_widget_name(&wid);
        self.connect_swt(&wid, &swt);
        self.swt.insert(wid.to_string(), swt);
        self.acts.insert(wid.to_string(), action.to_string());
        self.olds.insert(wid.to_string(), value.to_string());
        self.counter += 1;
        wid
    }
    fn connect_btn(&self, id: &str, btn: &gtk::Button) {
        let id = id.to_owned();
        let tx = self.tx.clone();
        btn.connect_clicked(move |_widget: &gtk::Button| {
            tx.send(PnlEvt::ListenerBtn(id.to_owned())).ok();
        });
    }
    fn connect_cmb(&self, id: &str, cmb: &gtk::ComboBoxText) {
        let id = id.to_owned();
        let tx = self.tx.clone();
        cmb.connect_changed(move |cmb: &gtk::ComboBoxText| {
            if cmb.get_sensitive() {
                tx.send(PnlEvt::ListenerCmb(id.to_owned())).ok();
            } else {
                cmb.set_sensitive(true);
            }
        });
    }
    fn connect_swt(&self, id: &str, swt: &gtk::Switch) {
        let id = id.to_owned();
        let tx = self.tx.clone();
        swt.connect_changed_active(move |swt: &gtk::Switch| {
            if swt.get_sensitive() {
                tx.send(PnlEvt::ListenerSwt(id.to_owned())).ok();
            } else {
                swt.set_sensitive(true);
            }
        });
    }
    fn connect_scl(&self, id: &str, scl: &gtk::Scale) {
        let id = id.to_owned();
        let tx = self.tx.clone();
        scl.connect_change_value(move |scl: &gtk::Scale, _scroll, _value| {
            if scl.is_focus() {
                tx.send(PnlEvt::ListenerScl(id.to_owned())).ok();
            }
            Inhibit(false)
        });
    }
    fn pack<P: IsA<gtk::Widget>>(
        pack: &str,
        parent: &gtk::Box,
        child: &P,
        expand: bool,
        fill: bool,
        padding: u32,
    ) {
        if pack == "end" {
            parent.pack_end(child, expand, fill, padding);
        } else {
            parent.pack_start(child, expand, fill, padding);
        }
    }
    fn cmb_data(combo: &gtk::ComboBoxText, data: &[Vec<String>], value: &str) {
        if let Some(model) = combo.model() {
            if let Ok(store) = model.downcast::<gtk::ListStore>() {
                combo.set_sensitive(false);
                while let Some(tree_iter) = store.iter_first() {
                    store.remove(&tree_iter);
                    if store.iter_is_valid(&tree_iter) {
                        store.iter_next(&tree_iter);
                    }
                }
                for item in data.iter() {
                    store.insert_with_values(None, &[(0, &item[1]), (1, &item[0])]);
                }
                combo.set_active_id(Some(value));
                combo.set_sensitive(true);
            }
        }
    }

    fn find_temps(&mut self) {
        // TODO
        let mut store = vec![];
        let mut scan_dir = scanner::ScanDir::new().unwrap();
        scan_dir.scan(&self.data.path_hw);

        use regex::Regex;
        let re = Regex::new(r"^([a-z]+)([0-9]+)$").unwrap();

        let mut data0: HashMap<String, String> = HashMap::new();
        for k0 in &scan_dir.data {
            let name = read_file_to_string(&format!("{}/name", k0.0));
            data0.insert(name.trim().to_string(), k0.0.to_string());
        }

        let mut sorted0: Vec<_> = data0.iter().collect();
        sorted0.sort_by_key(|a| a.0 /*.to_lowercase()*/);
        for (name0, path0) in sorted0.iter() {
            if *name0 == "amdgpu" {
                self.agpu = path0.to_string();
            }

            // TODO
            let label = name0.replace("ucsi_source_psy_", "").to_uppercase();

            store.push([
                label.to_string(),
                "".to_string(),
                name0.to_string(),
                "".to_string(),
                "".to_string(),
                "".to_string(),
                "".to_string(),
                "".to_string(),
                format!("{}/name", path0),
            ]);
            let data1 = scan_dir.data.get(&path0.to_string()).unwrap();
            //println!("{:?}", data1);
            let mut sorted1: Vec<_> = data1.iter().collect();
            sorted1.sort_by_key(|a| a.0);
            for (key1, val1) in sorted1.iter() {
                //if key1.to_string() == "name" && &val1.to_string() == "asus" {
                //    path_fan = path0.to_string();
                //}
                if *key1 != "name" {
                    let pats: Vec<&str> = key1.split('_').collect();
                    if pats[1] == "label"
                        || pats[1] == "min"
                        || pats[1] == "max"
                        || pats[1] == "alarm"
                        || pats[1] == "crit"
                    {
                        continue;
                    }
                    if pats[1] != "label" {
                        let caps = re.captures(pats[0]).unwrap();
                        let input = val1.trim();
                        let (value, unit) = Self::format_value(pats[1], &caps[1], input);

                        let key = &caps[1].to_string();
                        let num = &caps[2].to_string();
                        let p = format!("{}/{}{}_label", path0, key, num);
                        let label2 = fs::read_to_string(p)
                            .unwrap_or_else(|_| key.to_string())
                            .trim()
                            .to_owned();

                        store.push([
                            label.to_string(),
                            label2.to_string(),
                            pats[1].to_string(),
                            caps[2].to_string(),
                            caps[1].to_string(),
                            input.to_string(),
                            value.to_string(),
                            unit.to_string(),
                            format!("{}/{}", path0, &key1),
                        ]);
                    }
                }
            }
        }
        let mut temps = vec![];
        for row in store {
            if row[4] == "temp" {
                if row[0] == "K10TEMP" && row[1] == "Tdie" {
                    continue;
                }
                if row[0] == "ACPITZ" {
                    continue;
                }
                temps.push([row[0].to_string(), row[1].to_string(), row[8].to_string()]);
            }
        }

        self.temps = temps;
    }

    fn format_value(name: &str, key: &str, input: &str) -> (String, String) {
        let mut unit: String = String::new();
        let mut value = input.to_string();
        if input.is_empty() {
        } else if key == "in" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000.0);
                unit = String::from("V");
            }
        } else if key == "fan" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                //let num = input.parse::<f64>().unwrap();
                //value = (num / 1000.0).to_string();
                unit = String::from("RPM");
            }
        } else if key == "curr" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000.0);
                unit = String::from("A");
            }
        } else if key == "freq" {
            if name == "input" || name == "min" || name == "max" || name == "crit" {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000000.0);
                unit = String::from("MHz");
            }
        } else if key == "temp" {
            if name == "input"
                || name == "min"
                || name == "max"
                || name == "crit"
                || name == "alarm"
            {
                let num = input.parse::<f64>().unwrap();
                value = format!("{:.2}", num / 1000.0);
                unit = String::from("℃");
            }
        } else if key == "power" && name == "average" {
            let num = input.parse::<f64>().unwrap();
            value = format!("{:.2}", num / 1000000.0);
            unit = String::from("W");
        }

        (value, unit)
    }

    fn tock(&mut self) {
        let mut temps = HashMap::new();

        // CPU
        {
            let cpu = self.sys.cpu_percentage();
            let mem = self.sys.mem_percentage();

            let id = format!("system{}", 0);

            let action = format!("{}-processing", id);
            let format = cpu.to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();

            let action = format!("{}-memory", id);
            let format = mem.to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();
        }

        // TODO bat0-power_now
        {
            let action = "bat0-power_now".to_string();
            let value = bhm::get_action(&action);
            self.tx.send(PnlEvt::Listener(action, value)).ok();
        }

        // AMDGPU
        {
            let mut amdgpu = amdgpu::Amdgpu::new(self.agpu.to_string());
            amdgpu.gpu_metrics();

            let processing = amdgpu.gpu_percent().unwrap_or_default();
            let power_watt = amdgpu.gpu_power().unwrap_or_default();
            let memory = amdgpu.ram_percent();

            let id = format!("system{}", 1);
            let action = format!("{}-processing", id);
            let format = processing.to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();

            let action = format!("{}-memory", id);
            let format = memory.to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();

            let action = format!("{}-power", id);
            let format = (power_watt * 1000000.).to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();
        }

        // NVGPU
        let is_nvgpu = true;
        if is_nvgpu {
            let nvgpu = nvgpu::Nvgpu::new();
            let data = nvgpu.data();

            let id = format!("system{}", 2);

            let action = format!("{}-processing", id);
            let format = data[1].to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();

            let action = format!("{}-memory", id);
            let format = data[2].to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();

            let action = format!("{}-power", id);
            let format = (data[3] * 1000000.).to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();

            temps.insert("nvgpu".to_string(), data[0] as i32);
            let action = format!("temp{}-temp", self.temps.len());
            let format = (data[0] as i32 * 1000).to_string();
            self.tx.send(PnlEvt::Listener(action, format)).ok();
        } else {
            let id = format!("system{}", 2);

            let action = format!("{}-processing", id);
            self.tx.send(PnlEvt::Listener(action, "".to_string())).ok();

            let action = format!("{}-memory", id);
            self.tx.send(PnlEvt::Listener(action, "".to_string())).ok();

            let action = format!("{}-power", id);
            self.tx.send(PnlEvt::Listener(action, "".to_string())).ok();

            let action = format!("temp{}-temp", self.temps.len());
            self.tx.send(PnlEvt::Listener(action, "".to_string())).ok();
        }

        for (i, row) in self.temps.iter().enumerate() {
            let action = format!("temp{}-temp", i);
            let temp = read_file_to_string(&row[2])
                .parse::<i32>()
                .unwrap_or_default();
            temps.insert(action.to_string(), temp);
            self.tx
                .send(PnlEvt::Listener(action, temp.to_string()))
                .ok();
        }

        //println!("{:#?}", temps);
        //let key_with_max_value = temps.iter().max_by_key(|entry | entry.1).unwrap();
        //dbg!(key_with_max_value.0);

        self.tree.update();

        self.timeout("tock", 1000);
    }
}
