use std::process::Command;

pub fn cmd(command: &str) -> String {
    let mut result = "".to_string();
    let output = Command::new("sh")
        .arg("-c")
        .arg(command)
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
    if output.status.success() {
        result = String::from_utf8_lossy(&output.stdout).to_string();
    } else {
        let stderr = String::from_utf8_lossy(&output.stderr);
        print!("stderr: {}", stderr);
    }
    result
}

pub struct Sysfs {
}

impl Sysfs {
    //pub fn new() -> Self {}

    // https://www.kernel.org/doc/Documentation/cpu-freq/boost.txt
    // CPU boost: 0 - disabled, 1 - enabled
    pub fn get_cpu_boost() -> bool {
        let path = "/sys/devices/system/cpu/cpufreq/boost";
        let command = &format!("cat {}", path);
        let output = cmd(command);
        output == "1\n"
    }
    pub fn set_cpu_boost(mode: bool) {
        let path = "/sys/devices/system/cpu/cpufreq/boost";
        let value = if mode { 1 } else { 0 };
        let command = &format!("echo {} | tee {}", value, path);
        let command = &format!("/usr/bin/pkexec /bin/sh -c '{}'", command);
        cmd(command);
    }

    pub fn get_throttle_thermal_policy() -> String {
        //echo 2 | sudo tee /sys/devices/platform/asus-nb-wmi/throttle_thermal_policy
        let path = "/sys/devices/platform/asus-nb-wmi/throttle_thermal_policy";
        let command = &format!("cat {}", path);
        let output = cmd(command);
        let value = if &output == "0\n" { "0" }
        else if &output == "1\n" { "1" }
        else if &output == "2\n" { "2" }
        else { "" };
        value.to_string()
    }
    pub fn set_throttle_thermal_policy(mode: &str) {
        let path = "/sys/devices/platform/asus-nb-wmi/throttle_thermal_policy";
        let value = if mode == "0" { "0" }
        else if mode == "1" { "1" }
        else if mode == "2" { "2" }
        else { "" };
        if !mode.is_empty() {
            let command = &format!("echo {} | tee {}", value, path);
            let command = &format!("/usr/bin/pkexec /bin/sh -c '{}'", command);
            cmd(command);
        }
    }

    pub fn get_wireless_power_save(dev: &str) -> bool {
        let command = &format!("/sbin/iw dev {} get power_save", dev);
        let output = cmd(command);
        output == "Power save: on\n"
    }
    pub fn set_wireless_power_save(dev: &str, mode: bool) {
        let value = if mode { "on" } else { "off" };
        let command = &format!("/sbin/iw dev {} set power_save {}", dev, value);
        let command = &format!("/usr/bin/pkexec /bin/sh -c '{}'", command);
        cmd(command);
    }
}