#![allow(unused)]

use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use zbus::{dbus_proxy, Connection, Message, Result, SignalReceiver};
use zvariant::{derive::Type, Type};
use zvariant::{from_slice, to_bytes, EncodingContext};
use zvariant::{ObjectPath, OwnedObjectPath, Str};

#[derive(Deserialize, Serialize, Type, PartialEq, Debug)]
pub struct Profile {
    pub name: String,
    pub min_percentage: u8,
    pub max_percentage: u8,
    pub turbo: bool,
    pub fan_preset: u32,
    pub fan_curve: String,
}

impl Default for Profile {
    fn default() -> Self {
        Profile {
            name: "new".into(),
            min_percentage: 0,
            max_percentage: 100,
            turbo: false,
            fan_preset: 0,
            fan_curve: "".into(),
        }
    }
}

impl Profile {
    pub fn new(
        name: String,
        min_percentage: u8,
        max_percentage: u8,
        turbo: bool,
        fan_preset: u32,
        fan_curve: String,
    ) -> Self {
        Profile {
            name,
            min_percentage,
            max_percentage,
            turbo,
            fan_preset,
            fan_curve,
        }
    }
}

#[dbus_proxy(
    default_service = "bas.apps.bhm",
    default_path = "/bas/apps/bhm",
    interface = "bas.apps.bhm"
)]
pub trait Client0 {
    //fn start(&self) -> Result<()>;
    //fn stop(&self) -> Result<()>;

    //#[dbus_proxy(property)]
    //fn set_desktop_id(&mut self, id: &str) -> Result<()>;

    #[dbus_proxy(signal)]
    fn error(&self, message: String) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn signal(&self, message: &str) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn delta(&self, delta: Vec<Vec<String>>) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn profile(&self, data: Vec<Vec<String>>) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn action(&self, name: String, message: String) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn alert_count(&self, val: u64) -> zbus::Result<()>;

    #[dbus_proxy(signal)]
    fn change_fan_pwm(&self, mode: u8) -> zbus::Result<()>;
}

//"BluetoothAirplaneMode" ,"b", "bluetooth_airplane_mode"
#[dbus_proxy(
    default_service = "org.gnome.SettingsDaemon.Rfkill",
    default_path = "/org/gnome/SettingsDaemon/Rfkill",
    interface = "org.gnome.SettingsDaemon.Rfkill"
)]
pub trait OrgGnomeSettingsDaemonRfkill {
    //#[dbus_proxy(property)]
    //fn bluetooth_airplane_mode(&self) -> zbus::Result<()>;
    #[dbus_proxy(property)]
    fn bluetooth_airplane_mode(&self) -> zbus::Result<bool>;
}

#[dbus_proxy(
    default_service = "org.gnome.SettingsDaemon.Rfkill",
    default_path = "/org/gnome/SettingsDaemon/Rfkill",
    interface = "org.freedesktop.DBus.Properties"
)]
pub trait OrgGnomeSettingsDaemonRfkillP {
    //fn get(&self, interface_name: String, property_name: String) -> zbus::Result<()>;
    //fn get_all(&self, interface_name: String) -> zbus::Result<()>;
    #[dbus_proxy(signal)]
    fn properties_changed(
        &self,
        property: String,
        a: HashMap<String, zvariant::Value<'_>>,
        b: Vec<String>,
    ) -> zbus::Result<()>;
}

pub fn get_bluetooth_airplane_mode() -> zbus::Result<bool> {
    let connection = zbus::Connection::new_session().unwrap();
    let proxy = OrgGnomeSettingsDaemonRfkillProxy::new(&connection).unwrap();
    proxy.bluetooth_airplane_mode()
}

pub fn set_bluetooth_airplane_mode(mode: bool) -> zbus::Result<Message> {
    let connection = zbus::Connection::new_session().unwrap();
    //let m =
    connection.call_method(
        Some("org.gnome.SettingsDaemon.Rfkill"),
        "/org/gnome/SettingsDaemon/Rfkill",
        Some("org.freedesktop.DBus.Properties"),
        "Set",
        &(
            "org.gnome.SettingsDaemon.Rfkill",
            "BluetoothAirplaneMode",
            zvariant::Value::Bool(mode),
        ),
    )
}

pub fn start() {
    let conn = zbus::Connection::new_system().unwrap();
    let mut client = Client0Proxy::new_for_path(&conn, "/bas/apps/bhm").unwrap();
    client
        .connect_error(move |message| {
            println!("Signal Error: {}.", message);
            Ok(())
        })
        .unwrap();
    client
        .connect_alert_count(move |val| {
            println!("Signal AlertCount: {}.", val);
            Ok(())
        })
        .unwrap();
    client
        .connect_change_fan_pwm(move |mode| {
            println!("Signal ChangeFanPwmEnable: {}.", mode);
            Ok(())
        })
        .unwrap();
    let mut receiver = zbus::SignalReceiver::new(conn);
    receiver.receive_for(&client);

    loop {
        receiver.next_signal().unwrap();
    }
}
