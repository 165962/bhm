use crate::utl::read_file_to_string;
use std::collections::HashMap;
use std::error::Error;
use std::fs;

pub struct ScanDir {
    pub level: i8,
    pub data: HashMap<String, HashMap<String, String>>,
}

impl ScanDir {
    pub fn new() -> Option<ScanDir> {
        Some(ScanDir {
            level: 2,
            data: HashMap::new(),
        })
    }

    pub fn scan(&mut self, path: &str) {
        self._scan(path, 1);
    }

    fn _scan(&mut self, path: &str, level: i8) {
        if level <= self.level {
            let paths = fs::read_dir(path).unwrap();
            for dir_entry in paths {
                let path_buf = dir_entry.unwrap().path();
                let p = path_buf.to_string_lossy().to_string();
                if path_buf.is_dir() {
                    //println!("Dir {} {}", level, p);
                    self._scan(&p, level + 1);
                } else if path_buf.is_file() {
                    let file_name = path_buf.file_name().unwrap().to_string_lossy().to_string();
                    self._file(path, &p, file_name).unwrap();
                }
            }
        }
    }

    fn _file(&mut self, path: &str, p: &str, file_name: String) -> Result<(), Box<dyn Error>> {
        let content = read_file_to_string(p);
        if !content.is_empty() {
            let k = path.to_string();
            let list: &mut HashMap<String, String>;
            if !self.data.contains_key(&k) {
                self.data.insert(path.to_string(), HashMap::new());
            }
            list = self.data.get_mut(&k).unwrap();
            list.insert(file_name, content);
        }
        Ok(())
    }

    //pub fn path_cpu(&self) -> String {
    //    let mut path = String::new();
    //    //scan_dir.scan(HW_PATH);
    //    for k0 in &self.data {
    //        let mut sorted: Vec<_> = k0.1.iter().collect();
    //        sorted.sort_by_key(|a| a.0);
    //        for (key, val) in sorted.iter() {
    //            if *key == "name" && *val == "k10temp" {
    //                path = k0.0.to_string();
    //            }
    //        }
    //    }
    //    path
    //}
    //pub fn path_fan(&self) -> String {
    //    let mut path = String::new();
    //    //scan_dir.scan(HW_PATH);
    //    for k0 in &self.data {
    //        let mut sorted: Vec<_> = k0.1.iter().collect();
    //        sorted.sort_by_key(|a| a.0);
    //        for (key, val) in sorted.iter() {
    //            if *key == "name" && *val == "asus" {
    //                path = k0.0.to_string();
    //            }
    //        }
    //    }
    //    path
    //}
    //pub fn path_agpu(&self) -> String {
    //    let mut path = String::new();
    //    //scan_dir.scan(HW_PATH);
    //    for k0 in &self.data {
    //        let mut sorted: Vec<_> = k0.1.iter().collect();
    //        sorted.sort_by_key(|a| a.0);
    //        for (key, val) in sorted.iter() {
    //            if *key == "name" && *val == "amdgpu" {
    //                path = k0.0.to_string();
    //            }
    //        }
    //    }
    //    path
    //}
    //pub fn path_nvme(&self) -> String {
    //    let mut path = String::new();
    //    //scan_dir.scan(HW_PATH);
    //    for k0 in &self.data {
    //        let mut sorted: Vec<_> = k0.1.iter().collect();
    //        sorted.sort_by_key(|a| a.0);
    //        for (key, val) in sorted.iter() {
    //            if *key == "name" && *val == "nvme" {
    //                path = k0.0.to_string();
    //            }
    //        }
    //    }
    //    path
    //}
    //pub fn path_wifi(&self) -> String {
    //    let mut path = String::new();
    //    //scan_dir.scan(HW_PATH);
    //    for k0 in &self.data {
    //        let mut sorted: Vec<_> = k0.1.iter().collect();
    //        sorted.sort_by_key(|a| a.0);
    //        for (key, val) in sorted.iter() {
    //            if *key == "name" && *val == "iwlwifi_1" {
    //                path = k0.0.to_string();
    //            }
    //        }
    //    }
    //    path
    //}
}
