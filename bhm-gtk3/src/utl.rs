use std::fs;
use std::process::Command;

pub enum PnlEvt {
    Init(),
    Tick(),
    Profile(Vec<Vec<String>>),
    Listener(String, String),
    ListenerBtn(String),
    ListenerCmb(String),
    ListenerScl(String),
    ListenerSwt(String),
    WindowResize(),
}

//const PKEXEC: &str = "/usr/bin/pkexec";

pub(crate) fn exec(command: &str) -> String {
    let output = Command::new("sh")
        .arg("-c")
        .arg(command)
        .output()
        .unwrap_or_else(|e| panic!("failed to execute process: {}", e));
    if output.status.success() {
        String::from_utf8_lossy(&output.stdout).to_string()
    } else {
        let stderr = String::from_utf8_lossy(&output.stderr);
        eprintln!("stderr: {}", stderr);
        String::new()
    }
}

pub fn read_file_to_string(path: &str) -> String {
    match fs::read_to_string(path) {
        Ok(x) => x.trim().to_string(),
        Err(err) => {
            //Err(std::io::Error::new(std::io::ErrorKind::Other, "Could not parse float")),
            eprintln!("Error read file {}. {}.", path, err);
            String::from("")
        }
    }
}

//pub fn read_file_to_number(path: &str) -> f64 {
//    let content = read_file_to_string(path);
//    if content.is_empty() {
//        0.0
//    } else {
//        content.trim().parse::<f64>().unwrap()
//    }
//}
