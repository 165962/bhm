
###### AC
Событие:
- `ac0-online`

Чтение файла `AC[N]` `./online` через интервал времени.

_Переименовать в `ac_power`, `inotify` его не слышит, др. не пробовал._

###### BAT
События:
- `bat_system_percentage`
- `bat0-voltage`
- `bat0-power_watt`
- `bat0-system_remaining`

###### SYS
События:
- `system_cpu_percent`
- `system_mem_percent`

Извлечение данных, происходит чтением файлов `hwmon`.

###### AGPU
События:
- `agpu_gpu_percent`
- `agpu_mem_percent`
- `agpu_power_watt`

Чтение файлов `hwmon` через интервал времени.

###### NGPU
События:
- `ngpu_gpu_percent`
- `ngpu_mem_percent` 
- `ngpu_power_watt`

Извлечение данных осуществляется через запросы к `nvidia-smi`.

fan0-rpm_cpu //todo

temp.cpu
temp.agpu
temp.ngpu
temp.nvme
temp.wifi