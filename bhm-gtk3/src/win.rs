use crate::utl::PnlEvt;
use gtk::{glib, prelude::*, Application, ApplicationWindow};
use std::{fs::OpenOptions, io::Write};

#[derive(Clone)]
pub struct Win {
    pub size: Vec<i32>,
    is_init: bool,
    widget: ApplicationWindow,
    config_file: String,
}

impl Win {
    pub fn new(
        application: &Application,
        config_file: &str,
        tx_glib: glib::Sender<PnlEvt>,
    ) -> Self {
        let widget = gtk::ApplicationWindow::new(application);
        //widget.set_border_width(10);
        //widget.set_property("wid", "main");
        //widget.set_position(gtk::WindowPosition::Center);
        //widget.set_default_size(1200, 920);
        //widget.set_default_size(800, 700);
        //let w = gdk::FullscreenMode::CurrentMonitor;
        //let w = gdk::Screen::screen_width();
        //let h = gdk::Screen::screen_height();
        //println!("Screen {} {}", w, h);

        widget.connect_size_allocate(move |_window: &gtk::ApplicationWindow, _| {
            //tx.send(Evt::WindowResize()).ok();
            tx_glib.send(PnlEvt::WindowResize()).ok();
        });

        Self {
            is_init: false,
            size: vec![0, 0],
            widget,
            config_file: (*config_file).parse().unwrap(),
        }
    }

    pub fn add<P: IsA<gtk::Widget>>(&self, widget: &P) {
        self.widget.add(widget);
    }

    pub fn set_size(&mut self, w: i32, h: i32) {
        self.size[0] = w;
        self.size[1] = h;
    }

    pub fn set_role(&self, role: &str) {
        self.widget.set_role(role);
    }

    pub fn set_title(&self, role: &str) {
        self.widget.set_title(role);
    }

    pub fn set_default_size(&mut self, width: i32, height: i32) {
        self.widget.set_default_size(width, height);
        self.set_size(width, height);
    }

    pub fn show_all(&mut self) {
        self.widget.show_all();
        self.is_init = true;
    }

    pub fn resize(&mut self) {
        //if self.is_init {
        let size = self.widget.size();
        //if self.size[0] != size.0 || self.size[1] != size.1 {
        println!("Win::resize: {:?}", size);
        self.set_size(size.0, size.1);
        let data = format!("{},{}\n", self.size[0], self.size[1]);
        OpenOptions::new()
            .create(true)
            .write(true)
            .append(false)
            .truncate(true)
            .open(&self.config_file)
            .expect("Unable to open file")
            .write_all(data.as_bytes())
            .expect("Unable to write data");
        //}
        //}
    }

    pub fn get_widget(&self) -> &ApplicationWindow {
        &self.widget
    }
}
