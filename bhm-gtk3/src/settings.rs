use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use std::{fs, fs::OpenOptions, io::Write};

#[derive(Debug, Deserialize, Serialize)]
#[serde(transparent)]
struct SeqIndexMap {
    #[serde(with = "indexmap::serde_seq")]
    map: IndexMap<String, String>,
}

impl PartialEq for SeqIndexMap {
    fn eq(&self, other: &Self) -> bool {
        // explicitly compare items in order
        self.map.iter().eq(&other.map)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct Settings {
    pub(crate) path: String,
    pub(crate) data: Data,
}

impl Settings {
    pub(crate) fn open(path: &str) -> Self {
        match match fs::read_to_string(path) {
            Ok(deserialized) => match serde_yaml::from_str(&deserialized) {
                Ok(data) => Ok(data),
                Err(err) => {
                    eprintln!("Serialize invalid data format: {}", err);
                    Err(())
                }
            },
            Err(err) => {
                eprintln!("Unable to open data, path: {}, {}", path, err);
                Err(())
            }
        } {
            Ok(data) => Self {
                path: path.to_string(),
                data,
            },
            Err(_err) => Self {
                path: path.to_string(),
                data: Data::new(),
            },
        }
    }
    pub(crate) fn save(&self) {
        match serde_yaml::to_string(&self.data) {
            Ok(serialize) => match OpenOptions::new()
                .create(true)
                .write(true)
                .append(false)
                .truncate(true)
                .open(&self.path)
            {
                Ok(mut file) => match file.write_all(serialize.as_bytes()) {
                    Ok(_) => (),
                    Err(err) => eprintln!("Unable to save data, path: {}, {}.", &self.path, err),
                },
                Err(err) => eprintln!("Unable to open data, path: {}, {}", &self.path, err),
            },
            Err(err) => eprintln!("Serialize invalid data format: {}", err),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub(crate) struct Data {
    pub(crate) panel: IndexMap<String, bool>,
}

impl Data {
    fn new() -> Self {
        Self {
            panel: IndexMap::new(),
        }
    }
}
