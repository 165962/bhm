use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
use std::{env, fs, thread};

use gtk::glib;

use crate::utl::PnlEvt;
use crate::{bus, data::Data, pnl::Pnl, utl::read_file_to_string, win::Win, VERSION};

fn zv(value: &zvariant::Value) -> (String, String) {
    match value {
        // Simple types
        zvariant::Value::U8(_v) => (String::new(), String::new()),
        zvariant::Value::Bool(v) => (
            String::from("bool"),
            if *v {
                String::from("true")
            } else {
                String::from("false")
            },
        ),
        zvariant::Value::I16(_v) => (String::new(), String::new()),
        zvariant::Value::U16(_v) => (String::new(), String::new()),
        zvariant::Value::I32(_v) => (String::new(), String::new()),
        zvariant::Value::U32(_v) => (String::new(), String::new()),
        zvariant::Value::I64(_v) => (String::new(), String::new()),
        zvariant::Value::U64(_v) => (String::new(), String::new()),
        zvariant::Value::F64(_v) => (String::new(), String::new()),
        zvariant::Value::Str(_v) => (String::new(), String::new()),
        zvariant::Value::Signature(_v) => (String::new(), String::new()),
        zvariant::Value::ObjectPath(_v) => (String::new(), String::new()),
        zvariant::Value::Value(_v) => (String::new(), String::new()),
        zvariant::Value::Array(_v) => (String::new(), String::new()),
        zvariant::Value::Dict(_v) => (String::new(), String::new()),
        zvariant::Value::Structure(_v) => (String::new(), String::new()),
        zvariant::Value::Maybe(_v) => (String::new(), String::new()),
        zvariant::Value::Fd(_v) => (String::new(), String::new()),
    }
}

pub struct App {
    tx: glib::Sender<PnlEvt>,
}

impl App {
    pub fn new(application: &gtk::Application) -> Self {
        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let tx1 = tx.clone();
        let tx2 = tx.clone();

        // TODO
        let user_config_dir = format!("{}/bhm", glib::user_config_dir().to_string_lossy());
        match fs::create_dir(&user_config_dir) {
            Ok(_m) => (),
            Err(_e) => (),
        };

        // TODO
        let path = &format!("{}/size.txt", &user_config_dir);
        let mut content = read_file_to_string(path);
        if content.is_empty() {
            content = String::from("800,600");
        }
        let size: Vec<&str> = content.split(',').collect();
        let mut win = Win::new(
            application,
            &format!("{}/size.txt", &user_config_dir),
            tx.clone(),
        );
        win.set_role("main");
        let is_dev = env::var("DEVELOPER").unwrap_or_default() == "1";
        win.set_title(&if is_dev {
            format!("Development of BAS HWMon {} in GTK 3", VERSION)
        } else {
            format!("BAS HWMon {}", VERSION)
        });
        win.set_default_size(
            size[0].parse::<i32>().unwrap(),
            size[1].parse::<i32>().unwrap(),
        );

        ////////////////////////////////////////////////////////////////////////////////////////////
        let mut pnl = Pnl::new(
            win,
            Data::read(&format!("{}/panel.yml", &user_config_dir)),
            tx.clone(),
            Rc::new(RefCell::new(HashMap::new())),
        );

        rx.attach(None, move |snd| {
            match snd {
                PnlEvt::Init() => pnl.init(),
                PnlEvt::Tick() => pnl.tick(),
                PnlEvt::Profile(data) => pnl.profile(data),
                PnlEvt::Listener(action, value) => {
                    // TODO Signal bhm
                    if pnl.is_signal(&action) {
                        pnl.signal(&action, &value);
                    }
                }
                PnlEvt::ListenerBtn(id) => pnl.listener_btn(&id),
                PnlEvt::ListenerCmb(id) => pnl.listener_cmb(&id),
                PnlEvt::ListenerScl(id) => pnl.listener_scl(&id),
                PnlEvt::ListenerSwt(id) => pnl.listener_swt(&id),
                PnlEvt::WindowResize() => pnl.window_resize(),
            }
            glib::Continue(true)
        });

        //thread::spawn(move || {
        //    let sleep_time = std::time::Duration::from_millis(100);
        //    loop {
        //        tx_glib1.send(PnlEvt::Tick()).ok();
        //        thread::sleep(sleep_time);
        //    }
        //});

        let interval = std::time::Duration::from_millis(100);
        glib::timeout_add_local(interval, move || {
            tx1.send(PnlEvt::Tick()).ok();
            glib::Continue(true)
        });

        tx2.send(PnlEvt::Init()).ok();

        Self { tx }
    }

    pub fn start(&mut self) {
        self.proxies();
        gtk::main();
    }

    fn proxies(&mut self) {
        let conn0 = zbus::Connection::new_system().unwrap();
        let proxy0: Option<bus::Client0Proxy> = match bus::Client0Proxy::new(&conn0) {
            Ok(proxy) => {
                let tx = self.tx.clone();
                proxy
                    .connect_signal(move |message| {
                        let pats: Vec<&str> = message.split(':').collect();
                        tx.send(PnlEvt::Listener(pats[0].to_string(), pats[1].to_string()))
                            .ok();
                        Ok(())
                    })
                    .ok();
                let tx = self.tx.clone();
                proxy
                    .connect_delta(move |delta| {
                        for item in delta {
                            tx.send(PnlEvt::Listener(item[0].to_string(), item[1].to_string()))
                                .ok();
                        }
                        Ok(())
                    })
                    .ok();

                let tx = self.tx.clone();
                proxy
                    .connect_profile(move |data| {
                        tx.send(PnlEvt::Profile(data)).ok();
                        Ok(())
                    })
                    .ok();

                Some(proxy)
            }
            Err(err) => {
                eprintln!("{}", err);
                Option::None
            }
        };

        let conn1 = zbus::Connection::new_session().unwrap();
        let proxy_ogsdrp: Option<bus::OrgGnomeSettingsDaemonRfkillPProxy> =
            match bus::OrgGnomeSettingsDaemonRfkillPProxy::new(&conn1) {
                Ok(proxy) => {
                    let tx = self.tx.clone();
                    proxy
                        .connect_properties_changed(
                            move |_interface_name: String,
                                  properties: HashMap<String, zvariant::Value>,
                                  _c| {
                                for (property, value) in properties {
                                    if property == "BluetoothAirplaneMode" {
                                        let mode = zv(&value).1 == "true";
                                        tx.send(PnlEvt::Listener(
                                            "bluetooth-enabled".to_string(),
                                            (!mode as u8).to_string(),
                                        ))
                                        .expect("Couldn't send data to channel");
                                    }
                                }
                                Ok(())
                            },
                        )
                        .unwrap_or(());
                    Some(proxy)
                }
                Err(e) => {
                    eprintln!("{}", e);
                    Option::None
                }
            };
        thread::spawn(move || {
            let mut receiver = zbus::SignalReceiver::new(conn0);
            if proxy0.is_some() {
                receiver.receive_for(proxy0.as_ref().unwrap());
            }
            loop {
                receiver.next_signal().unwrap();
            }
        });
        thread::spawn(move || {
            let mut receiver = zbus::SignalReceiver::new(conn1);
            if proxy_ogsdrp.is_some() {
                receiver.receive_for(proxy_ogsdrp.as_ref().unwrap());
            }
            loop {
                receiver.next_signal().unwrap();
            }
        });
    }
}
