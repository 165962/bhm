// cat /sys/devices/pci0000:00/0000:00:08.1/0000:04:00.0/gpu_metrics
// watch -n 1 python3 ./amdgpu_metrics.py $(find /sys/devices -name gpu_metrics)
use std::convert::TryInto;
use std::fs::File;
use std::{fs, io, io::Read};

pub(crate) const METRICS_HEAD: [(&str, usize, usize); 3] = [
    ("structure_size      ", 0, 2),
    ("format_revision     ", 2, 1),
    ("content_revision    ", 3, 1),
];

pub(crate) const METRICS_V2_0: [(&str, usize, usize); 3] = [
    //("structure_size      ", 0, 2),
    //("format_revision     ", 2, 1),
    //("content_revision    ", 3, 1),
    //("65535               ", 4, 2),
    //("system_clock_counter", 6, 8),
    //("0?", 14, 2),
    //("TEMP_GPU            ", 16, 2),
    //("TEMP_CORE           ", 18, 2),
    //("TEMP_MEM            ", 20, 2),
    ("TEMP_EDGE           ", 22, 2),
    //("TEMP_HOTSPOT        ", 24, 2),
    //("TEMP_SOC            ", 26, 2),
    //("TEMP_VR_GFX         ", 28, 2),
    //("TEMP_VR_SOC         ", 30, 2),
    //("TEMP_VR_MEM0        ", 32, 2),
    //("TEMP_VR_MEM1        ", 34, 2),
    //("TEMP_LIQUID0        ", 36, 2),
    //("TEMP_LIQUID1        ", 38, 2),
    ("gpu_busy_percent", 40, 2),
    //("0?", 42, 2),
    ("average_socket_power", 44, 2),
    //("---", 46, 2),
    //("---", 48, 2),
    //("65535", 50, 2),
    //("core 0?", 52, 2),
    //("core 1?", 54, 2),
    //("core 2?", 56, 2),
    //("core 3?", 58, 2),
    //("core 4?", 60, 2),
    //("core 5?", 62, 2),
    //("core 6?", 64, 2),
    //("core 7?", 66, 2),
    //("average_sclk_frequency", 68, 2),
    //("average_socclk_frequency", 70, 2),
    //("65535", 72, 2),
    //("average_fclk_frequency", 74, 2),
    //("average frequency", 76, 2),
    //("65535", 78, 2),
    //("current_sclk", 80, 2),
    //("current_socclk", 82, 2),
    //("6?", 84, 2),
    //("current_fclk", 86, 2),
    //("400?", 88, 2),
    //("400?", 90, 2),
    //("cpu0 clk", 92, 2),
    //("cpu1 clk", 94, 2),
    //("cpu2 clk", 96, 2),
    //("cpu3 clk", 98, 2),
    //("cpu4 clk", 100, 2),
    //("cpu5 clk", 102, 2),
    //("cpu6 clk", 104, 2),
    //("cpu7 clk", 106, 2),
    //("cpu clk", 108, 2),
    //("cpu clk", 110, 2),
    //("0", 112, 2),
    //("throttle_status", 114, 4),
    //("fan_pwm", 116, 2),
    //("padding", 118, 2),
];

pub(crate) const METRICS_V2_2: [(&str, usize, usize); 3] = [
    //("structure_size      ", 0, 2),
    //("format_revision     ", 2, 1),
    //("content_revision    ", 3, 1),
    //("TEMP_GPU            ", 4, 2),
    //("TEMP_CORE           ", 6, 2),
    //("TEMP_MEM            ", 8, 2),
    ("TEMP_EDGE           ", 10, 2),
    //("TEMP_HOTSPOT        ", 12, 2),
    //("TEMP_SOC            ", 14, 2),
    //("TEMP_VR_GFX         ", 16, 2),
    //("TEMP_VR_SOC         ", 18, 2),
    //("TEMP_VR_MEM0        ", 20, 2),
    //("TEMP_VR_MEM1        ", 22, 2),
    //("TEMP_LIQUID0        ", 24, 2),
    //("TEMP_LIQUID1        ", 26, 2),
    ("gpu_busy_percent", 28, 2),
    //("0?                  ", 30, 2),
    //("system_clock_counter", 32, 8),
    ("average_socket_power", 40, 2),
    //("---", 42, 2),
    //("mem? frequency", 44, 2),
    //("65535", 46, 2),
    //("core 0                 ", 48, 2),
    //("core 1                 ", 50, 2),
    //("core 2                 ", 52, 2),
    //("core 3                 ", 54, 2),
    //("core 4                 ", 56, 2),
    //("core 5                 ", 58, 2),
    //("core 6                 ", 60, 2),
    //("core 7                 ", 62, 2),
    //("average_gfxclk_frequency", 64, 2),
    //("average_socclk_frequency", 66, 2),
    //("65535", 68, 2),
    //("gpu? mclk frequency", 70, 2),
    //("---", 72, 2),
    //("65535", 74, 2),
    //("current_sclk", 76, 2),
    //("current_socclk", 78, 2),
    //("6?", 80, 2),
    //("gpu? 1600", 82, 2),
    //("gpu? 400", 84, 2),
    //("gpu? 400", 86, 2),
    //("core? clk", 88, 2),
    //("core? clk", 90, 2),
    //("core? clk", 92, 2),
    //("core? clk", 94, 2),
    //("core? clk", 96, 2),
    //("core? clk", 98, 2),
    //("core? clk", 100, 2),
    //("core? clk", 102, 2),
    //("core? clk", 104, 2),
    //("core? clk", 106, 2),
    //("0", 108, 2),
    //("0", 110, 2),
    //("0", 112, 2),
    //("65535", 114, 2),
    //("65535", 116, 2),
    //("65535", 118, 2),
    //("throttle_status", 120, 4),
    //("fan_pwm", 124, 2),
    //("padding", 126, 2),
];

pub(crate) fn smu(data: &[u8], pos: usize, len: usize) -> u64 {
    let end = pos + len;
    if len == 1 {
        let bytes = data[pos..end].try_into().unwrap();
        u8::from_le_bytes(bytes) as u64
    } else if len == 2 {
        let bytes = data[pos..end].try_into().unwrap();
        u16::from_le_bytes(bytes) as u64
    } else if len == 4 {
        let bytes = data[pos..end].try_into().unwrap();
        u32::from_le_bytes(bytes) as u64
    } else if len == 8 {
        let bytes = data[pos..end].try_into().unwrap();
        u64::from_le_bytes(bytes) as u64
    } else {
        0
    }
}

fn read_file(path: &str) -> std::io::Result<String> {
    let mut s = String::new();
    fs::File::open(path)
        .and_then(|mut f| f.read_to_string(&mut s))
        .map(|_| s)
}

#[derive(Clone, Debug)]
pub struct Amdgpu {
    pub path: String,
    marks: Vec<(&'static str, usize, usize)>,
    gpu_metrics: Vec<u8>,
}

impl Amdgpu {
    pub fn new(path: String) -> Self {
        Self {
            path,
            marks: vec![],
            gpu_metrics: vec![],
        }
    }
    pub(crate) fn gpu_metrics(&mut self) -> Vec<u8> {
        //let path = "/sys/devices/pci0000:00/0000:00:08.1/0000:04:00.0/gpu_metrics";
        let path = format!("{}/device/gpu_metrics", self.path);
        let mut file = File::open(path).expect("Unable to open file");
        self.gpu_metrics = Vec::new();
        file.read_to_end(&mut self.gpu_metrics)
            .expect("Unable to read data");

        let head = METRICS_HEAD;
        //let size = amdgpu::smu(&data, head[0].1, head[0].2);
        let major = smu(&self.gpu_metrics, head[1].1, head[1].2);
        let minor = smu(&self.gpu_metrics, head[2].1, head[2].2);
        self.marks = if major == 2 && minor == 0 {
            METRICS_V2_0.to_vec()
        } else if major == 2 && minor == 2 {
            METRICS_V2_2.to_vec()
        } else {
            vec![]
        };

        self.gpu_metrics.clone()
    }
    pub(crate) fn get_gpu_metrics(&self, param: &str) -> f32 {
        for (name, pos, len) in self.marks.iter() {
            let value = smu(&self.gpu_metrics, *pos, *len);
            if param == *name && param == "gpu_busy_percent" {
                return value as f32 / 100.;
            } else if param == *name {
                return value as f32;
            }
            //println!("{} {}: {}", pos, name, value);
        }
        0.
    }
    //pub fn gpu_temp(&self) -> io::Result<f32> {
    //    read_file(&format!("{}/{}", self.path, "temp1_input"))
    //        .and_then(|data| match data.trim().parse::<f32>() {
    //            Ok(x) => Ok(x),
    //            Err(_) => Err(std::io::Error::new(
    //                std::io::ErrorKind::Other,
    //                "Could not parse float",
    //            )),
    //        })
    //        .map(|num| num / 1000.0)
    //}
    pub fn gpu_power(&self) -> io::Result<f32> {
        let power_watt = self.get_gpu_metrics("average_socket_power");
        if power_watt > 0. {
            Ok(power_watt)
        } else {
            let v = sys_info::os_release().unwrap();
            let v: Vec<&str> = v.split('-').collect();

            use semver::{Version, VersionReq};
            let req = VersionReq::parse(">=5.15.10").unwrap();
            let version = Version::parse(v[0]).unwrap();
            let divisor = if req.matches(&version) {
                1000.0
            } else {
                1000000.0
            };

            read_file(&format!("{}/{}", self.path, "power1_average"))
                .and_then(|data| match data.trim().parse::<f32>() {
                    Ok(x) => Ok(x),
                    Err(_) => Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "Could not parse float",
                    )),
                })
                .map(|num| num / divisor)
        }
    }
    pub fn gpu_percent(&self) -> io::Result<f32> {
        let processing = self.get_gpu_metrics("gpu_busy_percent");
        if processing > 0. {
            Ok(processing)
        } else {
            read_file(&format!("{}/{}", self.path, "device/gpu_busy_percent"))
                .and_then(|data| match data.trim().parse::<f32>() {
                    Ok(x) => Ok(x),
                    Err(_) => Err(std::io::Error::new(
                        std::io::ErrorKind::Other,
                        "Could not parse float",
                    )),
                })
                .map(|num| num / 1.)
        }
    }
    pub fn ram_used(&self) -> io::Result<f32> {
        read_file(&format!("{}/{}", self.path, "device/mem_info_vram_used"))
            .and_then(|data| match data.trim().parse::<f32>() {
                Ok(x) => Ok(x),
                Err(_) => Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Could not parse float",
                )),
            })
            .map(|num| num / 1.)
    }
    pub fn ram_total(&self) -> io::Result<f32> {
        read_file(&format!("{}/{}", self.path, "device/mem_info_vram_total"))
            .and_then(|data| match data.trim().parse::<f32>() {
                Ok(x) => Ok(x),
                Err(_) => Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Could not parse float",
                )),
            })
            .map(|num| num / 1.)
    }

    pub(crate) fn ram_percent(&self) -> f32 {
        self.ram_used().unwrap_or_default() / self.ram_total().unwrap_or_default() * 100.0
    }
}
