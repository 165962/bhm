use gtk::{glib, prelude::*, Widget};

// pub struct Box {
//     pub widget: gtk::Box,
// }
//
// impl Box {
//     pub fn new() -> Self {
//         Self {
//             widget: gtk::Box::new(gtk::Orientation::Horizontal, 0),
//         }
//     }
//     pub fn pack<P: IsA<gtk::Widget>>(
//         &self,
//         pack: &str,
//         child: &P,
//         expand: bool,
//         fill: bool,
//         padding: u32,
//     ) {
//         if pack == "end" {
//             self.widget.pack_end(child, expand, fill, padding);
//         } else {
//             self.widget.pack_start(child, expand, fill, padding);
//         }
//     }
//     pub fn set_widget_name(&self, name: &str) {
//         self.widget.set_widget_name(name);
//     }
// }

//pub struct Vbx {
//    pub widget: gtk::Box,
//}

//impl Vbx {
//    pub fn new() -> Self {
//        Self {
//            widget: gtk::Box::new(gtk::Orientation::Vertical, 0),
//        }
//    }
//    pub fn pack<P: IsA<gtk::Widget>>(
//        &self,
//        pack: &str,
//        child: &P,
//        expand: bool,
//        fill: bool,
//        padding: u32,
//    ) {
//        if pack == "end" {
//            self.widget.pack_end(child, expand, fill, padding);
//        } else {
//            self.widget.pack_start(child, expand, fill, padding);
//        }
//    }
//    pub fn set_widget_name(&self, name: &str) {
//        self.widget.set_widget_name(name);
//    }
//}

#[derive(Debug, Clone)]
pub struct Cmb {
    store: gtk::ListStore,
    pub widget: gtk::ComboBoxText,
}

impl Cmb {
    pub fn new(data: &[Vec<String>]) -> Self {
        let cmb = Self {
            store: gtk::ListStore::new(&[glib::Type::STRING, glib::Type::STRING]),
            widget: gtk::ComboBoxText::new(),
        };
        cmb.widget.set_model(Some(&cmb.store));
        for item in data.iter() {
            cmb.store
                .insert_with_values(None, &[(0, &item[1]), (1, &item[0])]);
        }

        // model_store.foreach(move |tree_model, _, tree_iter| {
        //     tree_model.remove(tree_iter);
        //     println!("{:?}", tree_iter);
        //     false
        // });

        //cmb.build_ui();
        cmb
    }
    pub fn connect_changed<F: Fn(&gtk::ComboBoxText) + 'static>(
        &self,
        f: F,
    ) -> glib::SignalHandlerId {
        self.widget.connect_changed(f)
    }
    // pub fn set<F: Fn() + 'static>(&mut self, f: F) {
    //     if self.is_run {
    //         self.is_run = false;
    //         f();
    //     } else {
    //         self.is_run = true;
    //     }
    // }
    // pub fn set_data(&mut self, data: &[Vec<String>]) {
    //     while let Some(tree_iter) = self.store.iter_first() {
    //         self.widget.set_sensitive(false);
    //         self.store.remove(&tree_iter);
    //         if self.store.iter_is_valid(&tree_iter) {
    //             self.store.iter_next(&tree_iter);
    //         }
    //     }
    //     for item in data.iter() {
    //         self.store
    //             .insert_with_values(None, &[(0, &item[1]), (1, &item[0])]);
    //     }
    // }
    // pub fn set_state(&mut self, value: &str) {
    //     if self.get_value() != value {
    //         self.widget.set_sensitive(false);
    //         self.set_value(value);
    //     }
    // }
    // pub fn set_state_id(&mut self, value: &str) {
    //     if self.get_value() != value {
    //         self.widget.set_sensitive(false);
    //         self.set_value(value);
    //     }
    // }
    pub fn set_value(&self, id: &str) {
        self.widget.set_active_id(Some(id));
    }
    // pub fn get_value(&self) -> String {
    //     match self.widget.active_id() {
    //         Some(v) => v.parse::<String>().unwrap(),
    //         None => String::new(),
    //     }
    //     //return self.widget.active_id().unwrap();
    // }
    pub fn set_widget_name(&self, name: &str) {
        self.widget.set_widget_name(name);
    }
}

//pub struct Swt {
//    pub widget: gtk::Switch,
//}

//impl Swt {
//    pub fn new() -> Self {
//        Self {
//            widget: gtk::Switch::new(),
//        }
//    }
//    pub fn connect_changed<F: Fn(&gtk::Switch) + 'static>(&self, f: F) -> glib::SignalHandlerId {
//        self.widget.connect_changed_active(f)
//    }
//    // pub fn set<F: Fn() + 'static>(&mut self, f: F) {
//    //     if self.is_run {
//    //         self.is_run = false;
//    //         f();
//    //     } else {
//    //         self.is_run = true;
//    //     }
//    // }
//    pub fn set_state(&mut self, value: bool) {
//        if self.get_value() != value {
//            self.widget.set_sensitive(false);
//            self.set_value(value);
//        }
//    }
//    pub fn set_value(&mut self, value: bool) {
//        self.widget.set_state(value);
//    }
//    pub fn get_value(&self) -> bool {
//        self.widget.is_active()
//    }
//    pub fn set_widget_name(&self, name: &str) {
//        self.widget.set_widget_name(name);
//    }
//}

//pub struct Lbl {
//    pub widget: gtk::Label,
//}

//impl Lbl {
//    pub fn new(text: &str) -> Self {
//        Self {
//            widget: gtk::Label::new(Some(text)),
//        }
//    }
//    pub fn set_value(&mut self, value: &str) {
//        self.widget.set_label(value);
//    }
//    pub fn set_widget_name(&self, name: &str) {
//        self.widget.set_widget_name(name);
//    }
//}

// pub struct Scl {
//     pub old: f64,
//     pub widget: gtk::Scale,
// }
//
// impl Scl {
//     pub fn new(value: f64, min: f64, max: f64) -> Self {
//         let adjustment = gtk::Adjustment::new(value, min, max, 1.0, 0.0, 0.0);
//         let scl = gtk::Scale::new(gtk::Orientation::Horizontal, Option::Some(&adjustment));
//         scl.set_digits(0);
//         if max > 10. {
//             //scl.add_mark(min, gtk::PositionType::Bottom, Option::Some(&min.to_string()));
//             scl.add_mark(min, gtk::PositionType::Bottom, None);
//             scl.add_mark(max, gtk::PositionType::Bottom, None);
//         } else {
//             for i in (min as i32..max as i32 + 1).map(|x| x as f64 * 1.) {
//                 //scl.add_mark(i, gtk::PositionType::Bottom, Option::Some(&i.to_string()));
//                 scl.add_mark(i, gtk::PositionType::Bottom, None);
//             }
//         }
//         Self {
//             old: value,
//             widget: scl,
//         }
//     }
//     pub fn set_old(&mut self, value: f64) {
//         self.old = value;
//     }
//     pub fn set_widget_name(&self, name: &str) {
//         self.widget.set_widget_name(name);
//     }
// }

// pub fn connect_spn(id: String, tx: Sender<Evt>, spn: &gtk::SpinButton) {
//     spn.connect_changed(move |widget: &gtk::SpinButton| {
//         if widget.get_sensitive() {
//             tx.send(Evt::ListenerSpn(id.to_owned())).ok();
//         } else {
//             widget.set_sensitive(true);
//         }
//     });
// }

// pub fn connect_cmb(id: String, tx: Sender<Evt>, cmb: &Cmb) {
//     cmb.connect_changed(move |widget: &gtk::ComboBoxText| {
//         if widget.get_sensitive() {
//             tx.send(Evt::ListenerCmb(id.to_owned())).ok();
//         } else {
//             widget.set_sensitive(true);
//         }
//     });
// }

// pub fn connect_scl(id: String, tx: Sender<Evt>, scl: &Scl) {
//     scl.widget
//         .connect_change_value(move |_widget: &gtk::Scale, _scroll, _value| {
//             tx.send(Evt::ListenerScl(id.to_owned())).ok();
//             Inhibit(false)
//         });
// }

//pub fn connect_scl(id: String, tx: Sender<Evt>, scl: &Scl) {
//    scl.widget
//        .connect_format_value(move |_widget: &gtk::Scale, value| {
//            //let digits = scale.digits() as usize;
//            tx.send(Evt::ListenerScl(id.to_owned())).ok();
//            format!("<{}>", value)
//        });
//}

// pub fn connect_swt(id: String, tx: Sender<Evt>, swt: &Swt) {
//     swt.widget
//         .connect_changed_active(move |widget: &gtk::Switch| {
//             if widget.get_sensitive() {
//                 tx.send(Evt::ListenerSwt(id.to_owned())).ok();
//             } else {
//                 widget.set_sensitive(true);
//             }
//         });
// }

// pub fn connect_btn(id: String, tx: Sender<Evt>, btn: &gtk::Button) {
//     btn.connect_clicked(move |_widget: &gtk::Button| {
//         tx.send(Evt::ListenerBtn(id.to_owned())).ok();
//     });
// }

pub(crate) fn to_top(widget: &gtk::Widget) -> Widget {
    match widget.parent() {
        Some(parent) => to_top(&parent),
        None => widget.to_owned(),
    }
}

pub(crate) fn find_set(
    widget: &gtk::Widget,
    name: &str,
    set: &str,
    data: Option<&Vec<Vec<String>>>,
) -> String {
    let top = to_top(widget);
    _find_set(top, name, set, data)
}
fn _find_set(
    widget: gtk::Widget,
    name: &str,
    set: &str,
    data: Option<&Vec<Vec<String>>>,
) -> String {
    let widget_type = &widget.type_().to_string();
    if widget_type == "GtkBox" {
        match widget.downcast::<gtk::Box>() {
            Ok(gbox) => {
                for widget in gbox.children() {
                    _find_set(widget, name, set, data);
                }
            }
            Err(err) => eprintln!("{}", err),
        }
    } else if widget_type == "GtkWindow" {
        match widget.downcast::<gtk::Window>() {
            Ok(dialog) => {
                for widget in dialog.children() {
                    _find_set(widget, name, set, data);
                }
            }
            Err(err) => eprintln!("{}", err),
        }
    } else if widget_type == "GtkDialog" {
        match widget.downcast::<gtk::Dialog>() {
            Ok(dialog) => {
                for widget in dialog.children() {
                    _find_set(widget, name, set, data);
                }
            }
            Err(err) => eprintln!("{}", err),
        }
    } else {
        let widget_name = &widget.widget_name().to_string();
        if widget_name == name {
            // GtkComboBoxText
            if widget_type == "GtkComboBoxText" {
                match widget.downcast::<gtk::ComboBoxText>() {
                    Ok(combo) => {
                        if data.is_some() {
                            //let store = gtk::ListStore::new(&[glib::Type::STRING, glib::Type::STRING]);
                            let store = combo.model().unwrap();
                            let store = store.downcast::<gtk::ListStore>().unwrap();
                            combo.set_sensitive(false);
                            while let Some(tree_iter) = store.iter_first() {
                                store.remove(&tree_iter);
                                if store.iter_is_valid(&tree_iter) {
                                    store.iter_next(&tree_iter);
                                }
                            }
                            if let Some(data) = data {
                                for item in data.iter() {
                                    store.insert_with_values(None, &[(0, &item[1]), (1, &item[0])]);
                                }
                            }
                            combo.set_sensitive(true);
                            //cmb.set_model(Some(&store));
                        }
                        combo.set_active_id(Some(&set.to_string()));
                    }
                    Err(err) => eprintln!("{}", err),
                }
            }
            // GtkSwitch
            else if widget_type == "GtkSwitch" {
                match widget.downcast::<gtk::Switch>() {
                    Ok(switch) => switch.set_state(set != "0"),
                    Err(err) => eprintln!("{}", err),
                }
            }
            // GtkEntry
            else if widget_type == "GtkEntry" {
                match widget.downcast::<gtk::Entry>() {
                    Ok(entry) => entry.set_text(set),
                    Err(err) => eprintln!("{}", err),
                }
            }
            // GtkLabel
            else if widget_type == "GtkLabel" {
                match widget.downcast::<gtk::Label>() {
                    Ok(label) => label.set_text(set),
                    Err(err) => eprintln!("{}", err),
                }
            }
        }
    }
    String::from("0")
}

pub(crate) fn find_get(widget: &gtk::Widget, name: &str) -> Option<gtk::Widget> {
    let top = to_top(widget);
    _find_get(top, name)
}
fn _find_get(widget: gtk::Widget, name: &str) -> Option<gtk::Widget> {
    let wn = &widget.widget_name().to_string();
    if wn == name {
        return Some(widget);
    }

    let wt = &widget.type_().to_string();
    //println!("{}", wt);

    let children = if wt == "GtkBox" {
        match widget.downcast::<gtk::Box>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::Box> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkGrid" {
        match widget.downcast::<gtk::Grid>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::Grid> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkWindow" {
        match widget.downcast::<gtk::Window>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::Window> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkPaned" {
        match widget.downcast::<gtk::Paned>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::Paned> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkScrolledWindow" {
        match widget.downcast::<gtk::ScrolledWindow>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::ScrolledWindow> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkViewport" {
        match widget.downcast::<gtk::Viewport>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::Viewport> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkDialog" {
        match widget.downcast::<gtk::Dialog>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::Dialog> {:?}", err);
                vec![]
            }
        }
    } else if wt == "GtkApplicationWindow" {
        match widget.downcast::<gtk::ApplicationWindow>() {
            Ok(w) => w.children(),
            Err(err) => {
                eprintln!("Error downcast::<gtk::ApplicationWindow> {:?}", err);
                vec![]
            }
        }
    } else {
        vec![]
    };

    for w in children {
        let r = _find_get(w, name);
        if r.is_some() {
            return r;
        }
    }

    None
}
