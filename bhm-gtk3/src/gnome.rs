use crate::utl;

pub(crate) fn get_themes() -> Vec<Vec<String>> {
    let mut themes = vec![];
    let themes_path = "/usr/share/themes";
    if let Ok(dir) = std::fs::read_dir(themes_path) {
        for entry in dir.flatten() {
            let path = entry.path();
            if path.is_dir() {
                if let Some(name) = path.file_name() {
                    let name = name.to_string_lossy().to_string();
                    themes.push(vec![format!("'{}'", name), name]);
                }
            }
        }
    }
    themes.sort_by(|a, b| a[1].cmp(&b[1]));
    themes
}

pub(crate) fn get_theme() -> String {
    let command = "gsettings get org.gnome.desktop.interface gtk-theme";
    utl::exec(command).trim().to_string()
}

pub(crate) fn set_theme(theme: &str) {
    let command = "gsettings set org.gnome.desktop.interface gtk-theme";
    utl::exec(&format!("{} {}", command, theme));
}
