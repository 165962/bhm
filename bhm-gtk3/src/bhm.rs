const T_ERR: u8 = 0;
const T_STR: u8 = 1;
const T_U8: u8 = 2;
const T_BOOL: u8 = 3;

fn call_bhm_dbus_method_old<B>(method_name: &str, body: &B) -> Option<zbus::Message>
where
    B: serde::ser::Serialize + zvariant::Type,
{
    let destination = Some("bas.apps.bhm");
    let path = "/bas/apps/bhm";
    let iface = Some("bas.apps.bhm");
    let connection = zbus::Connection::new_system().unwrap();
    match connection.call_method(destination, path, iface, method_name, body) {
        Ok(result) => Some(result),
        Err(err) => {
            eprintln!("Error {} {} {}", path, method_name, err);
            Option::None
        }
    }
}

fn call_bhm_dbus_method_type<B>(method_name: &str, body: &B) -> (u8, String, u8)
where
    B: serde::ser::Serialize + zvariant::Type,
{
    let destination = Some("bas.apps.bhm");
    let path = "/bas/apps/bhm";
    let iface = Some("bas.apps.bhm");
    let connection = zbus::Connection::new_system().unwrap();
    match connection.call_method(destination, path, iface, method_name, body) {
        Ok(message) => {
            if method_name == "SetAction" {
                (T_U8, String::new(), 0)
            }
            // u8
            else if method_name == "GetCpuFreqBoost"
                || method_name == "GetFanPwmEnable"
                || method_name == "GetFanThrottleThermalPolicy"
                || method_name == "GetBatChargeControlEndThreshold"
                || method_name == "GetKbdBrightness"
                || method_name == "GetKbdMaxBrightness"
                || method_name == "GetScrMaxBrightness"
                || method_name == "GetWirelessPowerSave"
                || method_name == "SetCpuFreqBoost"
                || method_name == "SetFanPwmEnable"
                || method_name == "SetFanThrottleThermalPolicy"
                || method_name == "SetBatChargeControlEndThreshold"
                || method_name == "SetKbdBrightness"
                || method_name == "SetWirelessPowerSave"
            {
                match message.body::<u8>() {
                    Ok(body) => (T_U8, String::new(), body),
                    Err(error) => {
                        let err = format!("Error {}: {}.", method_name, error);
                        eprintln!("{}", &err);
                        (T_ERR, err, 0)
                    }
                }
            }
            // str
            else if method_name == "GetVersion"
                || method_name == "GetAction"
                || method_name == "GetFanCurve"
                || method_name == "SetFanCurve"
                || method_name == "GetProfile"
                || method_name == "SetProfile"
                || method_name == "PrevProfile"
                || method_name == "NextProfile"
                || method_name == "RemoveProfile"
            {
                match message.body::<String>() {
                    Ok(body) => (T_STR, body, 0),
                    Err(error) => {
                        let err = format!("Error {} body: {}.", method_name, error);
                        eprintln!("{}", error);
                        (T_ERR, err, 0)
                    }
                }
            }
            // bool
            else if method_name == "GetWirelessEnabled" || method_name == "SetWirelessEnabled" {
                match message.body::<bool>() {
                    Ok(body) => (T_BOOL, String::new(), if body { 1 } else { 0 }),
                    Err(error) => {
                        let err = format!("Error {} body: {}.", method_name, error);
                        eprintln!("{}", error);
                        (T_ERR, err, 0)
                    }
                }
            } else {
                (T_ERR, String::from("Error key not found."), 0)
            }
        }
        Err(err) => {
            let err = format!("Error {}: {}.", method_name, err);
            eprintln!("{}", &err);
            (T_ERR, err, 0)
        }
    }
}

pub(crate) fn get_supported() -> Vec<Vec<String>> {
    let data: Vec<Vec<String>> = Vec::new();
    let method_name = "GetSupported";
    match call_bhm_dbus_method_old(method_name, &()) {
        Some(message) => match message.body::<Vec<Vec<String>>>() {
            Ok(body) => body,
            Err(error) => {
                eprintln!("Error {}: {}.", method_name, error);
                data
            }
        },
        None => data,
    }
}

//pub(crate) fn get_cpu_boost() -> u8 {
//    call_bhm_dbus_method_type("GetCpuFreqBoost", &()).2
//}

// pub(crate) fn get_fan_pwm() -> u8 {
//     call_bhm_dbus_method_type("GetFanPwmEnable", &()).2
// }

// pub(crate) fn get_fan_ttp() -> u8 {
//     call_bhm_dbus_method_type("GetFanThrottleThermalPolicy", &()).2
// }

pub(crate) fn get_fan_curve() -> String {
    let r = call_bhm_dbus_method_type("GetFanCurve", &());
    if r.0 == T_STR {
        r.1
    } else {
        String::new()
    }
}

pub(crate) fn get_action(action: &str) -> String {
    let r = call_bhm_dbus_method_type("GetAction", &(action));
    if r.0 == T_STR {
        r.1
    } else {
        String::new()
    }
}

// pub(crate) fn get_bat_limit() -> u8 {
//     call_bhm_dbus_method_type("GetBatChargeControlEndThreshold", &()).2
// }

// pub(crate) fn get_kbd_brightness() -> u8 {
//     call_bhm_dbus_method_type("GetKbdBrightness", &()).2
// }

// pub(crate) fn get_kbd_max_brightness() -> u8 {
//     call_bhm_dbus_method_type("GetKbdMaxBrightness", &()).2
// }

// pub(crate) fn get_scr_max_brightness() -> u8 {
//     call_bhm_dbus_method_type("GetScrMaxBrightness", &()).2
// }

// pub(crate) fn get_wireless_enabled() -> bool {
//     call_bhm_dbus_method_type("GetWirelessEnabled", &()).2 == 1
// }

// pub(crate) fn get_wireless_power_save(device: &str) -> u8 {
//     call_bhm_dbus_method_type("GetWirelessPowerSave", &(device)).2
// }

pub(crate) fn set_action(action: &str, value: &str) {
    call_bhm_dbus_method_type("SetAction", &(action, value));
}

//pub(crate) fn set_cpu_boost(boost: u8) -> u8 {
//    call_bhm_dbus_method_type("SetCpuFreqBoost", &(boost)).2
//}

//pub(crate) fn set_fan_pwm(mode: u8) -> u8 {
//    call_bhm_dbus_method_type("SetFanPwmEnable", &(mode)).2
//}

//pub(crate) fn set_fan_ttp(mode: u8) -> u8 {
//    call_bhm_dbus_method_type("SetFanThrottleThermalPolicy", &(mode)).2
//}

//pub(crate) fn set_fan_curve(curve: &str) -> String {
//    let r = call_bhm_dbus_method_type("SetFanCurve", &(curve));
//    if r.0 == T_STR {
//        r.1
//    } else {
//        String::new()
//    }
//}

//pub(crate) fn set_bat_limit(limit: u8) -> u8 {
//    call_bhm_dbus_method_type("SetBatChargeControlEndThreshold", &(limit)).2
//}

//pub(crate) fn set_kbd_brightness(value: u8) -> u8 {
//    call_bhm_dbus_method_type("SetKbdBrightness", &(value)).2
//}

//pub(crate) fn set_scr_brightness(value: u8) -> u8 {
//    call_bhm_dbus_method_type("SetScrBrightness", &(value)).2
//}

//pub(crate) fn set_wireless_enabled(enabled: bool) -> bool {
//    call_bhm_dbus_method_type("SetWirelessEnabled", &(enabled)).2 == 1
//}

//pub(crate) fn set_wireless_power_save(device: &str, value: u8) -> u8 {
//    call_bhm_dbus_method_type("SetWirelessPowerSave", &(device, value)).2
//}

// Profile

pub(crate) fn get_profile() -> String {
    let r = call_bhm_dbus_method_type("GetProfile", &());
    if r.0 == T_STR {
        r.1
    } else {
        String::new()
    }
}

// pub(crate) fn set_profile(id: &str) -> String {
//     let r = call_bhm_dbus_method_type("SetProfile", &(id));
//     if r.0 == T_STR {
//         r.1
//     } else {
//         String::new()
//     }
// }
pub(crate) fn profile_data(id: &str) -> Vec<Vec<String>> {
    let data: Vec<Vec<String>> = Vec::new();
    let method_name = "ProfileData";
    match call_bhm_dbus_method_old(method_name, &(id)) {
        Some(message) => match message.body::<Vec<Vec<String>>>() {
            Ok(body) => body,
            Err(error) => {
                eprintln!("Error {}: {}.", method_name, error);
                data
            }
        },
        None => data,
    }
}

pub(crate) fn prev_profile() -> String {
    let r = call_bhm_dbus_method_type("PrevProfile", &());
    if r.0 == T_STR {
        r.1
    } else {
        String::new()
    }
}
pub(crate) fn next_profile() -> String {
    let r = call_bhm_dbus_method_type("NextProfile", &());
    if r.0 == T_STR {
        r.1
    } else {
        String::new()
    }
}
pub(crate) fn create_profile(value: Vec<Vec<String>>) {
    call_bhm_dbus_method_type("CreateProfile", &(value));
}
pub(crate) fn update_profile(id: &str, value: Vec<Vec<String>>) {
    call_bhm_dbus_method_type("UpdateProfile", &(id, value));
}
pub(crate) fn remove_profile(id: &str) {
    let switched_id = call_bhm_dbus_method_type("RemoveProfile", &(id));
    if switched_id.0 == T_STR {
        println!(
            "Remove profile {}, switched to profile {}.",
            &id, &switched_id.1
        );
    }
}

pub(crate) fn data_fan_ttp() -> Vec<Vec<String>> {
    let data: Vec<Vec<String>> = vec![
        vec![String::from("1"), String::from("Boost")],
        vec![String::from("0"), String::from("Normal")],
        vec![String::from("2"), String::from("Silent")],
    ];
    data
}
pub(crate) fn data_bat_limit() -> Vec<Vec<String>> {
    let mut data: Vec<Vec<String>> = Vec::new();
    for i in 0..81 {
        data.push(vec![format!("{}", i + 20), format!("{}", i + 20)]);
    }
    data
}
pub(crate) fn data_profiles() -> Vec<Vec<String>> {
    let data: Vec<Vec<String>> = Vec::new();
    let method_name = "ProfileNames";
    match call_bhm_dbus_method_old(method_name, &()) {
        Some(message) => match message.body::<Vec<Vec<String>>>() {
            Ok(body) => body,
            Err(error) => {
                eprintln!("Error {}: {}.", method_name, error);
                data
            }
        },
        None => data,
    }
}
