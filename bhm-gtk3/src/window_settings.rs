use crate::utl::Evt;
use crate::widget;
use gtk::{glib, prelude::*, Orientation};
use std::sync::mpsc::Sender;
use std::{cell::RefCell, collections::HashMap, rc::Rc};

pub(crate) struct Settings {}

impl Settings {
    pub fn window(
        widget: &gtk::Widget,
        tx: Sender<Evt>,
        windows: &Rc<RefCell<HashMap<String, gtk::Window>>>,
        settings: Vec<(&String, bool)>,
    ) -> gtk::Window {
        let swt_connect_changed = move |switch: &gtk::Switch, group: &str| {
            let tx = tx.clone();
            let group = group.to_string();
            switch.connect_changed_active(move |switch| {
                //let key = &switch.widget_name().to_string();
                tx.send(Evt::Listener(format!(
                    "is_test:{}:{}",
                    group,
                    switch.is_active()
                )))
                .ok();
            });
        };

        let title = "Settings";

        let parent = widget::to_top(widget)
            .downcast::<gtk::ApplicationWindow>()
            .unwrap();
        let application = parent.application().unwrap();

        let window = gtk::Window::new(gtk::WindowType::Toplevel);
        window.set_role("settings");
        window.set_title(title);
        window.set_widget_name("settings");
        window.style_context().add_class("window-settings");
        window.set_position(gtk::WindowPosition::Mouse);
        window.set_default_size(400, 200);
        window.connect_delete_event(
            glib::clone!(@weak windows => @default-return Inhibit(false), move |_, _| {
                println!("Closing {} window.", title);
                windows.borrow_mut().remove("settings");
                Inhibit(false)
            }),
        );
        application.add_window(&window);

        let vbox = gtk::Box::new(Orientation::Vertical, 0);
        vbox.style_context().add_class("vbox");

        for item in settings {
            let hbox = gtk::Box::new(Orientation::Horizontal, 0);
            hbox.style_context().add_class("hbox");
            let lbl0 = gtk::Label::new(Some(item.0));
            let swt0 = gtk::Switch::new();
            swt0.set_widget_name(item.0);
            swt0.set_state(item.1);
            swt_connect_changed(&swt0, item.0);
            hbox.pack_start(&lbl0, false, false, 0);
            hbox.pack_end(&swt0, false, false, 0);
            vbox.pack_start(&hbox, false, false, 0);
        }

        window.add(&vbox);
        window.show_all();
        window.resize(240, 1);
        window
    }
}
