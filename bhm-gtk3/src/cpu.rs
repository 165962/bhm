//use std::fs;
//use std::io::Read;

use systemstat::platform::PlatformImpl;
use systemstat::{CPULoad, DelayedMeasurement, Platform, System};

//fn read_file(path: &str) -> std::io::Result<String> {
//    let mut s = String::new();
//    fs::File::open(path)
//        .and_then(|mut f| f.read_to_string(&mut s))
//        .map(|_| s)
//}

pub struct Cpu {
    sys: PlatformImpl,
    cpu_load_aggregate: DelayedMeasurement<CPULoad>,
    //path: String,
}

impl Cpu {
    pub fn new() -> Self {
        let sys = System::new();
        Cpu {
            sys: System::new(),
            cpu_load_aggregate: sys.cpu_load_aggregate().unwrap(),
            //path,
        }
    }
    // pub fn cpu_temp(&self) -> f32 {
    //     //return self.sys.cpu_temp().unwrap() as f32;
    //     let cpu_temp: f32 = read_file(&format!("{}/{}", self.path, "temp1_input"))
    //         .and_then(|data| match data.trim().parse::<f32>() {
    //             Ok(x) => Ok(x),
    //             Err(_) => Err(std::io::Error::new(
    //                 std::io::ErrorKind::Other,
    //                 "Could not parse float",
    //             )),
    //         })
    //         .map(|num| num / 1000.0)
    //         .unwrap_or(0.0);
    //     //println!("{:?}", self.sys.battery_life());
    //     //println!("{:?}", self.sys.on_ac_power());
    //
    //     //match self.sys.cpu_temp() {
    //     //    Ok(sys_cpu_temp) => {
    //     //        cpu_temp = sys_cpu_temp;
    //     //    }
    //     //    Err(x) => println!("\nCPU temp: {}", x)
    //     //}
    //     cpu_temp
    // }
    pub fn cpu_load_aggregate(&mut self) {
        self.cpu_load_aggregate = self.sys.cpu_load_aggregate().unwrap();
    }
    pub fn cpu_load_aggregate_done(&self) -> CPULoad {
        self.cpu_load_aggregate.done().unwrap()
    }
    pub fn cpu_percentage(&mut self) -> f32 {
        let cpu_data = self.cpu_load_aggregate_done();
        self.cpu_load_aggregate();
        (cpu_data.user + cpu_data.nice + cpu_data.system + cpu_data.interrupt) * 100.0
    }
    pub fn mem_percentage(&self) -> f32 {
        let mem_percentage: f32;
        match self.sys.memory() {
            Ok(mem) => {
                mem_percentage = (mem.total.as_u64() - mem.free.as_u64()) as f32
                    / mem.total.as_u64() as f32
                    * 100.0;
            }
            Err(x) => {
                mem_percentage = 0.0;
                println!("\nMemory: error: {}", x);
            }
        }
        mem_percentage
    }
}

//use cpu_freq;
//let cpus = cpu_freq::get();
//println!("CPU frequencies {:?}", cpus);
//for i in 0..16 {
//    println!("{} {}", i, cpus[i].cur.unwrap());
//}
//cpus.iter().for_each(|x|{
//    println!("{}", x.cur.unwrap());
//    //assert!(x.cur > Some(0.0));
//});

//use systemstat::{System, Platform, saturating_sub_bytes};
//let sys = System::new();
//use systemstat::{System, Platform, saturating_sub_bytes};
// thread::spawn(move || {
//     loop {
//         //let cpu0 = sys.cpu_load_aggregate().unwrap();
//         //thread::sleep(Duration::from_millis(100));
//         //let cpu = cpu0.done().unwrap();
//         //let all = cpu.user + cpu.nice + cpu.system + cpu.interrupt;
//         //println!("CPU load: {:.01}%", all * 100.0);
//         //match sys.cpu_load_aggregate() {
//         //    Ok(cpu) => {
//         //        thread::sleep(Duration::from_millis(100));
//         //        let cpu = cpu.done().unwrap();
//         //        let all = cpu.user + cpu.nice + cpu.system + cpu.interrupt;
//         //        //label04.set_text(&"");
//         //        println!("CPU load: {:.01}%", all * 100.0);
//         //        //println!("CPU load: {}% user, {}% nice, {}% system, {}% intr, {}% idle ",
//         //        //    cpu.user * 100.0, cpu.nice * 100.0, cpu.system * 100.0, cpu.interrupt * 100.0, cpu.idle * 100.0);
//         //    },
//         //    Err(x) => println!("\nCPU load: error: {}", x)
//         //}
//     }
// });
