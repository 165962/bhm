#[macro_use]
extern crate lazy_static;
use gtk::{gdk, gio, prelude::*};
use std::{env, process, process::exit};

use crate::app::App;

mod app;
mod utl;

mod pnl;
mod tree;
mod widget;
mod win;

mod bhm;
mod bus;
mod data;
mod scanner;

mod amdgpu;
mod cpu;
mod nvgpu;

mod gnome;
mod profile_vec;
mod window_fan_curve_editor;
mod window_profile_editor;

//use psutil;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    if gtk::init().is_err() {
        eprintln!("Failed to initialize GTK Application.");
        process::exit(1);
    }

    println!(
        "GTK major: {}, minor: {}.",
        gtk::major_version(),
        gtk::minor_version()
    );

    let is_dev = env::var("DEVELOPER").unwrap_or_default() == "1";
    let application_id = if is_dev {
        "bas.apps.hwmon-gtk3-dev"
    } else {
        "bas.apps.hwmon-gtk3"
    };
    let application = gtk::Application::new(Some(application_id), gio::ApplicationFlags::empty());
    application.connect_startup(|application| {
        let provider = gtk::CssProvider::new();
        let style = include_bytes!("style.css");
        provider.load_from_data(style).expect("Failed to load CSS");
        gtk::StyleContext::add_provider_for_screen(
            &gdk::Screen::default().expect("Error initializing gtk css provider."),
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        let mut app = App::new(application);
        app.start();
    });
    //application.connect_activate(build_ui);
    application.connect_window_added(w_added);
    application.connect_window_removed(a_remove);
    application.connect_shutdown(shutdown);
    application.run();
}

fn shutdown(a: &gtk::Application) {
    println!("shutdown {:?}", a);
}

fn w_added(_application: &gtk::Application, window: &gtk::Window) {
    match window.role() {
        Some(role) => {
            println!("Added window: {}, role: {}.", window.title().unwrap(), role);
        }
        None => {
            println!("Role none");
        }
    }
}

fn a_remove(application: &gtk::Application, window: &gtk::Window) {
    if let Some(role) = window.role() {
        println!(
            "Remove window: {}, role: {}.",
            window.title().unwrap(),
            role
        );
        if role == "main" {
            application.quit();
            println!("Application quit.");
            exit(0);
        }
    }
}
