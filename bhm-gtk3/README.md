##### Fedora 35
###### gtk3-devel
```shell
sudo dnf install gtk3-devel
```
###### acpi_call
```shell
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://repo.linrunner.de/fedora/tlp/repos/releases/tlp-release.fc$(rpm -E %fedora).noarch.rpm
sudo dnf install kernel-devel akmod-acpi_call
sudo modprobe acpi_call
```

```shell
sudo nano /etc/modules-load.d/acpi-call.conf
```
```text
acpi_call
```