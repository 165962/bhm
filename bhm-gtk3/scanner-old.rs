use crate::utl::read_file_to_string;
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::path::PathBuf;

pub struct ScanDir {
    pub level: i8,
    pub data: HashMap<String, HashMap<String, String>>,
    pub temp: Vec<Vec<String>>,
}

impl ScanDir {
    pub fn new() -> Option<ScanDir> {
        let data = HashMap::new();
        Some(ScanDir {
            level: 2,
            data,
            temp: vec![],
        })
    }

    pub fn scan(&mut self, path: &str) {
        self._scan(path, 1);
    }

    pub fn path_cpu(&self) -> String {
        let mut path = String::new();
        //scan_dir.scan(HW_PATH);
        for k0 in &self.data {
            let mut sorted: Vec<_> = k0.1.iter().collect();
            sorted.sort_by_key(|a| a.0);
            for (key, val) in sorted.iter() {
                if *key == "name" && *val == "k10temp" {
                    path = k0.0.to_string();
                }
            }
        }
        path
    }
    pub fn path_agpu(&self) -> String {
        let mut path = String::new();
        //scan_dir.scan(HW_PATH);
        for k0 in &self.data {
            let mut sorted: Vec<_> = k0.1.iter().collect();
            sorted.sort_by_key(|a| a.0);
            for (key, val) in sorted.iter() {
                if *key == "name" && *val == "amdgpu" {
                    path = k0.0.to_string();
                }
            }
        }
        path
    }
    pub fn path_fan(&self) -> String {
        let mut path = String::new();
        //scan_dir.scan(HW_PATH);
        for k0 in &self.data {
            let mut sorted: Vec<_> = k0.1.iter().collect();
            sorted.sort_by_key(|a| a.0);
            for (key, val) in sorted.iter() {
                if *key == "name" && *val == "asus" {
                    path = k0.0.to_string();
                }
            }
        }
        path
    }
    pub fn path_nvme(&self) -> String {
        let mut path = String::new();
        //scan_dir.scan(HW_PATH);
        for k0 in &self.data {
            let mut sorted: Vec<_> = k0.1.iter().collect();
            sorted.sort_by_key(|a| a.0);
            for (key, val) in sorted.iter() {
                if *key == "name" && *val == "nvme" {
                    path = k0.0.to_string();
                }
            }
        }
        path
    }
    pub fn path_wifi(&self) -> String {
        let mut path = String::new();
        //scan_dir.scan(HW_PATH);
        for k0 in &self.data {
            let mut sorted: Vec<_> = k0.1.iter().collect();
            sorted.sort_by_key(|a| a.0);
            for (key, val) in sorted.iter() {
                if *key == "name" && *val == "iwlwifi_1" {
                    path = k0.0.to_string();
                }
            }
        }
        path
    }

    fn _scan(&mut self, path: &str, level: i8) {
        if level <= self.level {
            let paths = fs::read_dir(path).unwrap();
            for dir_entry in paths {
                let path_buf = dir_entry.unwrap().path();
                let p = format!("{}", path_buf.display());
                if path_buf.is_dir() {
                    //println!("Dir {} {}", level, p);
                    self._scan(&p, level + 1);
                } else if path_buf.is_file() {
                    self._file(path, &p, &path_buf).unwrap();
                }
            }
        }
    }

    fn _file(&mut self, path: &str, p: &str, path_buf: &PathBuf) -> Result<(), Box<dyn Error>> {
        let file_name = format!("{}", path_buf.file_name().unwrap().to_string_lossy());
        let content = read_file_to_string(p);
        if !content.is_empty() {
            let k = path.to_string();
            let list: &mut HashMap<String, String>;
            if !self.data.contains_key(&k) {
                self.data.insert(path.to_string(), HashMap::new());
            }
            list = self.data.get_mut(&k).unwrap();
            list.insert(file_name, content);
        }
        Ok(())
    }

    //pub(crate) fn scan_temp(&mut self, path: &str) {
    //    self._scan_temp(path, 1);
    //}

    fn _scan_temp(&mut self, path: &str, level: i8) {
        if level <= self.level {
            let paths = fs::read_dir(path).unwrap();
            for dir_entry in paths {
                match dir_entry {
                    Ok(dir_entry) => {
                        //let id = self.temp.len();
                        let path_buf = dir_entry.path();
                        let obj_path = path_buf.display().to_string();
                        let file_name = dir_entry.file_name().to_string_lossy().to_string();
                        if path_buf.is_dir() {
                            self.temp.push(vec![obj_path, file_name]);
                            //self._scan_temp(&obj_path, level + 1);
                        } else if path_buf.is_file() {
                            //self.temp.push(vec![obj_path.to_string(), obj_name.to_string()]);
                        }
                    }
                    Err(e) => println!("Error {}", e),
                }
            }
        }
    }

    fn _file_temp(
        &mut self,
        _path: &str,
        p: &str,
        _path_buf: &PathBuf,
    ) -> Result<(), Box<dyn Error>> {
        //let file_name = format!("{}", path_buf.file_name().unwrap().to_string_lossy());
        let v = read_file_to_string(p);
        if !v.is_empty() {}
        Ok(())
    }
}
